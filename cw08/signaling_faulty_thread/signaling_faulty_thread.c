#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<signal.h>
#include<pthread.h>
#include <limits.h>
#include <unistd.h>

#define sig_num SIGUSR1

long thread_num;
char *file_name;
long records_num;     //for counting records
char *searched_word;
FILE *file_des;
pthread_t *thread_id;

long version;

void SIGINThandler(int signo)
{
    //fclose(file_des);
    pthread_exit(NULL);
}

void SIGhandler_echo(int signo)
{
    printf("Received signal. Receiver:\n "
                   "\tPid: %d,\n"
                   "\tThread ID: %ld,\n"
                   "\tsignal number: %d\n", getpid(), pthread_self(), signo);
}

int int_lenght(int i)
{
    int res = 0;
    while (i > 0)
    {
        i /= 10;
        res++;
    }
    return res;
}

void *thread_fun(void *args)
{
    if (version == 3 || version == 5) signal(sig_num, SIGhandler_echo);
    if (version == 4)
    {
        sigset_t mask;
        sigfillset(&mask);
        pthread_sigmask(SIG_BLOCK, &mask, NULL);
    }

    printf("Thread %ld working \n", pthread_self());

    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);


    int x;
    if (*((int *) args) == 1)
    {

        printf("Thread %ld is just committing suicide: x = 10 / %d\n", pthread_self(), *((int *) args));
        x = 10 / *((int *) args);
    }


    //pause();        //waiting for signal
    //for (int i=0;i<10000;i++);
    sleep(2);

    return NULL;
}

int main(int argc, char *argv[])
{

    if (argc < 3)
    {
        printf("Not enough arguments. Please provide how many threads to creat and version of the test:\n"
                       "1 - sending signal to process while no thread has it masked"
                       "2 - sending signal to process while only main thread has it masked"
                       "3 - sending signal to process while all threads use custom signal handler"
                       "4 - sending signal to thread that has it masked"
                       "5 - sending signal to thread that uses custom signal handler");
        exit(1);
    }
    /*
     *
     *
    wysłanie sygnału do procesu, gdy żaden wątek nie ma zamaskowanego tego sygnału
    wysłanie sygnału do procesu, gdy główny wątek programu ma zamaskowany ten sygnał, a wszystkie pozostałe wątki nie,
    wysłanie sygnału do procesu, gdy wszystkie wątki mają zainstalowaną niestandardową procedurę obsługi przerwania, która wypisuje informację o nadejściu tego sygnału oraz PID i TID danego wątku
    wysłanie sygnału do wątku z zamaskowanym tym sygnałem
    wysłanie sygnału do wątku, w którym zmieniona jest procedura obsługi sygnału, jak przedstawiono w punkcie 3

     */


    thread_num = strtol(argv[1], NULL, 10);
    thread_id = malloc(thread_num * sizeof(pthread_t));

    version = strtol(argv[2], NULL, 10);

    printf("Main pid %i\n\n", getpid());

    int arg = 1;
    for (int i = 0; i < thread_num - 1; i++)
    {
        if (pthread_create(&thread_id[i], NULL, thread_fun, &arg) < 0)
        {
            printf("Error while creating thread. Terminating\n");
            return 1;
        }
    }

    arg = 0;
    if (pthread_create(&thread_id[thread_num - 1], NULL, thread_fun, &arg) < 0)
    {
        printf("Error while creating last thread. Terminating\n");
        return 1;
    }

    /*
    The new thread starts execution by invoking
    start_routine() - which is thread_fun
    arg is passed as the sole argument of start_routine()- NULL cause everithing necessary inside is global.

     If attr is NULL, then the thread is created with
   default attributes.

     Before returning, a successful call to pthread_create() stores the ID
   of the new thread in the buffer pointed to by thread; this identifier
   is used to refer to the thread in subsequent calls to other pthreads
   functions.
     */
    sigset_t mask; //giving threads some time to set masks

    sleep(1);

    switch (version)
    {
        case 1:
            kill(getpid(), sig_num);   //main thread sending to itself, no masks set
            break;

        case 2:
            sigfillset(&mask);      //I dont bother about sig_num, just set all
            sigprocmask(SIG_BLOCK, &mask, NULL);    //block all
            kill(getpid(), sig_num);
            break;

        case 3:
            signal(sig_num, SIGhandler_echo);
            kill(getpid(), sig_num);
            break;

        case 4:
            for (long i = 0; i < thread_num; i++)
            {
                if (pthread_kill(thread_id[i], sig_num) != 0)
                {
                    printf("Error while sending signal to thread with ID %ld\n", thread_id[i]);
                    perror(NULL);
                }
            }
            break;

        case 5:
            for (long i = 0; i < thread_num; i++) pthread_kill(thread_id[i], sig_num);
            break;

        default:
            printf("Unknown version!\n");

    }

    printf("Done. Please enter any key to join threads.\n");
    getc(stdin);

    //waiting for threads to terminate
    for (int i = 0; i < thread_num; i++)
    {
        pthread_join(thread_id[i],
                     NULL);       // If retval is not NULL, then pthread_join() copies the exit status of the target thread
    }
    return 0;
}