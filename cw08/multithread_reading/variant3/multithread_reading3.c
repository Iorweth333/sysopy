#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<signal.h>
#include<pthread.h>
#include<fcntl.h>
#include<unistd.h>
#include<sys/syscall.h>
#include <limits.h>

long threads_num;
char *file_name;
long records_num;     //for counting records
char *searched_word;
FILE *file_des;
pthread_t *thread_id;
int *completed_threads;     //idea found here: https://stackoverflow.com/questions/19232957/pthread-create-passing-an-integer-as-the-last-argument

void SIGINThandler(int signo)
{
    //fclose(file_des);
    pthread_exit(NULL);
}

int int_lenght(int i)
{
    int res = 0;
    while (i > 0)
    {
        i /= 10;
        res++;
    }
    return res;
}

void *thread_fun(void *args)        //pthreat_getspecific pthread_setspecific
{
    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
    pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED, NULL);
    //In variant3 all threads are detached and the condition of terminating of a thread is that there is nothing else to read


    char *buf = malloc((size_t) records_num * 1024);      //file includes records of fixed size 1024 bytes
    char *record = malloc(1024);

    int int_max = INT_MAX;
    int int_len = int_lenght(int_max);      //holds numbers of chars that int coverted to string can take

    char record_id[int_len];        //God damn! I just realised i created static array of non-const. What is amazing- it actually works


    while (fread(buf, sizeof(char), (size_t) records_num * 1024, file_des) > 0)
    {
        //even if read chars are not enough to fill *buf, it should not casue problems
        for (int i = 0; i < records_num; i++)
        {
            strncpy(record, buf + i * 1024, 1024);
            strncpy(record_id, record, (size_t) int_len);

            //debug + nice observation
            //printf ("record_id = %s \n", record_id);

            //there is always @ char printing after record_id. I have absolutely no idea where it comes from, but it does no harm

            if (strstr(record + int_len, searched_word) != NULL)
                printf("Word %s found by thread %ld in %s record\n", searched_word, (long) (pthread_self()), record_id);

        }
        //no need for cancellation point, all records are to be read
    }

    //debug + also nice observation
    //printf ("Thread with id %ld and index %i finishes\n", (long) (pthread_self()), *((int *) args));

    completed_threads[*((int *) args)] = -1;    //negative number sygnalises and of execution
    //idea found here: https://stackoverflow.com/questions/19232957/pthread-create-passing-an-integer-as-the-last-argument

    return NULL;
}

int main(int argc, char *argv[])
{

    if (argc < 5)
    {
        printf("Not enough arguments. Please provide: number of threads, filename, number of records read by thread in one reading and a word to search for.\n");
        exit(1);
    }
    threads_num = strtol(argv[1], NULL, 10);
    thread_id = malloc(threads_num * sizeof(pthread_t));
    completed_threads = calloc((size_t) threads_num, sizeof(int));
    file_name = malloc((strlen(argv[2]) + 1) * sizeof(char));
    strcpy(file_name, argv[2]);
    file_des = fopen(file_name, "r");
    records_num = strtol(argv[3], NULL, 10);
    searched_word = malloc((strlen(argv[4]) + 1) * sizeof(char));
    strcpy(searched_word, argv[4]);
    signal(SIGINT, SIGINThandler);
    for (int i = 0; i < threads_num; i++)
    {
        completed_threads[i] = i;
        if (pthread_create(&thread_id[i], NULL, thread_fun, &completed_threads[i]) < 0)
        {
            printf("Error while creating thread. Terminating\n");
            return 1;
        }
        pthread_detach(thread_id[i]);   //in variant3 all threads are detached
    }
    /*
    The new thread starts execution by invoking
    start_routine() - which is thread_fun
    arg is passed as the sole argument of start_routine()- NULL cause everithing necessary inside is global.

     If attr is NULL, then the thread is created with
   default attributes.

     Before returning, a successful call to pthread_create() stores the ID
   of the new thread in the buffer pointed to by thread; this identifier
   is used to refer to the thread in subsequent calls to other pthreads
   functions.
     */


    //waiting for threads to terminate


    for (int i = 0; i < threads_num; i++)
    {
        while (completed_threads[i] >= 0);   //active waiting
    }


    fclose(file_des);
    return 0;
}