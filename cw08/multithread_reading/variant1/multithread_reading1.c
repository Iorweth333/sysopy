#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<signal.h>
#include<pthread.h>
#include<fcntl.h>
#include<unistd.h>
#include<sys/syscall.h>
#include <limits.h>

long thread_num;
char *file_name;
long records_num;     //for counting records
char *searched_word;
FILE *file_des;
pthread_t *thread_id;


void SIGINThandler(int signo)
{
    //fclose(file_des);
    pthread_exit(NULL);
}

int int_lenght(int i)
{
    int res = 0;
    while (i > 0)
    {
        i /= 10;
        res++;
    }
    return res;
}

void *thread_fun(void *args)
{
    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
    //in variant1 thread that finds the word, cancells other threads asynchronously


    char *buf = malloc((size_t) records_num * 1024);      //file includes records of fixed size 1024 bytes
    char *record = malloc(1024);

    int int_max = INT_MAX;
    int int_len = int_lenght(int_max);      //holds numbers of chars that int coverted to string can take

    char record_id[int_len];        //God damn! I just realised i created static array of non-const. What is amazing- it actually works

    short found = 0;

    while (fread(buf, sizeof(char), (size_t) records_num * 1024, file_des) > 0)
    {
        //even if read chars are not enough to fill *buf, it should not casue problems
        for (int i = 0; i < records_num; i++)
        {
            strncpy(record, buf + i * 1024, 1024);
            strncpy(record_id, record, (size_t) int_len);

            //debug + nice observation
            //printf ("record_id = %s \n", record_id);

            //there is always @ char printing after record_id. I have absolutely no idea where it comes from, but it does no harm

            if (strstr(record + int_len, searched_word) != NULL)
            {
                printf("Word %s found by thread %ld in %s record\n", searched_word, (long) (pthread_self()), record_id);
                found = 1;
                break;
            }
        }
        if (found)
        {
            //cancelling others
            pthread_t self = pthread_self();
            for (long i = 0; i < thread_num; i++)
                if (thread_id[i] != 0 && thread_id[i] != self)
                    pthread_cancel(thread_id[i]);

        }
    }
    return NULL;
}

int main(int argc, char *argv[])
{

    if (argc < 5)
    {
        printf("Not enough arguments. Please provide: number of threads, filename, number of records read by thread in one reading and a word to search for.\n");
        exit(1);
    }
    thread_num = strtol(argv[1], NULL, 10);
    thread_id = malloc(thread_num * sizeof(pthread_t));
    file_name = malloc((strlen(argv[2]) + 1) * sizeof(char));
    strcpy(file_name, argv[2]);
    file_des = fopen(file_name, "r");
    records_num = strtol(argv[3], NULL, 10);
    searched_word = malloc((strlen(argv[4]) + 1) * sizeof(char));
    strcpy(searched_word, argv[4]);
    signal(SIGINT, SIGINThandler);
    for (int i = 0; i < thread_num; i++)
    {
        if (pthread_create(&thread_id[i], NULL, thread_fun, NULL) < 0)
        {
            printf("Error while creating thread. Terminating\n");
            return 1;
        }

        /*
        The new thread starts execution by invoking
        start_routine() - which is thread_fun
        arg is passed as the sole argument of start_routine()- NULL cause everithing necessary inside is global.

         If attr is NULL, then the thread is created with
       default attributes.

         Before returning, a successful call to pthread_create() stores the ID
       of the new thread in the buffer pointed to by thread; this identifier
       is used to refer to the thread in subsequent calls to other pthreads
       functions.
         */
    }

    //waiting for threads to terminate
    for (int i = 0; i < thread_num; i++)
    {
        pthread_join(thread_id[i],
                     NULL);       // If retval is not NULL, then pthread_join() copies the exit status of the target thread
    }
    fclose(file_des);
    return 0;
}