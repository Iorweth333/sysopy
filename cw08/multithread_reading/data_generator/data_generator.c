#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<sys/stat.h>
#include <time.h>


int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        printf("Not enought arguments. Please provide size of the file (records) and it's name.\n");
        return 1;
    }
    srand( (unsigned int) time ( NULL));
    long size = strtol(argv[1], NULL, 10);
    char *name = malloc((strlen(argv[2]) + 1) * sizeof(char));
    strcpy(name, argv[2]);
    char str[1014]; //1014 = 1024 - 10. 10 is numbers of chars that int coverted to string can take
    FILE *file = fopen(name, "w");
    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < 1014; j++)
        {
            str[j] = (char) ((rand() % 26) + 'a');
            //printf ("str[j] = %c\n", str[j]);
        }

        fprintf(file, "%010d%s", i, str);        //"%010d means Format number with up to 10 leading zeroes
        printf ("new string: %s\n", str);
    }
    fclose(file);
    chmod(name, 0666);
    return 0;
}