#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <wait.h>

/*
 * "zasadniczo to ze czegos powinno się unikac nie jest równoznaczne z tym ze nie powinno się tego robic w ogole"
 *      Piotr Bilecki o zmiennych globalnych
 */

long signals_counter;
long replies;
int child_pid;
int ppid;

void SIGhandler(int signo)
{
    signals_counter++;
    //kill(getppid(), signo);       //when child gets SIGUSR1 or SIGRTMIN it counts it and sends it back to parent
    kill(ppid, signo);
    //optimization: assigning ppid to a global variable
}

void REPLYhandler(int signo)
{
    replies++;                      //when parent gets SIGUSR1 or SIGRTMIN back it counts it

}

void ENDSIGhandler(int signo)       //ending signal handler
{
    printf("Signals received (child): %ld\n", signals_counter);
    exit(0);                        //when child gets SIGUSR2 or SIGRTMIN+1 it exits
}

void SIGINThandler(int signo)
{
    printf("SIGINT received, terminating \n");
    kill(child_pid, SIGKILL);
    exit(0);
}


int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        printf("Too few arguments.\nPlease provide how many signals to send and type of those signals.\n"
                       "  Types: \n"
                       "  1:  SIGUSR1, SIGUSR2 using function kill\n"
                       "  2:  SIGUSR1, SIGUSR2 using function sigqueue\n"
                       "  3:  SIGRTMIN, SIGRTMIN+1 using function  kill\n");
        exit(1);
    }

    replies = 0;
    signals_counter = 0;
    long signals_num = strtol(argv[1], NULL, 10);
    long signals_type = strtol(argv[2], NULL, 10);


    if (signals_type > 3 || signals_type < 0 || signals_num < 1)
    {
        printf("Provided arguments incorrect.\n");
        exit(1);
    }

    struct sigaction act;
    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;


    act.sa_handler = ENDSIGhandler;
    sigaction(SIGRTMIN + 1, &act, NULL);

    int pid = fork();

    if (pid == -1)
    {
        printf("Fork failed. Terminating\n");
        perror(NULL);
        exit(1);
    }

    if (pid == 0)
    {
        //child;

        act.sa_handler = SIGhandler;
        sigaction(SIGUSR1, &act, NULL);

        act.sa_handler = SIGhandler;
        sigaction(SIGRTMIN, &act, NULL);

        act.sa_handler = ENDSIGhandler;
        sigaction(SIGUSR2, &act, NULL);

        ppid = getppid();

        //for (int i = 0; i < signals_num + 1; i++) sleep(1);   //child is supposed to exit when SIGUSR2 or SIGRTMIN+1 received
        for (int i = 0; i < signals_num + 1; i++) pause();      //pause instead of sleep, bacause sleep seems to cause problems

    }
    else
    {
        //parent

        child_pid = pid;

        act.sa_handler = REPLYhandler;
        sigaction(SIGUSR1, &act, NULL);

        act.sa_handler = REPLYhandler;
        sigaction(SIGRTMIN, &act, NULL);

        act.sa_handler = SIGINThandler;
        sigaction(SIGINT, &act, NULL);

        long signals_sent = 0;

        if (signals_type == 1)
        {
            for (int i = 0; i < signals_num; i++)
            {
                sleep(1);
                if (kill(pid, SIGUSR1) == 0) signals_sent++;
                //important: sleep must be before sending signal.
                // otherwise child process gets <defunct> immediately
                //(probably it doesn't manage to do sigaction before getting signals)

            }
            sleep(1);           //another sleep just to make sure the last signal doesn't get lost
            kill(pid, SIGUSR2);

        }

        if (signals_type == 2)
        {
            const union sigval garbage;             //garbage, nothing to do with it, declared only to be put into sigqueue and never be seen again

            for (int i = 0; i < signals_num; i++)
            {
                sleep(1);
                if (sigqueue(pid, SIGUSR1, garbage) == 0) signals_sent++;
            }
            sleep(1);
            sigqueue(pid, SIGUSR2, garbage);
        }

        if (signals_type == 3)
        {
            for (int i = 0; i < signals_num; i++)
            {
                sleep(1);
                if (kill(pid, SIGRTMIN) == 0) signals_sent++;
            }
            sleep(1);
            kill(pid, SIGRTMIN + 1);
        }

        int status;
        wait(&status);

        printf("Signals sent to child (not including end-of-work-signal): %ld\n", signals_sent);
        printf("Replies received (parent): %ld \n", replies);

    }


    return 0;
}