#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <wait.h>
#include <time.h>

/*
 * "zasadniczo to ze czegos powinno się unikac nie jest równoznaczne z tym ze nie powinno się tego robic w ogole"
 *      Piotr Bilecki o zmiennych globalnych
 */

long signals_counter;
long replies;
int *children_pids;
int ppid;
long children_num;

void SIGhandler(int signo)
{
    signals_counter++;
    //kill(getppid(), signo);       //when child gets SIGUSR1 or SIGRTMIN it counts it and sends it back to parent
    kill(ppid, signo);
    //optimization: assigning ppid to a global variable
}

void REPLYhandler(int signo)
{
    replies++;                      //when parent gets SIGUSR1 or SIGRTMIN back it counts it
}

void ENDSIGhandler(int signo)       //ending signal handler
{
    printf("Signals received (child): %ld\n", signals_counter);
    exit(0);                        //when child gets SIGUSR2 or SIGRTMIN+1 it exits
}

void SIGINThandler(int signo)
{
    printf("SIGINT received, terminating \n");

    for (int i = 0; i < children_num; i++) if (children_pids[i] != 0) kill(children_pids[i], SIGKILL);
    exit(0);
}


int send_signal(int sleep_arg)
{
    int signal_type = rand() % 3 + 1;

    int pid = children_pids[rand() % children_num];   //drawing a child

    //important: sleep must be before sending signal.
    // otherwise child process gets <defunct> immediately
    //(probably it doesn't manage to do sigaction before getting signals)
    if (sleep_arg > 0) sleep((unsigned int) sleep_arg);

    if (signal_type == 1)
    {
        if (kill(pid, SIGUSR1) == 0) return 1;
        return 0;
    }

    if (signal_type == 2)
    {
        const union sigval garbage;             //garbage, nothing to do with it, declared only to be put into sigqueue and never be seen again

        if (sigqueue(pid, SIGUSR1, garbage) == 0) return 1;
        return 0;
    }

    if (signal_type == 3)
    {
        if (kill(pid, SIGRTMIN) == 0) return 1;
        return 0;
    }
    return 0;
}


int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        printf("Exchanging signals, but with many children.\n"
                       "Too few arguments.\nPlease provide how many children to make and how many signals to send.\n"
                       "also (optionally) argument for sleep().\nTypes of signals will be randomised\n");
        exit(1);
    }

    replies = 0;
    signals_counter = 0;
    children_num = strtol(argv[1], NULL, 10);
    long signals_num = strtol(argv[2], NULL, 10);
    long sleep_arg = 0;
    if (argc == 4) sleep_arg = strtol(argv[3], NULL, 10);
    children_pids = calloc((size_t) children_num, sizeof(*children_pids));


    if (signals_num < 1)
    {
        printf("Provided arguments incorrect.\n");
        exit(1);
    }

    struct sigaction act;
    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;


    act.sa_handler = ENDSIGhandler;
    sigaction(SIGRTMIN + 1, &act, NULL);

    act.sa_handler = REPLYhandler;
    sigaction(SIGUSR1, &act, NULL);

    act.sa_handler = REPLYhandler;
    sigaction(SIGRTMIN, &act, NULL);

    act.sa_handler = SIGINThandler;
    sigaction(SIGINT, &act, NULL);


    for (int j = 0; j < children_num; j++)
    {
        int pid = fork();

        if (pid == -1)
        {
            printf("Fork failed. Terminating\n");
            perror(NULL);
            exit(1);
        }

        if (pid == 0)
        {
            //child; changing perent's reactions for signals

            act.sa_handler = SIGhandler;
            sigaction(SIGUSR1, &act, NULL);

            act.sa_handler = SIGhandler;
            sigaction(SIGRTMIN, &act, NULL);

            act.sa_handler = ENDSIGhandler;
            sigaction(SIGUSR2, &act, NULL);

            ppid = getppid();

            //TODO: this waiting for signals won't work properly now. maybe I should use sleep() again?

            //for (int i = 0; i < signals_num + 1; i++) sleep(1);   //child is supposed to exit when SIGUSR2 or SIGRTMIN+1 received
            for (int i = 0; i < signals_num + 1; i++)
                sleep(1);      //pause instead of sleep, bacause sleep seems to cause problems
            return 0;

        }
        //parent
        children_pids[j] = pid;
    }

    srand((unsigned int) time(NULL));

    long signals_sent = 0;



    for (int k = 0; k < signals_num; k++)
    {
        signals_sent += send_signal((int) sleep_arg);
    }

    int status;
    for (long i = 0; i < children_num; i++)
    {
        kill(children_pids[i], SIGUSR2);
        waitpid(children_pids[i], &status, 0);      //waitpid just to make sure
    }

    printf("Signals sent to children (not including end-of-work-signal): %ld\n", signals_sent);
    printf("Replies received (parent): %ld \n", replies);


    return 0;
}