#include <signal.h>
#include <stdio.h>
#include <stdlib.h>


int direction;


typedef void ( * SIG_handler) (int signal_no);   //pointer to a comparing function



void SIGhandler(int signo)
{
    if (signo == 20)    //SIGTSTP. I could use macros instead
    {
        if (direction == 1) direction = -1;
        else direction = 1;
    }

    if (signo == 2)     //SIGINT
    {
        printf("\nOdebrano sygnał SIGINT. Zakonczenie dzialania.\n");
        exit (0);
    }

}


//void (*signal(int 20, *SIGhandler)(20))) ();
//int sigaction(int sig_no, const struct sigaction *act, struct sigaction *old_act);

int main() {

    char letter = 'A' - 1;  //-1 because it's incremented before first cout

    direction = 1;

    SIG_handler handler = SIGhandler;
    signal (20, handler);       //SIGTSTP
    //signal (2, handler);        //SIGINT


    struct sigaction act;
    act.sa_handler = SIGhandler;
    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;
    sigaction(SIGINT, &act, NULL);


    while (1)
    {
        while (!getchar()) system("PAUSE");

        letter += direction;

        if (letter > 'Z') letter = 'A';
        if (letter < 'A') letter = 'Z';

        printf ("%c", letter);
    }


    return 0;
}