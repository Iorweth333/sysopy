#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <time.h>
#include <limits.h>
#include <bits/siginfo.h>
#include <sys/wait.h>

long requests_delivered;
long requests_delivered_overall;
long children_num;
long requests_num;
int *children_pids;
short int *requests_flags;

//typedef void ( * SIG_handler) (int signal_no);   //pointer to a comparing function

void clear_flags(short int *arr, long len)         //todo check if this work as I want it
{
    for (long i = 0; i < len; i++) arr[i] = 0;
}

void set_flag_for(pid_t pid)
{
    for (long i = 0; i < children_num; i++)
        if (children_pids[i] == pid)
        {
            requests_flags[i] = 1;
            break;
        }
}


void requests_handler(int signo, siginfo_t *info, void *ucontext)
{
    printf("Got signal %i, ", info->si_signo);

    if (signo == SIGUSR1)
    {
        printf("it's request from PID %i \n", info->si_pid);
        requests_delivered++;
        requests_delivered_overall++;
        set_flag_for(info->si_pid);
    }

    if (signo >= SIGRTMIN && signo <= SIGRTMAX) printf("it's SIGRT_ from PID %i \n", info->si_pid);


    //if (signo == SIGUSR2) printf ("it's SIGUSR2\n");      //children do not have to respond that they got their reply
}

void SIGINT_handler(int signo, siginfo_t *info, void *ucontext)
{
    if (signo == SIGINT)
    {
        printf("Got signal, it's SIGINT - exiting\n");
        for (long i = 0; i < children_num; i++) kill(children_pids[i], SIGKILL);
        exit(0);
    }
}

void child_termination_handler(int signo, siginfo_t *info, void *ucontext)
{
    printf("Child %i terminated with %i \n", info->si_pid, info->si_status);
}


int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        printf("Too few arguments.\nPlease provide how many children to fork and how many requests to wait for.\n");
        exit(0);
    }
    children_num = strtol(argv[1], NULL, 10);
    requests_num = strtol(argv[2], NULL, 10);

    if (requests_num > children_num || children_num % requests_num != 0)
    {
        printf("Sorry, but arguments you provided could cause some trouble\n"
                       "good arguments should meet these conditions:\n"
                       "requests number < children number \n"
                       "children_num %% requests_num = 0 \n");
        exit(1);
    }

    children_pids = malloc(children_num * sizeof(int));
    requests_flags = malloc(children_num * sizeof(short int));
    clear_flags(requests_flags, children_num);           //TODO: check if this is ok

    struct sigaction act;
    act.sa_sigaction = requests_handler;
    sigemptyset(&act.sa_mask);
    act.sa_flags = 0 | SA_SIGINFO;      //sa_siginfo allows to handle signal with function that takes three args- useful to have sender's pid
    sigaction(SIGUSR1, &act, NULL);
    sigaction(SIGUSR2, &act, NULL);

    for (int i = SIGRTMIN; i <= SIGRTMAX; i++) sigaction(i, &act, NULL);

    act.sa_sigaction = SIGINT_handler;
    sigaction(SIGINT, &act, NULL);
    act.sa_sigaction = child_termination_handler;
    sigaction(SIGCHLD, &act, NULL);

    for (int i = 0; i < children_num; i++)
    {

        int pid = fork();

        if (pid == 0)
        {
            //child;
            //simulation of work
            srand((unsigned int) time(NULL));
            //sleep ( (unsigned int) rand () % 10 + 1);
            sleep((unsigned int) i % 11);

            kill(getppid(), SIGUSR1);       //senging request
            int time = sleep(USHRT_MAX);   // USHRT_MAX = max value that unsigned short int can hold
            // sleepcauses the calling thread to sleep either until the number of
            // real-time seconds specified in seconds have elapsed or until a signal
            // arrives which is not ignored. number of seconds left to sleep is returned
            time = USHRT_MAX - time;        // Time it waited is the difference between MAX an what's left
            printf("child with PID %i waited %i seconds\n", getpid(), time);
            kill(getppid(),
                 rand() % (SIGRTMAX - SIGRTMIN + 1) + SIGRTMIN);     //sending random one signal of realtime signals
            exit(time);
        }
        else
        {
            children_pids[i] = pid;
        }
    }

    while (requests_delivered_overall <= children_num)
    {
        if (requests_delivered == requests_num)
        {
            requests_delivered = 0;
            printf("\nSending replies\n");
            for (long i = 0; i < children_num; i++)
            {
                if (requests_flags[i])
                {
                    printf("  sending reply to %i\n", children_pids[i]);
                    kill(children_pids[i], SIGUSR2);
                }
            }
            printf("\n");
            clear_flags(requests_flags, children_num);
        }


        if (requests_delivered_overall == children_num) break;
        pause();

    }

    //now, there are at least fewtwo
    //first: wait for SIGRT_ and SIGCHLD from everyone from the last wave - unreliable, signals tend to be discarded
    /*
    for (int i = 0; i < requests_num * 2; i++) pause();
 */



    //second: checking exit status of every child
    //it's realiable, but causes mess in input... well, quid pro quo

    int status;


    printf("\nChecking state of children\n");
    for (long i = 0; i < children_num; i++)
    {
        printf("  checking %i, ", children_pids[i]);
        waitpid(children_pids[i], &status, 0);
        if (WIFEXITED(status)) status = WEXITSTATUS (status);
        printf("terminated with %i \n", status);
    }

    //after finishing this task, I've come up with another idea: sleeping (e.g. for 10 seconds) while waiting for SIGCHLD
    //if all seconds run up it means that it's time to finish

    return 0;
    //the program will exit due to SIGINT or exits od all children
}