#define _GNU_SOURCE //necessary to get POLLDRHUP definition
#pragma GCC diagnostic ignored "-Wstrict-aliasing"       //supressing this warning

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<signal.h>
#include<fcntl.h>
#include<unistd.h>
#include<errno.h>
#include<endian.h>
#include<netinet/in.h>
#include<sys/types.h>
/*POSIX.1 does not require the inclusion of <sys/types.h>, and this
  header file is not required on Linux.  However, some historical (BSD)
  implementations required this header file, and portable applications
  are probably wise to include it.
 */
#include<sys/socket.h>
#include<sys/un.h>
#include<arpa/inet.h>
#include<limits.h>
#include<poll.h>
#include<pthread.h>
#include <sys/stat.h>
#include "client-server_commons.h"

//-lpthread -lrt

const int LOCAL = MAX_CLIENTS_NUM + 1;
const int NET = MAX_CLIENTS_NUM;

int port_num;
struct sockaddr_un local_addr;  /* Structure describing the address of an AF_LOCAL (aka AF_UNIX) socket.
                                 * Contains socket family (e.g. AF_UNIX */
struct sockaddr_in net_addr;    //* Structure describing an Internet socket address.  */
struct pollfd observer[MAX_CLIENTS_NUM + 2];  /* Data structure describing a polling request.  */
pthread_t threads_ids[3];
char socket_path[SUNPATH_MAX_LEN];
char clients_names[MAX_CLIENTS_NUM][MAX_NAME_LEN];
int order_num;
int pinged[MAX_CLIENTS_NUM];

int pick_client();

void sighandler(int signo);

void *input_handler(void *arg);

void *watch(void *arg);

void *ping(void *arg);


int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        printf("Not enough arguments. Please provide port number and socket path\n");
        exit(1);
    }
    //path=calloc(108, sizeof(char));
    //path = getcwd(path, 108);
    port_num = (int) strtol(argv[1], NULL, 10);
    strcpy(socket_path, argv[2]);
    signal(SIGINT, sighandler);
    order_num = 0;


    //initiating observer which is an array of structures describin pollig requests
    for (int i = 0; i < MAX_CLIENTS_NUM; i++)
    {
        for (int j = 0; j < MAX_NAME_LEN; j++) clients_names[i][j] = '\0';  //that is probably an overkill, but ok
        observer[i].fd = -1;
        /*The field fd contains a file descriptor for an open file.  If this
       field is negative, then the corresponding events field is ignored and
       the revents field returns zero.  (This provides an easy way of ignor‐
       ing a file descriptor for a single poll() call: simply negate the fd
       field.
         */
        observer[i].events = POLLIN | POLLHUP | POLLRDHUP;
        //POLLDRHUP - Stream socket peer closed connection, or shut down writing half of connection.
        //POLLHUP - Hang up (only returned in revents; ignored in events)

        pinged[i] = 0;
    }

    //creating local socket
    int socket_fd = 0;
    if ((socket_fd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0) //The AF_UNIX (also known as AF_LOCAL) socket family is
    {                                             //used tocommunicate between processes on the same machine efficiently.
        printf("Error while creating local socket. Terminating\n");
        exit(1);
    }
    //and saving it in observer array
    observer[LOCAL].fd = socket_fd;


    //creating net socket
    if ((socket_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)  //AF_INET = IPv4 Internet protocols
    {
        printf("Error in creating local socket\n");
        exit(1);
    }
    //and also saving it in observer
    observer[NET].fd = socket_fd;

    net_addr.sin_port = htons((uint16_t) port_num);    //sin_port = Port number.  */
    net_addr.sin_addr.s_addr = htonl(INADDR_ANY);       //struct in_addr sin_addr =  Internet address.

    printf("Server Internet address: %ld\n", (long) net_addr.sin_addr.s_addr);


    // Structure in_addr sin_addr contains only one field  */
    //The htonl() ("host to online" I suppose) function converts the unsigned integer argument from host byte order to network byte order.
    //The network byte order is defined to always be big-endian. Big-endian byte ordering places the most significant byte first
    //htons differs from htonl only with length of argument: htons takes unsigned short integerand htonl takes unsigned integer
    net_addr.sin_family = AF_INET;
    local_addr.sun_family = AF_UNIX;


    //binding names to sockets
    strncpy(local_addr.sun_path, (char *) socket_path, sizeof(local_addr.sun_path));
    if (bind(observer[LOCAL].fd, (struct sockaddr *) &local_addr, sizeof(struct sockaddr_un)) < 0)
    {
        /*about casting to sockaddr: The only purpose of this structure is to cast the structure pointer
        passed in addr in order to avoid compiler warnings.
         The actual structure passed for the addr argument will depend on the address family.
        */
        printf("Error while binding local\n");
        perror(NULL);
        sighandler(errno);
    }
    printf("Server Local path: %s\n", local_addr.sun_path);

    if (bind(observer[NET].fd, (struct sockaddr *) &net_addr, sizeof(struct sockaddr_in)) < 0)
    {
        printf("Error while binding network\n");
        perror(NULL);
        sighandler(errno);
    }

    //creating threads, because handling input and handling of the network must be in separate threads (it's a task condition)
    pthread_create(&threads_ids[0], NULL, watch, NULL);
    pthread_create(&threads_ids[1], NULL, input_handler, NULL);
    pthread_create(&threads_ids[2], NULL, ping, NULL);
    for (int i = 0; i < 3; i++) pthread_join(threads_ids[i], NULL);
    unlink(socket_path);
    return 0;
}


int pick_client()
{
    int clients_counter = 0;
    int tmp = 0;
    for (int i = 0; i < MAX_CLIENTS_NUM; i++) if (observer[i].fd != -1) clients_counter++;

    if (clients_counter == 0) return -1;

    if (clients_counter == 1)   //there is only one client, searching for it's index
        for (int i = 0; i < MAX_CLIENTS_NUM; i++)
            if (observer[i].fd != -1) return i;

    //else picking one of clients randomly
    tmp = rand() % clients_counter;
    clients_counter = 0;
    for (int i = 0; i < MAX_CLIENTS_NUM; i++)
        if (observer[i].fd != -1)
            if (tmp == clients_counter++) return i;

    return -1;
}


void sighandler(int signo)
{
    for (int i = 0; i < 3; i++) pthread_cancel(threads_ids[i]);
    for (int i = 0; i < MAX_CLIENTS_NUM + 2; i++)
    {
        if (observer[i].fd != -1)
        {
            shutdown(observer[i].fd, SHUT_RDWR);
            close(observer[i].fd);
        }
    }
    unlink(socket_path);
    exit(signo);
}


void *input_handler(void *arg)  //waits for input from keyboard and interprets it
{
    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
    char buf[100];
    char symbol;
    //char* tmp;
    char num1[30];
    char num2[30];
    unsigned int i, j;
    int client = -1;
    struct message msg;
    int fd = -1;

    while (1)
    {
        fgets(buf, sizeof(buf), stdin);     //no unnecessary spaces pls!
        for (i = 0; i < strlen(buf) && buf[i] >= '0' && buf[i] <= '9'; i++)num1[i] = buf[i];
        num1[i] = '\0';
        symbol = buf[i++];
        for (j = i; j < strlen(buf) && buf[j] >= '0' && buf[j] <= '9'; j++)num2[j - i] = buf[j];
        num2[j - i] = '\0';
        if (!(symbol == '+' || symbol == '-' || symbol == '*' || symbol == '/')) continue;
        client = pick_client();     //client = index in observer tab
        if (client == -1) printf("No clients connected\n");
        else
        {
            fd = observer[client].fd;
            msg.type = ORDER;
            msg.order_num = order_num++;
            msg.num1 = (int) strtol(num1, NULL, 10);
            msg.num2 = (int) strtol(num2, NULL, 10);
            msg.sign = symbol;
            write(fd, (void *) &msg, sizeof(msg));
        }
    }
    return NULL;
}


void *watch(void *arg)
{
    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
    //for(int i=0; i<MAX_CLIENTS_NUM; i++) observer[i].fd=-1;
    //for(int i=0; i<MAX_CLIENTS_NUM+2; i++) observer[i].events = POLLIN | POLLOUT | POLLRDHUP;

    //listening for connections on sockets
    int err = listen(observer[LOCAL].fd, MAX_CLIENTS_NUM);
    if (err != 0)
    {
        printf("Error while listening on local socket\n");
        perror(NULL);
    }

    err = listen(observer[NET].fd, MAX_CLIENTS_NUM);
    //second argument specifies the queue length for completely established sockets waiting to be accepted
    if (err != 0)
    {
        printf("Error while listening on network socket\n");
        perror(NULL);
    }


    int revents_num;
    int fd;
    struct message msg;
    char buf[1024];
    while (1)
    {
        fd = -1;
        for (int i = 0; i < MAX_CLIENTS_NUM + 2; i++) observer[i].events = POLLIN | POLLRDHUP;  // | POLLOUT?
        //POLLIN - there is data to read
        //POLLOUT - writing is now possible
        //POLLRDHUP - Stream socket peer closed connection, or shut down writing half of connection.

        revents_num = poll(observer, MAX_CLIENTS_NUM + 2, -1);  //TODO: change to + 2
        //is the number of structures which have nonzero revents fields (in other words, those descriptors with events or errors reported).


        if (revents_num == 0) continue;
        for (int i = 0; i < MAX_CLIENTS_NUM + 2; i++)
        {
            if (observer[i].revents & POLL_ERR)
            {
                printf("Poll error under index i = %i\n", i);
                continue;
            }
            //printf ("revents: %i\n", observer[i].revents);

            //if (observer[i].revents & POLLHUP) printf("POLLHUP on %i\n", i);
            //if (observer[i].revents & POLLNVAL) printf ("POLLNVAL on %i\n", i);       //fd not open
            //if (observer[i].revents & POLLIN) printf ("POLLIN on %i\n", i);

            if (i == NET || i == LOCAL)     //connection comming
            {
                if (observer[i].revents != 0)
                {
                    int accepted = 0;
                    int already_used = 0;
                    fd = observer[i].fd;
                    int handle = accept(fd, NULL, NULL);
                    read(handle, buf, sizeof(struct message));      //fd or handle?

                    printf("Handshake received\n");
                    msg = *(struct message *) buf;              //this will break strict-aliasing rules, but is necessary
                    if (strcmp("", msg.name) != 0)
                        for (int j = 0; j < MAX_CLIENTS_NUM; j++)
                        {
                            if (strcmp(clients_names[j], msg.name) == 0)
                            {
                                already_used = 1;
                                break;
                            }
                        }
                    else
                    {
                        printf("Noname client. Denying\n");
                        already_used = 1;
                        accepted = 0;
                    }
                    if (!already_used)
                        for (int j = 0; j < MAX_CLIENTS_NUM; j++)
                        {
                            if (observer[j].fd == -1)
                            {
                                accepted = 1;
                                observer[j].fd = handle;
                                strcpy(clients_names[j], msg.name);
                                break;
                            }
                        }
                    if (!accepted)
                    {
                        msg.type = DENIAL;
                        write(fd, (void *) &msg, sizeof(struct message));
                        shutdown(handle, SHUT_RDWR);
                        close(handle);
                    }
                }
            }
            else
            {

                if (observer[i].revents & POLLIN)  //there is data to read
                {


                    fd = observer[i].fd;
                    if (recv(fd, buf, sizeof(struct message), MSG_DONTWAIT) <= 0) continue;
                    //if (recv(fd, buf, sizeof(struct message), 0) <= 0)continue;
                    //if (read (fd, buf, sizeof(struct message)) <=0) continue;

                    //MSG_DONTWAIT is similar to O_NONBLOCK, but is a per-call option, whereas O_NONBLOCK is a
                    //setting on the open file description, which would affect all threads

                    msg = *(struct message *) buf;
                    if (msg.type == PING) pinged[i] = 0;

                    if (msg.type == ANSWER)
                        printf("Received answer to order nr %d. It says: %d\n", msg.order_num, msg.answer);
                    if (msg.type == EXIT)
                    {

                        //debug
                        printf("Some client closed.\n");


                        int err = shutdown(fd, SHUT_RDWR);
                        if (err != 0) printf("Shutting down failed!\n");
                        close(fd);
                        observer[i].fd = -1;
                        for (int j = 0; j < MAX_NAME_LEN; j++) clients_names[i][j] = '\0';
                        pinged[i] = 0;
                    }
                    if (msg.type == HANDSHAKE)
                    {
                        int accepted = 0;
                        int already_used = 0;
                        int handle = accept(fd, NULL, NULL);
                        for (int j = 0; j < MAX_CLIENTS_NUM; j++)
                        {
                            if (strcmp(clients_names[j], msg.name) == 0)
                            {
                                already_used = 1;
                                break;
                            }
                        }
                        if (!already_used)
                            for (int j = 0; j < MAX_CLIENTS_NUM; j++)
                            {
                                if (observer[j].fd == -1)
                                {
                                    accepted = 1;
                                    observer[j].fd = handle;
                                    strcpy(clients_names[j], msg.name);
                                    break;
                                }
                            }

                        if (!accepted)
                        {
                            msg.type = DENIAL;
                            write(fd, (void *) &msg, sizeof(struct message));
                            shutdown(handle, SHUT_RDWR);
                            close(handle);
                        }
                    }
                }

                if ((observer[i].revents & (POLLRDHUP & POLLHUP)) != 0)     //peer closed, closing socket
                {
                    fd = observer[i].fd;
                    shutdown(fd, SHUT_RDWR);
                    close(fd);
                    observer[i].fd = -1;
                    for (int j = 0; j < MAX_NAME_LEN; j++) clients_names[i][j] = '\0';
                    pinged[i] = 0;
                }
            }
        }
    }
    return NULL;
}


void *ping(void *arg)
{
    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
    int i = 0;
    struct message msg;
    msg.type = PING;

    while (1)
    {
        if (observer[i].fd != -1)
        {//maybe it would be good to save the fd in some variable?
            write(observer[i].fd, (void *) &msg, sizeof(struct message));
            pinged[i] = 1;
            sleep(4);       //giving client some time to respond
            if (pinged[i])  //while handling response to ping, ping[i] is zeroed, so if still 1 then the client is dead
            {
                printf("found dead client, index i = %i\n", i);
                shutdown(observer[i].fd, SHUT_RDWR);
                close(observer[i].fd);
                observer[i].fd = -1;
                strcpy(clients_names[i], "");
            }
        }
        i = (i + 1) % MAX_CLIENTS_NUM;
    }
    return NULL;
}