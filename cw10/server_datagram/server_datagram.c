#define _GNU_SOURCE //necessary to get POLLDRHUP definition
#pragma GCC diagnostic ignored "-Wstrict-aliasing"       //supressing this warning

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<signal.h>
#include<fcntl.h>
#include<unistd.h>
#include<errno.h>
#include<endian.h>
#include<netinet/in.h>
#include<sys/types.h>
/*POSIX.1 does not require the inclusion of <sys/types.h>, and this
  header file is not required on Linux.  However, some historical (BSD)
  implementations required this header file, and portable applications
  are probably wise to include it.
 */
#include<sys/socket.h>
#include<sys/un.h>
#include<arpa/inet.h>
#include<limits.h>
#include<poll.h>
#include<pthread.h>
#include <sys/stat.h>
#include "client-server_commons.h"

//-lpthread -lrt

const int NET = 1;
const int LOCAL = 0;

int net_socket;
int local_socket;
int port_num;
struct sockaddr_un local_addr;  /* Structure describing the address of an AF_LOCAL (aka AF_UNIX) socket.
                                 * Contains socket family (e.g. AF_UNIX */
struct sockaddr_in net_addr;    //* Structure describing an Internet socket address.  */
//struct pollfd observer[MAX_CLIENTS_NUM + 2];  /* Data structure describing a polling request.  */
//cant have observer, dgram is connectionless
struct pollfd observer[2];     //but can have monitor for incomming "connections"
pthread_t threads_ids[3];
char socket_path[SUNPATH_MAX_LEN];
char clients_names[MAX_CLIENTS_NUM][MAX_NAME_LEN];
struct sockaddr clients_addr[MAX_CLIENTS_NUM];


int order_num;
int pinged[MAX_CLIENTS_NUM];
int connection[MAX_CLIENTS_NUM];    //has type of "connection"


int pick_client();

void sighandler(int signo);

void *input_handler(void *arg);

void *watch(void *arg);

void *pinger(void *arg);


void handle_msg(int fd, struct message *msg, struct sockaddr client);

int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        printf("Not enough arguments. Please provide port number and socket path\n");
        exit(1);
    }
    //path=calloc(108, sizeof(char));
    //path = getcwd(path, 108);
    port_num = (int) strtol(argv[1], NULL, 10);
    strcpy(socket_path, argv[2]);
    signal(SIGINT, sighandler);
    order_num = 0;


    for (int i = 0; i < MAX_CLIENTS_NUM; i++)
    {
        strcpy(clients_names[i], "");
        connection[i] = -1;
    }


    //cant have observe coause dgram is conectionless

    //creating local socket
    int socket_fd = 0;
    if ((socket_fd = socket(AF_UNIX, SOCK_DGRAM, 0)) < 0) //The AF_UNIX (also known as AF_LOCAL) socket family is
    {                                             //used tocommunicate between processes on the same machine efficiently.
        printf("Error while creating local socket. Terminating\n");
        exit(1);
    }
    observer[LOCAL].fd = socket_fd;
    //creating net socket
    if ((socket_fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)  //AF_INET = IPv4 Internet protocols
    {
        printf("Error in creating local socket\n");
        exit(1);
    }
    observer[NET].fd = socket_fd;
    net_addr.sin_port = htons((uint16_t) port_num);    //sin_port = Port number.  */
    net_addr.sin_addr.s_addr = htonl(INADDR_ANY);       //struct in_addr sin_addr =  Internet address.


    printf("Server Internet address: %ld\n", (long) net_addr.sin_addr.s_addr);


    // Structure in_addr sin_addr contains only one field  */
    //The htonl() ("host to online" I suppose) function converts the unsigned integer argument from host byte order to network byte order.
    //The network byte order is defined to always be big-endian. Big-endian byte ordering places the most significant byte first
    //htons differs from htonl only with length of argument: htons takes unsigned short integerand htonl takes unsigned integer
    net_addr.sin_family = AF_INET;
    local_addr.sun_family = AF_UNIX;


    //binding names to sockets
    strncpy(local_addr.sun_path, (char *) socket_path, sizeof(local_addr.sun_path));
    if (bind(observer[LOCAL].fd, (struct sockaddr *) &local_addr, sizeof(struct sockaddr_un)) < 0)
    {
        /*about casting to sockaddr: The only purpose of this structure is to cast the structure pointer
        passed in addr in order to avoid compiler warnings.
         The actual structure passed for the addr argument will depend on the address family.
        */
        printf("Error while binding local\n");
        perror(NULL);
        sighandler(errno);
    }
    printf("Server Local path: %s\n", local_addr.sun_path);

    if (bind(observer[NET].fd, (struct sockaddr *) &net_addr, sizeof(struct sockaddr_in)) < 0)
    {
        printf("Error while binding network\n");
        perror(NULL);
        sighandler(errno);
    }

    //creating threads, because handling input and handling of the network must be in separate threads (it's a task condition)
    pthread_create(&threads_ids[0], NULL, watch, NULL);
    pthread_create(&threads_ids[1], NULL, input_handler, NULL);
    pthread_create(&threads_ids[2], NULL, pinger, NULL);
    for (int i = 0; i < 3; i++) pthread_join(threads_ids[i], NULL);
    unlink(socket_path);
    return 0;
}


int pick_client()
{
    int clients_counter = 0;
    int tmp = 0;
    for (int i = 0; i < MAX_CLIENTS_NUM; i++) if (strcmp(clients_names[i], "") != 0) clients_counter++;

    if (clients_counter == 0) return -1;

    if (clients_counter == 1)   //there is only one client, searching for it's index
        for (int i = 0; i < MAX_CLIENTS_NUM; i++)
            if (strcmp(clients_names[i], "") != 0) return i;

    //else picking one of clients randomly
    tmp = rand() % clients_counter;
    clients_counter = 0;
    for (int i = 0; i < MAX_CLIENTS_NUM; i++)
        if (strcmp(clients_names[i], "") != 0)
            if (tmp == clients_counter++) return i;

    return -1;
}


void sighandler(int signo)
{
    for (int i = 0; i < 3; i++) pthread_cancel(threads_ids[i]);
    close(observer[LOCAL].fd);
    close(observer[NET].fd);
    unlink(socket_path);
    exit(signo);
}


void *input_handler(void *arg)  //waits for input from keyboard and interprets it
{
    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
    char buf[100];
    char symbol;
    //char* tmp;
    char num1[30];
    char num2[30];
    unsigned int i, j;
    int client = -1;
    struct message msg;

    while (1)
    {
        fgets(buf, sizeof(buf), stdin);     //no unnecessary spaces pls!
        for (i = 0; i < strlen(buf) && buf[i] >= '0' && buf[i] <= '9'; i++)num1[i] = buf[i];
        num1[i] = '\0';
        symbol = buf[i++];
        for (j = i; j < strlen(buf) && buf[j] >= '0' && buf[j] <= '9'; j++)num2[j - i] = buf[j];
        num2[j - i] = '\0';
        if (!(symbol == '+' || symbol == '-' || symbol == '*' || symbol == '/')) continue;
        client = pick_client();     //client = index in observer tab
        if (client == -1) printf("No clients connected\n");
        else
        {

            msg.type = ORDER;
            msg.order_num = order_num++;
            msg.num1 = (int) strtol(num1, NULL, 10);
            msg.num2 = (int) strtol(num2, NULL, 10);
            msg.sign = symbol;
            if (connection[client] == NET) sendto(observer[NET].fd, (void*)&msg, sizeof(struct message), 0, &clients_addr[i], sizeof(struct sockaddr));
            else sendto(observer[LOCAL].fd, (void*)&msg, sizeof(struct message), 0, &clients_addr[i], sizeof(struct sockaddr));
        }
    }
    return NULL;
}


void *watch(void *arg)
{
    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
    //for(int i=0; i<MAX_CLIENTS_NUM; i++) observer[i].fd=-1;
    //for(int i=0; i<MAX_CLIENTS_NUM+2; i++) observer[i].events = POLLIN | POLLOUT | POLLRDHUP;

    //no listening for connections on sockets

    struct message msg;
    char buf[1000];
    struct sockaddr client;
    while (1)
    {
        observer[NET].events = POLLIN | POLLRDHUP;
        observer[LOCAL].events = POLLIN | POLLRDHUP;
        poll(observer, 2, -1);

        if (observer[LOCAL].revents != 0)
        {
            recvfrom(observer[LOCAL].fd, (void *) &buf, sizeof(struct message), 0, &client, NULL);
            //last parameter gives amount of data downloaded
            msg = *(struct message *) buf;

            handle_msg(observer[LOCAL].fd, &msg, client);
        }

        if (observer[NET].revents != 0)
        {
            recvfrom(observer[NET].fd, (void *) &buf, sizeof(struct message), 0, &client, NULL);
            //last parameter gives amount of data downloaded
            msg = *(struct message *) buf;
            handle_msg(observer[NET].fd, &msg, client);
        }

    }
    return NULL;
}


void handle_msg(int fd, struct message *msg, struct sockaddr client)
{
    if ((*msg).type == PING)
        for (int i = 0; i < MAX_CLIENTS_NUM; i++)
            if (strcmp((*msg).name, clients_names[i]) == 0) pinged[i] = 0;

    if ((*msg).type == ANSWER)
        printf("Received answer to order nr %d. It says: %d\n", (*msg).order_num, (*msg).answer);

    if ((*msg).type == EXIT)
    {

        //debug
        printf("Some client closed.\n");

        for (int i = 0; i < MAX_CLIENTS_NUM; i++)
        {
            if (strcmp((*msg).name, clients_names[i]) == 0)
            {
                pinged[i] = 0;
                strcpy(clients_names[i], "");
                connection[i] = -1;
            }
        }
    }
    if ((*msg).type == HANDSHAKE)
    {
        printf ("Handshake received\n");
        int accepted = 0;
        int already_used = 0;
        int handle = accept(fd, NULL, NULL);
        for (int j = 0; j < MAX_CLIENTS_NUM; j++)
        {
            if (strcmp(clients_names[j], (*msg).name) == 0)
            {
                already_used = 1;
                break;
            }
        }
        if (!already_used)
            for (int j = 0; j < MAX_CLIENTS_NUM; j++)
            {
                if (strcmp(clients_names[j],"") == 0)
                {
                    printf ("Accepting client\n");
                    accepted = 1;
                    strncpy(clients_addr[j].sa_data, client.sa_data, 14 * sizeof (char));
                    clients_addr[j].sa_family = client.sa_family;
                    strcpy(clients_names[j], (*msg).name);
                    connection[j] = 1;
                    break;
                }
            }

        if (!accepted)
        {
            (*msg).type = DENIAL;
            sendto(fd, (void*)&msg, sizeof(struct message), 0, &client, sizeof(struct sockaddr));
            close(handle);
        }
    }
}


void *pinger(void *arg)
{
    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
    int i = 0;
    struct message msg;
    msg.type = PING;

    while (1)
    {
        if (strcmp(clients_names[i], "") != 0)
        {
            if (connection[i] == NET)
            {
                //DEBUG
                int uploaded = (int) sendto(net_socket, (void *) &msg, sizeof(struct message), 0, &clients_addr[i], sizeof(struct sockaddr));

            }



            else
                sendto(local_socket, (void *) &msg, sizeof(struct message), 0, &clients_addr[i],
                       sizeof(struct sockaddr));
            write(observer[i].fd, (void *) &msg, sizeof(struct message));
            pinged[i] = 1;
            sleep(4);       //giving client some time to respond
            if (pinged[i])  //while handling response to ping, ping[i] is zeroed, so if still 1 then the client is dead
            {
                printf("found dead client, index i = %i\n", i);
                strcpy(clients_names[i], "");
                connection[i] = -1;
                close(observer[i].fd);
                observer[i].fd = -1;
                strcpy(clients_names[i], "");

                //sending denial in case that datagram wa just lost and client is in fact alive
                //and if it is alive, denial will make it die
                msg.type = DENIAL;
                if (connection[i] == NET)
                    sendto(net_socket, (void *) &msg, sizeof(struct message), 0, &clients_addr[i],
                           sizeof(struct sockaddr));
                else
                    sendto(local_socket, (void *) &msg, sizeof(struct message), 0, &clients_addr[i],
                           sizeof(struct sockaddr));
            }
        }
        i = (i + 1) % MAX_CLIENTS_NUM;
    }
    return NULL;
}