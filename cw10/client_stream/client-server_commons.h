#define SUNPATH_MAX_LEN 108     //On Linux sun_path is 108 bytes in size;
#define MAX_CLIENTS_NUM 16
#define MAX_NAME_LEN 16


#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<signal.h>
#include<fcntl.h>
#include<unistd.h>
#include<errno.h>
#include<endian.h>
#include<netinet/in.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<sys/un.h>
#include<arpa/inet.h>
#include<limits.h>
#include<poll.h>
#include<pthread.h>

enum type{
    PING = 20,
    ORDER,
    ANSWER,
    EXIT,
    HANDSHAKE,
    DENIAL
};


struct message{
    enum type type;
    int answer;
    //struct data stuff;
    int order_num;
    int num1;
    int num2;
    char sign;
    char name[MAX_NAME_LEN];
};