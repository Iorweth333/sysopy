//#define _GNU_SOURCE
#pragma GCC diagnostic ignored "-Wstrict-aliasing"  //supressing this warning
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<signal.h>
#include<fcntl.h>
#include<unistd.h>
#include<errno.h>
#include<endian.h>
#include<netinet/in.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<sys/un.h>
#include<arpa/inet.h>
#include<limits.h>
#include<poll.h>
#include<pthread.h>
#include "client-server_commons.h"

const int LOCAL = 0;
const int NET = 1;

char client_name[MAX_NAME_LEN];
char *server_addr;
int connection_type;
int port;
int socket_fd;



void cleaner ()
{
    struct message msg;
    msg.type = EXIT;
    int uploaded = (int) write(socket_fd, (void *) &msg, sizeof(struct message));
    printf("uploaded EXIT, %i bytes\n", uploaded);
    close(socket_fd);
}

void sighandler(int signo)
{
    cleaner();
    //shutdown(socket_fd, SHUT_RDWR);
    exit(signo);
}

int main(int argc, char *argv[])
{
    if (argc < 4)
    {
        printf("Not enough arguments. Please provide client's name, mean of connection (net or local) "
                       "and address of the server (with port number, if necessary).\n");
        exit(1);
    }

    strcpy(client_name, argv[1]);
    if (strcmp(argv[2], "net") == 0) connection_type = NET;
    else if (strcmp(argv[2], "local") == 0) connection_type = LOCAL;
    else
    {
        printf("Unknown type. Maybe a typoo? Please try again\n");
        exit(1);
    }

    if (connection_type == LOCAL && strlen(argv[3]) > SUNPATH_MAX_LEN)
    {
        printf("Too long path. Max length: %i. Terminating\n", SUNPATH_MAX_LEN);
        exit(1);
    }

    server_addr = malloc((strlen(argv[3]) + 1) * sizeof(char));  //+1 for null terminating sign
    strcpy(server_addr, argv[3]);

    if (connection_type == NET && argc == 5) port = (int) strtol(argv[4], NULL, 10);
    else port = -1;

    struct message msg;
    struct sockaddr_un local_addr;   //local address
    struct sockaddr_in net_addr;    //internet address

    //mambo-jumb finished, real stuff starts


    if (connection_type == NET)
    {
        if ((socket_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        {
            printf("Error while creating socket\n");
            perror(NULL);
            exit(errno);
        }
        //struct sockaddr_in addr;
        net_addr.sin_family = AF_INET;
        net_addr.sin_port = htons((uint16_t) port);
        inet_pton(AF_INET, server_addr,
                  &(net_addr.sin_addr));      //convert IPv4 and IPv6 addresses from text to binary form
        if ((connect(socket_fd, (struct sockaddr *) &net_addr, sizeof(struct sockaddr_in))) != 0)
        {
            printf("Error while establishing connection\n");
            perror(NULL);
            exit(errno);
        }
    }
    else    //type LOCAL
    {
        if ((socket_fd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0)
        {
            printf("Error while creating socket\n");
            perror(NULL);
            exit(errno);
        }
        local_addr.sun_family = AF_UNIX;
        strcpy(local_addr.sun_path, server_addr);

        printf ("path: %s\n", local_addr.sun_path);

        if ((connect(socket_fd, (struct sockaddr *) &local_addr, sizeof(struct sockaddr_un))) != 0)
        {
            printf("Error while establishing connection\n");
            perror(NULL);
            exit(errno);
        }
    }

    atexit(cleaner);

    msg.type = HANDSHAKE;
    strcpy(msg.name, client_name);
    sleep(1);   //giving server some time to accept or deny the connection
    write(socket_fd, (void *) &msg, sizeof(struct message));
    printf("Handshake sent\n");
    signal(SIGINT, sighandler); //sends message to server about exiting
    int loaded = 0;
    int uploaded = 0;
    char buf[1024];
    while (1)
    {

        loaded = (int) read(socket_fd, buf, sizeof(struct message));
        msg = *(struct message *) buf;      //this will break strict-aliasing rules, but is necessary

        printf("read %i bytes\n", loaded);

        if (!loaded)
        {
            printf("Server turned off\n");
            sighandler(0);
        }

        if (loaded == -1)
        {
            printf("Error while reading from socket. NOT terminating\n");
            perror(NULL);
        }

        else if (msg.type == PING)
        {
            printf("Received ping\n");
            uploaded = (int) write(socket_fd, (void *) &msg, sizeof(struct message));
            if (uploaded <= 0)
            {
                printf ("Error while uploading\n");
                perror(NULL);
            }
            //just responding to ping with also a ping
            //printf("uploaded ping %i bytes\n", uploaded);
        }
        else if (msg.type == ORDER)
        {
            msg.type = ANSWER;
            if (msg.sign == '+') msg.answer = msg.num1 + msg.num2;

            if (msg.sign == '-') msg.answer = msg.num1 - msg.num2;

            if (msg.sign == '*') msg.answer = msg.num1 * msg.num2;

            if (msg.sign == '/') msg.answer = msg.num1 / msg.num2;


            printf("Order number: %d. Answering: %d\n", msg.order_num, msg.answer);
            //printing in client. Its' always good to know that something came in
            uploaded = (int) write(socket_fd, (void *) &msg, sizeof(struct message));    //sending answer
            printf("uploaded answer %i bytes\n", uploaded);
        }
        else if (msg.type == DENIAL)
        {
            printf("Server denied client. How rude!\n");
            shutdown(socket_fd, SHUT_RDWR);
            exit(1);
        }
    }
    return 0;
}