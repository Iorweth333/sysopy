#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>


#define NUMBERS_SCOPE 21    //maximum number will be 20

int *arr;
long arr_size;

pthread_t *readers_ids; //threads identifiers
pthread_t *writers_ids;

long readers_num;
long reading_quantity;      //quantity is not particularly preciseful name, but I couldn't come up with anythin else

long writers_num;
long writing_quantity;

int descriptive = 0;        //for -i flag

sem_t reader_sem;
sem_t writer_sem;
sem_t start_sem;
//variables for mutexes
int checker = 0;
int actual_writers = 0;     //of course max 1 at the time!
int actual_readers = 0;


pthread_mutex_t ready_steady;                 //the point is that threads must work simultaneously
pthread_mutex_t mutex;                          //scheme taken from auxiliary materials written by one of tutors
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
pthread_cond_t start_cond = PTHREAD_COND_INITIALIZER;

int arguments_good(int argc, char **argv);

int is_number(char const *arg);

void *reader_thread(void *arg);

void *writer_thread(void *arg);

void arr_init(long size);

void cleaner();

int main(int argc, char **argv)
{

    if (!arguments_good(argc, argv))
    {
        printf("Inappropriate arguments. Please provide: \n"
                       "how many readers \n"
                       "how much do they read"
                       "how many writers \n"
                       "how much do they write"
                       "size of array \n"
                       "(optionally) for extended communitacion flag -i  \n");
        exit(1);
    }

    readers_num = strtol(argv[1], NULL, 10);
    reading_quantity = strtol(argv[2], NULL, 10);
    writers_num = strtol(argv[3], NULL, 10);
    writing_quantity = strtol(argv[4], NULL, 10);
    arr_size = strtol(argv[5], NULL, 10);
    arr_init(arr_size);
    printf("Time format: seconds (s), microseconds (us)\n");

    if (argv[6] != NULL) descriptive = 1;   //


    //initialising semaphores with ready-to-take value
    //If pshared has the value 0, then the semaphore is shared between the
    //threads of a process, and should be located at some address that is
    //visible to all threads (e.g., a global variable, or a variable
    //allocated dynamically on the heap).
    if (sem_init(&reader_sem, 0, 1) == -1)
    {
        printf("Error while creating reader_sem");
        exit(1);
    }

    if (sem_init(&writer_sem, 0, 1) == -1)
    {
        printf("Error while creating writer_sem");
        exit(1);
    }
    //initalising start_sem with unready_to_take value
    if (sem_init(&start_sem, 0, 0) == -1)
    {
        printf("Error while creating start_sem");
        exit(1);
    }



    //RUNNING THREADS
    //FAVOR READERS

    //allocating arrays for thread identifiers
    readers_ids = malloc(sizeof(pthread_t) * readers_num);
    writers_ids = malloc(sizeof(pthread_t) * writers_num);

    int *dividers = malloc(sizeof(int) * readers_num);

    for (int i = 0; i < readers_num; i++)
        dividers[i] = rand() % (NUMBERS_SCOPE - 1) + 1;


    for (int i = 0; i < readers_num; i++)
    {
        if (pthread_create(&readers_ids[i], NULL, reader_thread, &dividers[i]) != 0) // reader takes divider
        {
            printf("Error while creating reader thread.\n");
            perror(NULL);
            exit(1);
        }
    }
    for (int i = 0; i < writers_num; i++)
    {
        if (pthread_create(&writers_ids[i], NULL, writer_thread, NULL) != 0)
        {
            printf("Error while creating writer thread.\n");
            perror(NULL);
            exit(1);
        }
    }

    struct timespec tm_spec;
    clock_gettime(CLOCK_MONOTONIC, &tm_spec);
    printf("%d s, %d us: START\n",
           (int) tm_spec.tv_sec, (int) tm_spec.tv_nsec / 1000);

    sem_post(&start_sem);       //GO!


    for (int i = 0; i < readers_num; i++)
    {
        if (pthread_join(readers_ids[i], NULL) != 0)
        {
            printf("Error while joining readers' threads.\n");
            perror(NULL);
            exit(1);
        }
    }

    for (int i = 0; i < writers_num; i++)
    {
        if (pthread_join(writers_ids[i], NULL) != 0)
        {
            printf("Error while joining writers' threads.\n");
            perror(NULL);
            exit(1);
        }
    }


    cleaner();
    return 0;
}

void cleaner()
{
    free(arr);
    free(readers_ids);
    free(writers_ids);

    sem_close(&reader_sem);
    sem_close(&writer_sem);
    sem_close(&start_sem);
}

//FAVOR READERS
void *reader_thread(void *arg)
{
    sem_wait(&start_sem);
    sem_post(&start_sem);   //must allow others too

    int divider = *(int *) arg;

    struct timespec tm_spec;
    clock_gettime(CLOCK_MONOTONIC, &tm_spec);
    printf("%d s, %d us: reader with ID: %lu STARTS\n",
           (int) tm_spec.tv_sec, (int) tm_spec.tv_nsec / 1000, pthread_self());

    for (int i = 0; i < reading_quantity; i++)
    {
        sem_wait(&reader_sem);

        actual_readers++;
        if (actual_readers == 1)
            sem_wait(&writer_sem); //  equivalet no pthread_cond_wait but preffering readers this time

        sem_post(&reader_sem);


        int counter = 0;

        for (int j = 0; j < arr_size; j++)
        {
            if (arr[j] % divider == 0)
            {
                counter++;
                if (descriptive)
                {
                    clock_gettime(CLOCK_MONOTONIC, &tm_spec);

                    printf("%d s, %d us: reader with ID: %lu reports: index: %d, value: %d, divider: %d\n",
                           (int) tm_spec.tv_sec, (int) tm_spec.tv_nsec / 1000, pthread_self(), j, arr[j], divider);
                }
            }
        }
        clock_gettime(CLOCK_MONOTONIC, &tm_spec);
        printf("%d s, %d us: reader with ID: %lu reports: %d number(s) divisible by %d\n",
               (int) tm_spec.tv_sec, (int) tm_spec.tv_nsec / 1000, pthread_self(), counter, divider);


        sem_wait(&reader_sem);
        if (--actual_readers == 0) sem_post(&writer_sem); //if no readers reading, allowing writers
        sem_post(&reader_sem);

        //maybe sleep(1)?
    }

    return NULL;
}


void *writer_thread(void *arg)
{
    sem_wait(&start_sem);
    sem_post(&start_sem);   //must allow others too

    struct timespec tm_spec;
    clock_gettime(CLOCK_MONOTONIC, &tm_spec);
    printf("%d s, %d us: Writer with ID:  %lu STARTS\n",
           (int) tm_spec.tv_sec, (int) tm_spec.tv_nsec / 1000, pthread_self());


    for (int i = 0; i < writing_quantity; i++)
    {
        sem_wait(&writer_sem);

        long how_many, index;
        int value;
        how_many = (rand() % arr_size) + 1;


        clock_gettime(CLOCK_MONOTONIC, &tm_spec);
        printf("%d s, %d us: Writer with ID:  %lu starts writing.\n",
               (int) tm_spec.tv_sec, (int) tm_spec.tv_nsec / 1000, pthread_self());

        for (int j = 0; j < how_many; j++)
        {
            index = rand() % arr_size;
            value = rand() % NUMBERS_SCOPE;

            arr[index] = value;
            if (descriptive)
            {
                clock_gettime(CLOCK_MONOTONIC, &tm_spec);
                printf("%d s, %d us: Writer with ID: %lu changed number under index %ld "
                               "to %d\n", (int) tm_spec.tv_sec, (int) tm_spec.tv_nsec / 1000, pthread_self(), index,
                       value);
            }
        }

        sem_post(&writer_sem);
        // sleep(1) maybe ?
    }

    return NULL;
}

void arr_init(long size)
{
    arr_size = size;
    arr = malloc(arr_size * sizeof(int));

    printf("Printing array: \n");
    for (int i = 0; i < arr_size; i++)
    {
        arr[i] = rand() % NUMBERS_SCOPE;
        printf("%d ", arr[i]);
    }
    printf("\n");
}

int arguments_good(int argc, char **argv)
{
    if ((argc < 6) || (argc > 7)) return 0;

    for (int i = 1; i < 6; i++) if (!is_number(argv[i])) return 0;

    if (argc == 7 && (strcmp(argv[6], "-i") != 0)) return 0;

    return 1;

}

int is_number(char const *arg)
{
    int i = 0;
    while (arg[i] != '\0')
        if (arg[i] >= '0' && arg[i] <= '9') i++;
        else return 0;
    return 1;
}