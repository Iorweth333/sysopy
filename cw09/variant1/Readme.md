My observation: very much depends on which kind of threads do we create first. 
This time i favor Readers, so they are created first



examplary run (compiled without optimisation flags- default set I guess- but in this variant it matters than in the other)

[karolb@localhost variant1]$ ./readers-writers1 50 3 50 3 10
Printing array: 
1 4 9 19 8 10 10 9 15 10 
Time format: seconds (s), microseconds (us)
163376 s, 818025 us: START
163376 s, 818244 us: Writer with ID:  139715031074560 STARTS
163376 s, 818260 us: Writer with ID:  139715031074560 starts writing.
163376 s, 818262 us: Writer with ID:  139715031074560 starts writing.
163376 s, 818264 us: Writer with ID:  139715031074560 starts writing.
163376 s, 818317 us: reader with ID: 139715845166848 STARTS
163376 s, 818398 us: reader with ID: 139715845166848 reports: 1 number(s) divisible by 8
163376 s, 818401 us: reader with ID: 139715845166848 reports: 1 number(s) divisible by 8
163376 s, 818403 us: reader with ID: 139715845166848 reports: 1 number(s) divisible by 8
163376 s, 818522 us: reader with ID: 139715836774144 STARTS
163376 s, 818527 us: reader with ID: 139715836774144 reports: 0 number(s) divisible by 11
163376 s, 818529 us: reader with ID: 139715836774144 reports: 0 number(s) divisible by 11
163376 s, 818530 us: reader with ID: 139715836774144 reports: 0 number(s) divisible by 11
163376 s, 818549 us: reader with ID: 139715853559552 STARTS
163376 s, 818560 us: reader with ID: 139715853559552 reports: 4 number(s) divisible by 3
163376 s, 818563 us: reader with ID: 139715853559552 reports: 4 number(s) divisible by 3
163376 s, 818566 us: reader with ID: 139715853559552 reports: 4 number(s) divisible by 3
163376 s, 818664 us: reader with ID: 139715819988736 STARTS
163376 s, 818719 us: reader with ID: 139715819988736 reports: 2 number(s) divisible by 4
163376 s, 818722 us: reader with ID: 139715819988736 reports: 2 number(s) divisible by 4
163376 s, 818723 us: reader with ID: 139715819988736 reports: 2 number(s) divisible by 4
163376 s, 818853 us: Writer with ID:  139715022681856 STARTS
163376 s, 818859 us: Writer with ID:  139715022681856 starts writing.
163376 s, 818861 us: Writer with ID:  139715022681856 starts writing.
163376 s, 818864 us: Writer with ID:  139715022681856 starts writing.
163376 s, 818929 us: reader with ID: 139715803203328 STARTS
163376 s, 818934 us: reader with ID: 139715803203328 reports: 10 number(s) divisible by 1
163376 s, 818936 us: reader with ID: 139715803203328 reports: 10 number(s) divisible by 1
163376 s, 818938 us: reader with ID: 139715803203328 reports: 10 number(s) divisible by 1
163376 s, 818667 us: reader with ID: 139715828381440 STARTS
163376 s, 818956 us: reader with ID: 139715828381440 reports: 0 number(s) divisible by 20
163376 s, 818958 us: reader with ID: 139715828381440 reports: 0 number(s) divisible by 20
163376 s, 818959 us: reader with ID: 139715828381440 reports: 0 number(s) divisible by 20
163376 s, 819014 us: reader with ID: 139715811596032 STARTS
163376 s, 819023 us: reader with ID: 139715811596032 reports: 4 number(s) divisible by 7
163376 s, 819025 us: reader with ID: 139715811596032 reports: 4 number(s) divisible by 7
163376 s, 819027 us: reader with ID: 139715811596032 reports: 4 number(s) divisible by 7
163376 s, 819190 us: reader with ID: 139715794810624 STARTS
163376 s, 819196 us: reader with ID: 139715794810624 reports: 4 number(s) divisible by 7
163376 s, 819197 us: reader with ID: 139715794810624 reports: 4 number(s) divisible by 7
163376 s, 819199 us: reader with ID: 139715794810624 reports: 4 number(s) divisible by 7
163376 s, 819410 us: reader with ID: 139715786417920 STARTS
163376 s, 819418 us: reader with ID: 139715786417920 reports: 2 number(s) divisible by 13
163376 s, 819421 us: reader with ID: 139715786417920 reports: 2 number(s) divisible by 13
163376 s, 819423 us: reader with ID: 139715786417920 reports: 2 number(s) divisible by 13
163376 s, 819492 us: reader with ID: 139715778025216 STARTS
163376 s, 819495 us: reader with ID: 139715778025216 reports: 0 number(s) divisible by 17
163376 s, 819497 us: reader with ID: 139715778025216 reports: 0 number(s) divisible by 17
163376 s, 819498 us: reader with ID: 139715778025216 reports: 0 number(s) divisible by 17
163376 s, 819542 us: reader with ID: 139715769632512 STARTS
163376 s, 819548 us: reader with ID: 139715769632512 reports: 1 number(s) divisible by 12
163376 s, 819550 us: reader with ID: 139715769632512 reports: 1 number(s) divisible by 12
163376 s, 819553 us: reader with ID: 139715769632512 reports: 1 number(s) divisible by 12
163376 s, 819629 us: reader with ID: 139715761239808 STARTS
163376 s, 819632 us: reader with ID: 139715761239808 reports: 0 number(s) divisible by 9
163376 s, 819634 us: reader with ID: 139715761239808 reports: 0 number(s) divisible by 9
163376 s, 819635 us: reader with ID: 139715761239808 reports: 0 number(s) divisible by 9
163376 s, 819683 us: reader with ID: 139715752847104 STARTS
163376 s, 819691 us: reader with ID: 139715752847104 reports: 1 number(s) divisible by 8
163376 s, 819693 us: reader with ID: 139715752847104 reports: 1 number(s) divisible by 8
163376 s, 819694 us: reader with ID: 139715752847104 reports: 1 number(s) divisible by 8
163376 s, 819769 us: reader with ID: 139715744454400 STARTS
163376 s, 819777 us: reader with ID: 139715744454400 reports: 0 number(s) divisible by 10
163376 s, 819779 us: reader with ID: 139715744454400 reports: 0 number(s) divisible by 10
163376 s, 819781 us: reader with ID: 139715744454400 reports: 0 number(s) divisible by 10
163376 s, 819823 us: reader with ID: 139715736061696 STARTS
163376 s, 819828 us: reader with ID: 139715736061696 reports: 2 number(s) divisible by 3
163376 s, 819829 us: reader with ID: 139715736061696 reports: 2 number(s) divisible by 3
163376 s, 819830 us: reader with ID: 139715736061696 reports: 2 number(s) divisible by 3
163376 s, 819914 us: reader with ID: 139715727668992 STARTS
163376 s, 819919 us: reader with ID: 139715727668992 reports: 0 number(s) divisible by 11
163376 s, 819921 us: reader with ID: 139715727668992 reports: 0 number(s) divisible by 11
163376 s, 819922 us: reader with ID: 139715727668992 reports: 0 number(s) divisible by 11
163376 s, 819931 us: reader with ID: 139715710883584 STARTS
163376 s, 819937 us: reader with ID: 139715710883584 reports: 2 number(s) divisible by 4
163376 s, 819940 us: reader with ID: 139715710883584 reports: 2 number(s) divisible by 4
163376 s, 819942 us: reader with ID: 139715710883584 reports: 2 number(s) divisible by 4
163376 s, 819989 us: reader with ID: 139715719276288 STARTS
163376 s, 819993 us: reader with ID: 139715719276288 reports: 2 number(s) divisible by 3
163376 s, 819994 us: reader with ID: 139715719276288 reports: 2 number(s) divisible by 3
163376 s, 819996 us: reader with ID: 139715719276288 reports: 2 number(s) divisible by 3
163376 s, 820475 us: reader with ID: 139715694098176 STARTS
163376 s, 820481 us: reader with ID: 139715694098176 reports: 0 number(s) divisible by 16
163376 s, 820483 us: reader with ID: 139715694098176 reports: 0 number(s) divisible by 16
163376 s, 820485 us: reader with ID: 139715694098176 reports: 0 number(s) divisible by 16
163376 s, 820534 us: reader with ID: 139715702490880 STARTS
163376 s, 820539 us: reader with ID: 139715702490880 reports: 1 number(s) divisible by 8
163376 s, 820541 us: reader with ID: 139715702490880 reports: 1 number(s) divisible by 8
163376 s, 820542 us: reader with ID: 139715702490880 reports: 1 number(s) divisible by 8
163376 s, 820605 us: reader with ID: 139715677312768 STARTS
163376 s, 820610 us: reader with ID: 139715677312768 reports: 2 number(s) divisible by 3
163376 s, 820612 us: reader with ID: 139715677312768 reports: 2 number(s) divisible by 3
163376 s, 820614 us: reader with ID: 139715677312768 reports: 2 number(s) divisible by 3
163376 s, 820658 us: reader with ID: 139715668920064 STARTS
163376 s, 820664 us: reader with ID: 139715668920064 reports: 2 number(s) divisible by 3
163376 s, 820666 us: reader with ID: 139715668920064 reports: 2 number(s) divisible by 3
163376 s, 820667 us: reader with ID: 139715668920064 reports: 2 number(s) divisible by 3
163376 s, 820708 us: reader with ID: 139715660527360 STARTS
163376 s, 820713 us: reader with ID: 139715660527360 reports: 0 number(s) divisible by 19
163376 s, 820812 us: reader with ID: 139715660527360 reports: 0 number(s) divisible by 19
163376 s, 820815 us: reader with ID: 139715660527360 reports: 0 number(s) divisible by 19
163376 s, 821021 us: reader with ID: 139715685705472 STARTS
163376 s, 821030 us: reader with ID: 139715685705472 reports: 0 number(s) divisible by 10
163376 s, 821032 us: reader with ID: 139715685705472 reports: 0 number(s) divisible by 10
163376 s, 821034 us: reader with ID: 139715685705472 reports: 0 number(s) divisible by 10
163376 s, 821098 us: reader with ID: 139715635349248 STARTS
163376 s, 821105 us: reader with ID: 139715635349248 reports: 3 number(s) divisible by 14
163376 s, 821109 us: reader with ID: 139715635349248 reports: 3 number(s) divisible by 14
163376 s, 821111 us: reader with ID: 139715635349248 reports: 3 number(s) divisible by 14
163376 s, 821221 us: reader with ID: 139715652134656 STARTS
163376 s, 821228 us: reader with ID: 139715652134656 reports: 0 number(s) divisible by 10
163376 s, 821231 us: reader with ID: 139715652134656 reports: 0 number(s) divisible by 10
163376 s, 821233 us: reader with ID: 139715652134656 reports: 0 number(s) divisible by 10
163376 s, 821308 us: reader with ID: 139715643741952 STARTS
163376 s, 821313 us: reader with ID: 139715643741952 reports: 1 number(s) divisible by 8
163376 s, 821314 us: reader with ID: 139715643741952 reports: 1 number(s) divisible by 8
163376 s, 821316 us: reader with ID: 139715643741952 reports: 1 number(s) divisible by 8
163376 s, 821409 us: reader with ID: 139715618563840 STARTS
163376 s, 821418 us: reader with ID: 139715618563840 reports: 1 number(s) divisible by 12
163376 s, 821420 us: reader with ID: 139715618563840 reports: 1 number(s) divisible by 12
163376 s, 821422 us: reader with ID: 139715618563840 reports: 1 number(s) divisible by 12
163376 s, 821444 us: reader with ID: 139715626956544 STARTS
163376 s, 821448 us: reader with ID: 139715626956544 reports: 0 number(s) divisible by 17
163376 s, 821450 us: reader with ID: 139715626956544 reports: 0 number(s) divisible by 17
163376 s, 821452 us: reader with ID: 139715626956544 reports: 0 number(s) divisible by 17
163376 s, 821604 us: reader with ID: 139715593385728 STARTS
163376 s, 821608 us: reader with ID: 139715593385728 reports: 3 number(s) divisible by 14
163376 s, 821609 us: reader with ID: 139715593385728 reports: 3 number(s) divisible by 14
163376 s, 821611 us: reader with ID: 139715593385728 reports: 3 number(s) divisible by 14
163376 s, 821863 us: reader with ID: 139715584993024 STARTS
163376 s, 821869 us: reader with ID: 139715584993024 reports: 5 number(s) divisible by 2
163376 s, 821871 us: reader with ID: 139715584993024 reports: 5 number(s) divisible by 2
163376 s, 821874 us: reader with ID: 139715584993024 reports: 5 number(s) divisible by 2
163376 s, 821893 us: reader with ID: 139715576600320 STARTS
163376 s, 821897 us: reader with ID: 139715576600320 reports: 0 number(s) divisible by 20
163376 s, 821899 us: reader with ID: 139715576600320 reports: 0 number(s) divisible by 20
163376 s, 821900 us: reader with ID: 139715576600320 reports: 0 number(s) divisible by 20
163376 s, 821923 us: reader with ID: 139715551422208 STARTS
163376 s, 821925 us: reader with ID: 139715551422208 reports: 0 number(s) divisible by 19
163376 s, 821927 us: reader with ID: 139715551422208 reports: 0 number(s) divisible by 19
163376 s, 821928 us: reader with ID: 139715551422208 reports: 0 number(s) divisible by 19
163376 s, 821476 us: reader with ID: 139715610171136 STARTS
163376 s, 821989 us: reader with ID: 139715568207616 STARTS
163376 s, 821993 us: reader with ID: 139715568207616 reports: 1 number(s) divisible by 5
163376 s, 821996 us: reader with ID: 139715568207616 reports: 1 number(s) divisible by 5
163376 s, 821998 us: reader with ID: 139715568207616 reports: 1 number(s) divisible by 5
163376 s, 822015 us: reader with ID: 139715543029504 STARTS
163376 s, 822019 us: reader with ID: 139715543029504 reports: 1 number(s) divisible by 5
163376 s, 822021 us: reader with ID: 139715543029504 reports: 1 number(s) divisible by 5
163376 s, 822024 us: reader with ID: 139715543029504 reports: 1 number(s) divisible by 5
163376 s, 822030 us: reader with ID: 139715534636800 STARTS
163376 s, 822035 us: reader with ID: 139715534636800 reports: 0 number(s) divisible by 16
163376 s, 822037 us: reader with ID: 139715534636800 reports: 0 number(s) divisible by 16
163376 s, 822038 us: reader with ID: 139715526244096 STARTS
163376 s, 821527 us: reader with ID: 139715601778432 STARTS
163376 s, 822049 us: reader with ID: 139715601778432 reports: 0 number(s) divisible by 10
163376 s, 822052 us: reader with ID: 139715509458688 STARTS
163376 s, 822050 us: reader with ID: 139715526244096 reports: 0 number(s) divisible by 11
163376 s, 822061 us: reader with ID: 139715526244096 reports: 0 number(s) divisible by 11
163376 s, 822063 us: reader with ID: 139715526244096 reports: 0 number(s) divisible by 11
163376 s, 822181 us: reader with ID: 139715517851392 STARTS
163376 s, 822187 us: reader with ID: 139715517851392 reports: 3 number(s) divisible by 14
163376 s, 822190 us: reader with ID: 139715517851392 reports: 3 number(s) divisible by 14
163376 s, 822192 us: reader with ID: 139715517851392 reports: 3 number(s) divisible by 14
163376 s, 822276 us: reader with ID: 139715492673280 STARTS
163376 s, 822282 us: reader with ID: 139715492673280 reports: 10 number(s) divisible by 1
163376 s, 822284 us: reader with ID: 139715492673280 reports: 10 number(s) divisible by 1
163376 s, 822287 us: reader with ID: 139715492673280 reports: 10 number(s) divisible by 1
163376 s, 822056 us: reader with ID: 139715509458688 reports: 4 number(s) divisible by 7
163376 s, 822329 us: reader with ID: 139715509458688 reports: 4 number(s) divisible by 7
163376 s, 822331 us: reader with ID: 139715509458688 reports: 4 number(s) divisible by 7
163376 s, 822341 us: reader with ID: 139715501065984 STARTS
163376 s, 822054 us: reader with ID: 139715601778432 reports: 0 number(s) divisible by 10
163376 s, 821999 us: reader with ID: 139715610171136 reports: 2 number(s) divisible by 3
163376 s, 822360 us: reader with ID: 139715610171136 reports: 2 number(s) divisible by 3
163376 s, 822362 us: reader with ID: 139715610171136 reports: 2 number(s) divisible by 3
163376 s, 822404 us: reader with ID: 139715601778432 reports: 0 number(s) divisible by 10
163376 s, 822407 us: reader with ID: 139715484280576 STARTS
163376 s, 822414 us: reader with ID: 139715484280576 reports: 0 number(s) divisible by 17
163376 s, 822439 us: reader with ID: 139715475887872 STARTS
163376 s, 822444 us: reader with ID: 139715475887872 reports: 3 number(s) divisible by 14
163376 s, 822448 us: reader with ID: 139715559814912 STARTS
163376 s, 822451 us: reader with ID: 139715559814912 reports: 0 number(s) divisible by 18
163376 s, 822453 us: reader with ID: 139715559814912 reports: 0 number(s) divisible by 18
163376 s, 822455 us: reader with ID: 139715559814912 reports: 0 number(s) divisible by 18
163376 s, 822460 us: reader with ID: 139715459102464 STARTS
163376 s, 822464 us: reader with ID: 139715459102464 reports: 0 number(s) divisible by 11
163376 s, 822467 us: reader with ID: 139715459102464 reports: 0 number(s) divisible by 11
163376 s, 822469 us: reader with ID: 139715459102464 reports: 0 number(s) divisible by 11
163376 s, 822039 us: reader with ID: 139715534636800 reports: 0 number(s) divisible by 16
163376 s, 822533 us: reader with ID: 139715450709760 STARTS
163376 s, 822537 us: reader with ID: 139715450709760 reports: 0 number(s) divisible by 17
163376 s, 822539 us: reader with ID: 139715450709760 reports: 0 number(s) divisible by 17
163376 s, 822540 us: reader with ID: 139715450709760 reports: 0 number(s) divisible by 17
163376 s, 822345 us: reader with ID: 139715501065984 reports: 1 number(s) divisible by 12
163376 s, 822545 us: reader with ID: 139715501065984 reports: 1 number(s) divisible by 12
163376 s, 822546 us: reader with ID: 139715501065984 reports: 1 number(s) divisible by 12
163376 s, 822581 us: reader with ID: 139715467495168 STARTS
163376 s, 822583 us: reader with ID: 139715467495168 reports: 2 number(s) divisible by 3
163376 s, 822585 us: reader with ID: 139715467495168 reports: 2 number(s) divisible by 3
163376 s, 822586 us: reader with ID: 139715467495168 reports: 2 number(s) divisible by 3
163376 s, 822635 us: reader with ID: 139715475887872 reports: 3 number(s) divisible by 14
163376 s, 822640 us: reader with ID: 139715475887872 reports: 3 number(s) divisible by 14
163376 s, 822650 us: reader with ID: 139715442317056 STARTS
163376 s, 822655 us: reader with ID: 139715442317056 reports: 5 number(s) divisible by 2
163376 s, 822658 us: reader with ID: 139715442317056 reports: 5 number(s) divisible by 2
163376 s, 822659 us: reader with ID: 139715442317056 reports: 5 number(s) divisible by 2
163376 s, 822714 us: Writer with ID:  139715425531648 STARTS
163376 s, 822836 us: Writer with ID:  139715433924352 STARTS
163376 s, 823015 us: Writer with ID:  139715417138944 STARTS
163376 s, 823253 us: Writer with ID:  139715408746240 STARTS
163376 s, 823267 us: Writer with ID:  139715400353536 STARTS
163376 s, 822445 us: reader with ID: 139715484280576 reports: 0 number(s) divisible by 17
163376 s, 823298 us: reader with ID: 139715484280576 reports: 0 number(s) divisible by 17
163376 s, 823342 us: Writer with ID:  139715433924352 starts writing.
163376 s, 823349 us: Writer with ID:  139715433924352 starts writing.
163376 s, 823352 us: Writer with ID:  139715433924352 starts writing.
163376 s, 823394 us: Writer with ID:  139715408746240 starts writing.
163376 s, 823399 us: Writer with ID:  139715408746240 starts writing.
163376 s, 823401 us: Writer with ID:  139715408746240 starts writing.
163376 s, 823455 us: Writer with ID:  139715383568128 STARTS
163376 s, 823462 us: Writer with ID:  139715383568128 starts writing.
163376 s, 823465 us: Writer with ID:  139715383568128 starts writing.
163376 s, 823467 us: Writer with ID:  139715383568128 starts writing.
163376 s, 823613 us: Writer with ID:  139715375175424 STARTS
163376 s, 823619 us: Writer with ID:  139715375175424 starts writing.
163376 s, 823622 us: Writer with ID:  139715375175424 starts writing.
163376 s, 823625 us: Writer with ID:  139715375175424 starts writing.
163376 s, 823396 us: Writer with ID:  139715391960832 STARTS
163376 s, 823645 us: Writer with ID:  139715391960832 starts writing.
163376 s, 823648 us: Writer with ID:  139715391960832 starts writing.
163376 s, 823650 us: Writer with ID:  139715391960832 starts writing.
163376 s, 823660 us: Writer with ID:  139715417138944 starts writing.
163376 s, 823663 us: Writer with ID:  139715417138944 starts writing.
163376 s, 823665 us: Writer with ID:  139715417138944 starts writing.
163376 s, 823680 us: Writer with ID:  139715425531648 starts writing.
163376 s, 823683 us: Writer with ID:  139715425531648 starts writing.
163376 s, 823686 us: Writer with ID:  139715425531648 starts writing.
163376 s, 823706 us: Writer with ID:  139715366782720 STARTS
163376 s, 823836 us: Writer with ID:  139715349997312 STARTS
163376 s, 823907 us: Writer with ID:  139715366782720 starts writing.
163376 s, 823912 us: Writer with ID:  139715366782720 starts writing.
163376 s, 823770 us: Writer with ID:  139715358390016 STARTS
163376 s, 824178 us: Writer with ID:  139715366782720 starts writing.
163376 s, 824303 us: Writer with ID:  139715316426496 STARTS
163376 s, 824309 us: Writer with ID:  139715316426496 starts writing.
163376 s, 824312 us: Writer with ID:  139715316426496 starts writing.
163376 s, 824314 us: Writer with ID:  139715316426496 starts writing.
163376 s, 823837 us: Writer with ID:  139715333211904 STARTS
163376 s, 824322 us: Writer with ID:  139715333211904 starts writing.
163376 s, 824324 us: Writer with ID:  139715333211904 starts writing.
163376 s, 824427 us: Writer with ID:  139715324819200 STARTS
163376 s, 824489 us: Writer with ID:  139715333211904 starts writing.
163376 s, 824805 us: Writer with ID:  139715291248384 STARTS
163376 s, 824883 us: Writer with ID:  139715274462976 STARTS
163376 s, 824889 us: Writer with ID:  139715274462976 starts writing.
163376 s, 824517 us: Writer with ID:  139715308033792 STARTS
163376 s, 824943 us: Writer with ID:  139715282855680 STARTS
163376 s, 824948 us: Writer with ID:  139715282855680 starts writing.
163376 s, 824961 us: Writer with ID:  139715308033792 starts writing.
163376 s, 824995 us: Writer with ID:  139715266070272 STARTS
163376 s, 825025 us: Writer with ID:  139715249284864 STARTS
163376 s, 824619 us: Writer with ID:  139715299641088 STARTS
163376 s, 825465 us: Writer with ID:  139715240892160 STARTS
163376 s, 824998 us: Writer with ID:  139715274462976 starts writing.
163376 s, 825506 us: Writer with ID:  139715400353536 starts writing.
163376 s, 825525 us: Writer with ID:  139715232499456 STARTS
163376 s, 825530 us: Writer with ID:  139715232499456 starts writing.
163376 s, 825555 us: Writer with ID:  139715274462976 starts writing.
163376 s, 825561 us: Writer with ID:  139715358390016 starts writing.
163376 s, 825565 us: Writer with ID:  139715266070272 starts writing.
163376 s, 825588 us: Writer with ID:  139715240892160 starts writing.
163376 s, 825615 us: Writer with ID:  139715358390016 starts writing.
163376 s, 825681 us: Writer with ID:  139715215714048 STARTS
163376 s, 825686 us: Writer with ID:  139715215714048 starts writing.
163376 s, 825825 us: Writer with ID:  139715224106752 STARTS
163376 s, 825830 us: Writer with ID:  139715224106752 starts writing.
163376 s, 825905 us: Writer with ID:  139715266070272 starts writing.
163376 s, 825995 us: Writer with ID:  139715240892160 starts writing.
163376 s, 826148 us: Writer with ID:  139715358390016 starts writing.
163376 s, 826189 us: Writer with ID:  139715224106752 starts writing.
163376 s, 826233 us: Writer with ID:  139715266070272 starts writing.
163376 s, 826242 us: Writer with ID:  139715215714048 starts writing.
163376 s, 826266 us: Writer with ID:  139715240892160 starts writing.
163376 s, 826274 us: Writer with ID:  139715249284864 starts writing.
163376 s, 826278 us: Writer with ID:  139715291248384 starts writing.
163376 s, 826331 us: Writer with ID:  139715190535936 STARTS
163376 s, 826336 us: Writer with ID:  139715190535936 starts writing.
163376 s, 826388 us: Writer with ID:  139715249284864 starts writing.
163376 s, 826393 us: Writer with ID:  139715198928640 STARTS
163376 s, 826396 us: Writer with ID:  139715198928640 starts writing.
163376 s, 826416 us: Writer with ID:  139715207321344 STARTS
163376 s, 826419 us: Writer with ID:  139715207321344 starts writing.
163376 s, 826512 us: Writer with ID:  139715190535936 starts writing.
163376 s, 826550 us: Writer with ID:  139715291248384 starts writing.
163376 s, 826602 us: Writer with ID:  139715249284864 starts writing.
163376 s, 826677 us: Writer with ID:  139715198928640 starts writing.
163376 s, 826680 us: Writer with ID:  139715198928640 starts writing.
163376 s, 826774 us: Writer with ID:  139715291248384 starts writing.
163376 s, 826799 us: Writer with ID:  139715190535936 starts writing.
163376 s, 826806 us: Writer with ID:  139715182143232 STARTS
163376 s, 826841 us: Writer with ID:  139715173750528 STARTS
163376 s, 825465 us: Writer with ID:  139715257677568 STARTS
163376 s, 826903 us: Writer with ID:  139715165357824 STARTS
163376 s, 826912 us: Writer with ID:  139715140179712 STARTS
163376 s, 826918 us: Writer with ID:  139715123394304 STARTS
163376 s, 824351 us: Writer with ID:  139715341604608 STARTS
163376 s, 826966 us: Writer with ID:  139715341604608 starts writing.
163376 s, 826969 us: Writer with ID:  139715341604608 starts writing.
163376 s, 827040 us: Writer with ID:  139715232499456 starts writing.
163376 s, 827159 us: Writer with ID:  139715299641088 starts writing.
163376 s, 827181 us: Writer with ID:  139715131787008 STARTS
163376 s, 827186 us: Writer with ID:  139715131787008 starts writing.
163376 s, 827236 us: Writer with ID:  139715341604608 starts writing.
163376 s, 827303 us: Writer with ID:  139715131787008 starts writing.
163376 s, 827330 us: Writer with ID:  139715232499456 starts writing.
163376 s, 827353 us: Writer with ID:  139715299641088 starts writing.
163376 s, 827414 us: Writer with ID:  139715156965120 STARTS
163376 s, 827420 us: Writer with ID:  139715156965120 starts writing.
163376 s, 827502 us: Writer with ID:  139715131787008 starts writing.
163376 s, 827630 us: Writer with ID:  139715400353536 starts writing.
163376 s, 827754 us: Writer with ID:  139715299641088 starts writing.
163376 s, 827839 us: Writer with ID:  139715207321344 starts writing.
163376 s, 827905 us: Writer with ID:  139715106608896 STARTS
163376 s, 827912 us: Writer with ID:  139715106608896 starts writing.
163376 s, 828032 us: Writer with ID:  139715400353536 starts writing.
163376 s, 828178 us: Writer with ID:  139715148572416 STARTS
163376 s, 828184 us: Writer with ID:  139715148572416 starts writing.
163376 s, 828213 us: Writer with ID:  139715349997312 starts writing.
163376 s, 828236 us: Writer with ID:  139715207321344 starts writing.
163376 s, 828325 us: Writer with ID:  139715156965120 starts writing.
163376 s, 828332 us: Writer with ID:  139715156965120 starts writing.
163376 s, 828389 us: Writer with ID:  139715106608896 starts writing.
163376 s, 828394 us: Writer with ID:  139715106608896 starts writing.
163376 s, 828407 us: Writer with ID:  139715173750528 starts writing.
163376 s, 828413 us: Writer with ID:  139715257677568 starts writing.
163376 s, 828448 us: Writer with ID:  139715148572416 starts writing.
163376 s, 828458 us: Writer with ID:  139715089823488 STARTS
163376 s, 828463 us: Writer with ID:  139715089823488 starts writing.
163376 s, 829103 us: Writer with ID:  139715324819200 starts writing.
163376 s, 829116 us: Writer with ID:  139715115001600 STARTS
163376 s, 829121 us: Writer with ID:  139715115001600 starts writing.
163376 s, 829257 us: Writer with ID:  139715165357824 starts writing.
163376 s, 829290 us: Writer with ID:  139715089823488 starts writing.
163376 s, 828687 us: Writer with ID:  139715098216192 STARTS
163376 s, 829321 us: Writer with ID:  139715098216192 starts writing.
163376 s, 829335 us: Writer with ID:  139715324819200 starts writing.
163376 s, 829404 us: Writer with ID:  139715115001600 starts writing.
163376 s, 829478 us: Writer with ID:  139715173750528 starts writing.
163376 s, 829485 us: Writer with ID:  139715165357824 starts writing.
163376 s, 829609 us: Writer with ID:  139715098216192 starts writing.
163376 s, 829749 us: Writer with ID:  139715324819200 starts writing.
163376 s, 829818 us: Writer with ID:  139715115001600 starts writing.
163376 s, 829877 us: Writer with ID:  139715089823488 starts writing.
163376 s, 829906 us: Writer with ID:  139715173750528 starts writing.
163376 s, 829983 us: Writer with ID:  139715165357824 starts writing.
163376 s, 829996 us: Writer with ID:  139715215714048 starts writing.
163376 s, 830004 us: Writer with ID:  139715064645376 STARTS
163376 s, 830007 us: Writer with ID:  139715064645376 starts writing.
163376 s, 830009 us: Writer with ID:  139715064645376 starts writing.
163376 s, 830011 us: Writer with ID:  139715064645376 starts writing.
163376 s, 830018 us: Writer with ID:  139715098216192 starts writing.
163376 s, 830026 us: Writer with ID:  139715140179712 starts writing.
163376 s, 830181 us: Writer with ID:  139715308033792 starts writing.
163376 s, 830297 us: Writer with ID:  139715148572416 starts writing.
163376 s, 830354 us: Writer with ID:  139715282855680 starts writing.
163376 s, 830389 us: Writer with ID:  139715123394304 starts writing.
163376 s, 830445 us: Writer with ID:  139715140179712 starts writing.
163376 s, 828760 us: Writer with ID:  139715073038080 STARTS
163376 s, 830491 us: Writer with ID:  139715073038080 starts writing.
163376 s, 830533 us: Writer with ID:  139715282855680 starts writing.
163376 s, 830577 us: Writer with ID:  139715257677568 starts writing.
163376 s, 830610 us: Writer with ID:  139715308033792 starts writing.
163376 s, 830621 us: Writer with ID:  139715140179712 starts writing.
163376 s, 830670 us: Writer with ID:  139715073038080 starts writing.
163376 s, 830744 us: Writer with ID:  139715349997312 starts writing.
163376 s, 830760 us: Writer with ID:  139715123394304 starts writing.
163376 s, 830879 us: Writer with ID:  139715123394304 starts writing.
163376 s, 828766 us: Writer with ID:  139715047859968 STARTS
163376 s, 830912 us: Writer with ID:  139715047859968 starts writing.
163376 s, 830917 us: Writer with ID:  139715047859968 starts writing.
163376 s, 830920 us: Writer with ID:  139715047859968 starts writing.
163376 s, 828769 us: Writer with ID:  139715039467264 STARTS
163376 s, 830927 us: Writer with ID:  139715039467264 starts writing.
163376 s, 830929 us: Writer with ID:  139715039467264 starts writing.
163376 s, 830930 us: Writer with ID:  139715039467264 starts writing.
163376 s, 828756 us: Writer with ID:  139715081430784 STARTS
163376 s, 830956 us: Writer with ID:  139715073038080 starts writing.
163376 s, 828763 us: Writer with ID:  139715056252672 STARTS
163376 s, 831303 us: Writer with ID:  139715056252672 starts writing.
163376 s, 831359 us: Writer with ID:  139715081430784 starts writing.
163376 s, 831424 us: Writer with ID:  139715349997312 starts writing.
163376 s, 831435 us: Writer with ID:  139715056252672 starts writing.
163376 s, 831458 us: Writer with ID:  139715182143232 starts writing.
163376 s, 831518 us: Writer with ID:  139715081430784 starts writing.
163376 s, 831569 us: Writer with ID:  139715224106752 starts writing.
163376 s, 831591 us: Writer with ID:  139715257677568 starts writing.
163376 s, 831594 us: Writer with ID:  139715056252672 starts writing.
163376 s, 831772 us: Writer with ID:  139715182143232 starts writing.
163376 s, 831779 us: Writer with ID:  139715182143232 starts writing.
163376 s, 831791 us: Writer with ID:  139715081430784 starts writing.

