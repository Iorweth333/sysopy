Variant 2 favors Writers

examplary arguments: 8 1 3 1 10
./readers-writers2 8 1 3 1 10

8 readers reading once at the time
3 writers writing 1 at the time

examplary output:

Printing array: 
1 4 9 19 8 10 10 9 15 10 
Time format: seconds (s), microseconds (us)
157123 s, 225656 us: START
157123 s, 225745 us: reader with ID: 139878332110592 STARTS
157123 s, 225761 us: reader with ID: 139878332110592 reports: 0 number(s) divisible by 7
157123 s, 225870 us: Writer with ID:  139878416037632 STARTS
157123 s, 225879 us: Writer with ID:  139878416037632 starts writing.
157123 s, 225901 us: Writer with ID:  139878399252224 STARTS
157123 s, 225907 us: Writer with ID:  139878399252224 starts writing.
157123 s, 225984 us: reader with ID: 139878382466816 STARTS
157123 s, 225990 us: reader with ID: 139878382466816 reports: 1 number(s) divisible by 8
157123 s, 225756 us: reader with ID: 139878340503296 STARTS
157123 s, 226150 us: reader with ID: 139878340503296 reports: 10 number(s) divisible by 1
157123 s, 226137 us: Writer with ID:  139878407644928 STARTS
157123 s, 226253 us: Writer with ID:  139878407644928 starts writing.
157123 s, 226265 us: reader with ID: 139878374074112 STARTS
157123 s, 226271 us: reader with ID: 139878374074112 reports: 0 number(s) divisible by 11
157123 s, 226466 us: reader with ID: 139878390859520 STARTS
157123 s, 226474 us: reader with ID: 139878390859520 reports: 2 number(s) divisible by 3
157123 s, 226605 us: reader with ID: 139878365681408 STARTS
157123 s, 226613 us: reader with ID: 139878365681408 reports: 1 number(s) divisible by 20
157123 s, 226787 us: reader with ID: 139878357288704 STARTS
157123 s, 226796 us: reader with ID: 139878357288704 reports: 5 number(s) divisible by 4
157123 s, 226837 us: reader with ID: 139878348896000 STARTS
157123 s, 226842 us: reader with ID: 139878348896000 reports: 0 number(s) divisible by 7



CONCLUSION

Basically Writers win the race, as predicted. Those readers at the beginnig ar there probably because of the scheduler.
What is disappointing is that when writers or readers release the mutex, but want it back again, they get it creating a train of reading or writing operations (this can be seen in outputs below). I don't know why it happens


EDIT IMPORTANT
changing optimisation has a certain impact on this problem





[karolb@localhost variant2]$ ./readers-writers2 8 1 3 3 10
Printing array: 
1 4 9 19 8 10 10 9 15 10 
Time format: seconds (s), microseconds (us)
157334 s, 654221 us: START
157334 s, 654421 us: Writer with ID:  140561109550848 STARTS
157334 s, 654441 us: Writer with ID:  140561109550848 starts writing.
157334 s, 654443 us: Writer with ID:  140561109550848 starts writing.
157334 s, 654444 us: Writer with ID:  140561109550848 starts writing.
157334 s, 654581 us: Writer with ID:  140561101158144 STARTS
157334 s, 654591 us: Writer with ID:  140561101158144 starts writing.
157334 s, 654593 us: Writer with ID:  140561101158144 starts writing.
157334 s, 654594 us: Writer with ID:  140561101158144 starts writing.
157334 s, 654713 us: Writer with ID:  140561092765440 STARTS
157334 s, 654719 us: Writer with ID:  140561092765440 starts writing.
157334 s, 654721 us: Writer with ID:  140561092765440 starts writing.
157334 s, 654722 us: Writer with ID:  140561092765440 starts writing.
157334 s, 654826 us: reader with ID: 140561075980032 STARTS
157334 s, 654835 us: reader with ID: 140561075980032 reports: 1 number(s) divisible by 8
157334 s, 654962 us: reader with ID: 140561084372736 STARTS
157334 s, 654970 us: reader with ID: 140561084372736 reports: 2 number(s) divisible by 3
157334 s, 655219 us: reader with ID: 140561059194624 STARTS
157334 s, 655229 us: reader with ID: 140561059194624 reports: 0 number(s) divisible by 20
157334 s, 655343 us: reader with ID: 140561067587328 STARTS
157334 s, 655351 us: reader with ID: 140561067587328 reports: 0 number(s) divisible by 11
157334 s, 655399 us: reader with ID: 140561050801920 STARTS
157334 s, 655404 us: reader with ID: 140561050801920 reports: 3 number(s) divisible by 4
157334 s, 655633 us: reader with ID: 140561042409216 STARTS
157334 s, 655640 us: reader with ID: 140561042409216 reports: 4 number(s) divisible by 7
157334 s, 655742 us: reader with ID: 140561034016512 STARTS
157334 s, 655751 us: reader with ID: 140561034016512 reports: 10 number(s) divisible by 1
157334 s, 655782 us: reader with ID: 140561025623808 STARTS
157334 s, 655790 us: reader with ID: 140561025623808 reports: 4 number(s) divisible by 7






[karolb@localhost variant2]$ ./readers-writers2 20 3 20 3 10
Printing array: 
1 4 9 19 8 10 10 9 15 10 
Time format: seconds (s), microseconds (us)
157424 s, 833441 us: START
157424 s, 833469 us: Writer with ID:  139760843384576 STARTS
157424 s, 833482 us: Writer with ID:  139760843384576 starts writing.
157424 s, 833484 us: Writer with ID:  139760843384576 starts writing.
157424 s, 833485 us: Writer with ID:  139760843384576 starts writing.
157424 s, 833486 us: reader with ID: 139760524461824 STARTS
157424 s, 833491 us: reader with ID: 139760524461824 reports: 1 number(s) divisible by 8
157424 s, 833493 us: reader with ID: 139760524461824 reports: 1 number(s) divisible by 8
157424 s, 833495 us: reader with ID: 139760524461824 reports: 1 number(s) divisible by 8
157424 s, 833518 us: Writer with ID:  139760826599168 STARTS
157424 s, 833520 us: Writer with ID:  139760826599168 starts writing.
157424 s, 833522 us: Writer with ID:  139760826599168 starts writing.
157424 s, 833523 us: Writer with ID:  139760826599168 starts writing.
157424 s, 833597 us: reader with ID: 139760516069120 STARTS
157424 s, 833601 us: reader with ID: 139760516069120 reports: 0 number(s) divisible by 16
157424 s, 833603 us: reader with ID: 139760516069120 reports: 0 number(s) divisible by 16
157424 s, 833605 us: reader with ID: 139760516069120 reports: 0 number(s) divisible by 16
157424 s, 833649 us: Writer with ID:  139760818206464 STARTS
157424 s, 833715 us: Writer with ID:  139760818206464 starts writing.
157424 s, 833719 us: Writer with ID:  139760818206464 starts writing.
157424 s, 833720 us: Writer with ID:  139760818206464 starts writing.
157424 s, 833760 us: Writer with ID:  139760834991872 STARTS
157424 s, 833767 us: Writer with ID:  139760834991872 starts writing.
157424 s, 833769 us: Writer with ID:  139760834991872 starts writing.
157424 s, 833770 us: Writer with ID:  139760834991872 starts writing.
157424 s, 833883 us: Writer with ID:  139760809813760 STARTS
157424 s, 833888 us: Writer with ID:  139760809813760 starts writing.
157424 s, 833889 us: Writer with ID:  139760809813760 starts writing.
157424 s, 833891 us: Writer with ID:  139760809813760 starts writing.
157424 s, 833932 us: Writer with ID:  139760801421056 STARTS
157424 s, 833939 us: Writer with ID:  139760801421056 starts writing.
157424 s, 833941 us: Writer with ID:  139760801421056 starts writing.
157424 s, 833943 us: Writer with ID:  139760801421056 starts writing.
157424 s, 834008 us: Writer with ID:  139760784635648 STARTS
157424 s, 834012 us: Writer with ID:  139760784635648 starts writing.
157424 s, 834013 us: Writer with ID:  139760784635648 starts writing.
157424 s, 834015 us: Writer with ID:  139760784635648 starts writing.
157424 s, 834216 us: Writer with ID:  139760793028352 STARTS
157424 s, 834225 us: Writer with ID:  139760793028352 starts writing.
157424 s, 834227 us: Writer with ID:  139760793028352 starts writing.
157424 s, 834228 us: Writer with ID:  139760793028352 starts writing.
157424 s, 834396 us: Writer with ID:  139760776242944 STARTS
157424 s, 834403 us: Writer with ID:  139760776242944 starts writing.
157424 s, 834405 us: Writer with ID:  139760776242944 starts writing.
157424 s, 834406 us: Writer with ID:  139760776242944 starts writing.
157424 s, 834500 us: Writer with ID:  139760767850240 STARTS
157424 s, 834507 us: Writer with ID:  139760767850240 starts writing.
157424 s, 834509 us: Writer with ID:  139760767850240 starts writing.
157424 s, 834510 us: Writer with ID:  139760767850240 starts writing.
157424 s, 834609 us: Writer with ID:  139760759457536 STARTS
157424 s, 834615 us: Writer with ID:  139760759457536 starts writing.
157424 s, 834617 us: Writer with ID:  139760759457536 starts writing.
157424 s, 834619 us: Writer with ID:  139760759457536 starts writing.
157424 s, 834725 us: Writer with ID:  139760751064832 STARTS
157424 s, 834731 us: Writer with ID:  139760751064832 starts writing.
157424 s, 834733 us: Writer with ID:  139760751064832 starts writing.
157424 s, 834734 us: Writer with ID:  139760751064832 starts writing.
157424 s, 834748 us: Writer with ID:  139760742672128 STARTS
157424 s, 834750 us: Writer with ID:  139760742672128 starts writing.
157424 s, 834751 us: Writer with ID:  139760742672128 starts writing.
157424 s, 834753 us: Writer with ID:  139760742672128 starts writing.
157424 s, 834808 us: Writer with ID:  139760725886720 STARTS
157424 s, 834812 us: Writer with ID:  139760725886720 starts writing.
157424 s, 834813 us: Writer with ID:  139760725886720 starts writing.
157424 s, 834815 us: Writer with ID:  139760725886720 starts writing.
157424 s, 834911 us: Writer with ID:  139760734279424 STARTS
157424 s, 834918 us: Writer with ID:  139760734279424 starts writing.
157424 s, 834920 us: Writer with ID:  139760734279424 starts writing.
157424 s, 834921 us: Writer with ID:  139760734279424 starts writing.
157424 s, 835005 us: Writer with ID:  139760717494016 STARTS
157424 s, 835011 us: Writer with ID:  139760717494016 starts writing.
157424 s, 835013 us: Writer with ID:  139760717494016 starts writing.
157424 s, 835014 us: Writer with ID:  139760717494016 starts writing.
157424 s, 835231 us: Writer with ID:  139760709101312 STARTS
157424 s, 835238 us: Writer with ID:  139760709101312 starts writing.
157424 s, 835240 us: Writer with ID:  139760709101312 starts writing.
157424 s, 835242 us: Writer with ID:  139760709101312 starts writing.
157424 s, 835372 us: Writer with ID:  139760700708608 STARTS
157424 s, 835380 us: Writer with ID:  139760700708608 starts writing.
157424 s, 835382 us: Writer with ID:  139760700708608 starts writing.
157424 s, 835384 us: Writer with ID:  139760700708608 starts writing.
157424 s, 835425 us: Writer with ID:  139760692315904 STARTS
157424 s, 835432 us: Writer with ID:  139760692315904 starts writing.
157424 s, 835433 us: Writer with ID:  139760692315904 starts writing.
157424 s, 835435 us: Writer with ID:  139760692315904 starts writing.
157424 s, 835499 us: reader with ID: 139760675530496 STARTS
157424 s, 835503 us: reader with ID: 139760675530496 reports: 5 number(s) divisible by 3
157424 s, 835504 us: reader with ID: 139760675530496 reports: 5 number(s) divisible by 3
157424 s, 835506 us: reader with ID: 139760675530496 reports: 5 number(s) divisible by 3
157424 s, 835613 us: Writer with ID:  139760683923200 STARTS
157424 s, 835618 us: Writer with ID:  139760683923200 starts writing.
157424 s, 835620 us: Writer with ID:  139760683923200 starts writing.
157424 s, 835622 us: Writer with ID:  139760683923200 starts writing.
157424 s, 835680 us: reader with ID: 139760667137792 STARTS
157424 s, 835684 us: reader with ID: 139760667137792 reports: 0 number(s) divisible by 8
157424 s, 835686 us: reader with ID: 139760667137792 reports: 0 number(s) divisible by 8
157424 s, 835688 us: reader with ID: 139760667137792 reports: 0 number(s) divisible by 8
157424 s, 835735 us: reader with ID: 139760658745088 STARTS
157424 s, 835739 us: reader with ID: 139760658745088 reports: 1 number(s) divisible by 11
157424 s, 835741 us: reader with ID: 139760658745088 reports: 1 number(s) divisible by 11
157424 s, 835742 us: reader with ID: 139760658745088 reports: 1 number(s) divisible by 11
157424 s, 835868 us: reader with ID: 139760641959680 STARTS
157424 s, 835871 us: reader with ID: 139760641959680 reports: 0 number(s) divisible by 4
157424 s, 835873 us: reader with ID: 139760641959680 reports: 0 number(s) divisible by 4
157424 s, 835874 us: reader with ID: 139760641959680 reports: 0 number(s) divisible by 4
157424 s, 835976 us: reader with ID: 139760650352384 STARTS
157424 s, 835984 us: reader with ID: 139760650352384 reports: 0 number(s) divisible by 20
157424 s, 835985 us: reader with ID: 139760650352384 reports: 0 number(s) divisible by 20
157424 s, 835987 us: reader with ID: 139760650352384 reports: 0 number(s) divisible by 20
157424 s, 836229 us: reader with ID: 139760633566976 STARTS
157424 s, 836237 us: reader with ID: 139760633566976 reports: 1 number(s) divisible by 7
157424 s, 836238 us: reader with ID: 139760633566976 reports: 1 number(s) divisible by 7
157424 s, 836240 us: reader with ID: 139760633566976 reports: 1 number(s) divisible by 7
157424 s, 836436 us: reader with ID: 139760625174272 STARTS
157424 s, 836444 us: reader with ID: 139760625174272 reports: 10 number(s) divisible by 1
157424 s, 836446 us: reader with ID: 139760625174272 reports: 10 number(s) divisible by 1
157424 s, 836447 us: reader with ID: 139760625174272 reports: 10 number(s) divisible by 1
157424 s, 836562 us: reader with ID: 139760616781568 STARTS
157424 s, 836569 us: reader with ID: 139760616781568 reports: 1 number(s) divisible by 7
157424 s, 836571 us: reader with ID: 139760616781568 reports: 1 number(s) divisible by 7
157424 s, 836573 us: reader with ID: 139760616781568 reports: 1 number(s) divisible by 7
157424 s, 836612 us: reader with ID: 139760608388864 STARTS
157424 s, 836618 us: reader with ID: 139760608388864 reports: 0 number(s) divisible by 13
157424 s, 836620 us: reader with ID: 139760608388864 reports: 0 number(s) divisible by 13
157424 s, 836622 us: reader with ID: 139760608388864 reports: 0 number(s) divisible by 13
157424 s, 836710 us: reader with ID: 139760599996160 STARTS
157424 s, 836714 us: reader with ID: 139760599996160 reports: 0 number(s) divisible by 17
157424 s, 836716 us: reader with ID: 139760599996160 reports: 0 number(s) divisible by 17
157424 s, 836717 us: reader with ID: 139760599996160 reports: 0 number(s) divisible by 17
157424 s, 836797 us: reader with ID: 139760591603456 STARTS
157424 s, 836813 us: reader with ID: 139760591603456 reports: 0 number(s) divisible by 12
157424 s, 836815 us: reader with ID: 139760591603456 reports: 0 number(s) divisible by 12
157424 s, 836817 us: reader with ID: 139760591603456 reports: 0 number(s) divisible by 12
157424 s, 836969 us: reader with ID: 139760583210752 STARTS
157424 s, 836975 us: reader with ID: 139760583210752 reports: 3 number(s) divisible by 9
157424 s, 836977 us: reader with ID: 139760583210752 reports: 3 number(s) divisible by 9
157424 s, 836979 us: reader with ID: 139760583210752 reports: 3 number(s) divisible by 9
157424 s, 837013 us: reader with ID: 139760574818048 STARTS
157424 s, 837019 us: reader with ID: 139760574818048 reports: 0 number(s) divisible by 8
157424 s, 837021 us: reader with ID: 139760574818048 reports: 0 number(s) divisible by 8
157424 s, 837023 us: reader with ID: 139760574818048 reports: 0 number(s) divisible by 8
157424 s, 837038 us: reader with ID: 139760566425344 STARTS
157424 s, 837040 us: reader with ID: 139760566425344 reports: 0 number(s) divisible by 10
157424 s, 837041 us: reader with ID: 139760566425344 reports: 0 number(s) divisible by 10
157424 s, 837042 us: reader with ID: 139760566425344 reports: 0 number(s) divisible by 10
157424 s, 837049 us: reader with ID: 139760558032640 STARTS
157424 s, 837051 us: reader with ID: 139760558032640 reports: 6 number(s) divisible by 3
157424 s, 837052 us: reader with ID: 139760558032640 reports: 6 number(s) divisible by 3
157424 s, 837054 us: reader with ID: 139760558032640 reports: 6 number(s) divisible by 3
157424 s, 837060 us: reader with ID: 139760549639936 STARTS
157424 s, 837062 us: reader with ID: 139760549639936 reports: 1 number(s) divisible by 11
157424 s, 837063 us: reader with ID: 139760549639936 reports: 1 number(s) divisible by 11
157424 s, 837064 us: reader with ID: 139760549639936 reports: 1 number(s) divisible by 11
157424 s, 837300 us: reader with ID: 139760541247232 STARTS
157424 s, 837305 us: reader with ID: 139760541247232 reports: 6 number(s) divisible by 3
157424 s, 837306 us: reader with ID: 139760541247232 reports: 6 number(s) divisible by 3
157424 s, 837308 us: reader with ID: 139760541247232 reports: 6 number(s) divisible by 3
157424 s, 837456 us: reader with ID: 139760532854528 STARTS
157424 s, 837462 us: reader with ID: 139760532854528 reports: 0 number(s) divisible by 4
157424 s, 837463 us: reader with ID: 139760532854528 reports: 0 number(s) divisible by 4
157424 s, 837465 us: reader with ID: 139760532854528 reports: 0 number(s) divisible by 4



