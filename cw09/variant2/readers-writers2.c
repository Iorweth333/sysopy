#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <string.h>

#define NUMBERS_SCOPE 21    //maximum number will be 20

int *arr;
long arr_size;

pthread_t *readers_ids; //threads identifiers
pthread_t *writers_ids;

long readers_num;
long reading_quantity;      //quantity is not particularly preciseful name, but I couldn't come up with anythin else

long writers_num;
long writing_quantity;

int descriptive = 0;        //for -i flag

//variables for mutexes
int checker = 0;
int actual_writers = 0;     //of course max 1 at the time!
int actual_readers = 0;
int start = 0;

pthread_mutex_t ready_steady;                 //the point is that threads must work simultaneously
pthread_mutex_t mutex;                          //scheme taken from auxiliary materials written by one of tutors
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
pthread_cond_t start_cond = PTHREAD_COND_INITIALIZER;

int arguments_good(int argc, char **argv);

int is_number(char const *arg);

void *reader_thread(void *arg);

void *writer_thread(void *arg);

void arr_init(long size);

void cleaner();

int main(int argc, char **argv)
{

    if (!arguments_good(argc, argv))
    {
        printf("Inappropriate arguments. Please provide: \n"
                       "how many readers \n"
                       "how much do they read"
                       "how many writers \n"
                       "how much do they write"
                       "size of array \n"
                       "(optionally) for extended communitacion flag -i  \n");
        exit(1);
    }

    readers_num = strtol(argv[1], NULL, 10);
    reading_quantity = strtol(argv[2], NULL, 10);
    writers_num = strtol(argv[3], NULL, 10);
    writing_quantity = strtol(argv[4], NULL, 10);
    arr_size = strtol(argv[5], NULL, 10);
    arr_init(arr_size);
    printf ("Time format: seconds (s), microseconds (us)\n");

    if (argv[6] != NULL) descriptive = 1;   //


    //RUNNING THREADS
    // FAVOR WRITERS

    //allocating arrays for thread identifiers
    readers_ids = malloc(sizeof(pthread_t) * readers_num);
    writers_ids = malloc(sizeof(pthread_t) * writers_num);


    int *dividers = malloc(sizeof(int) * readers_num);

    for (int i = 0; i < readers_num; i++)
        dividers[i] = rand() % (NUMBERS_SCOPE - 1) + 1;

    pthread_mutexattr_t attr;
    pthread_mutexattr_init(&attr);      //Initialize mutex attribute object ATTR with default attributes
    pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_ERRORCHECK);
    //error checking mutex is in my opinion the best option,
    //for instance, when unlocking already unlocked, it returns EDEADLK

    pthread_mutex_init(&ready_steady, &attr);
    pthread_mutex_init(&mutex, &attr);

    pthread_mutex_lock(&ready_steady);        //ready.... steady....


    for (int i = 0; i < writers_num; i++)
    {
        if (pthread_create(&writers_ids[i], NULL, writer_thread, NULL) != 0)
        {
            printf("Error while creating writer thread.\n");
            perror(NULL);
            exit(1);
        }
    }


    for (int i = 0; i < readers_num; i++)
    {
        if (pthread_create(&readers_ids[i], NULL, reader_thread, &dividers[i]) != 0) // reader takes divider
        {
            printf("Error while creating reader thread.\n");
            perror(NULL);
            exit(1);
        }
    }



    struct timespec tm_spec;
    clock_gettime(CLOCK_MONOTONIC, &tm_spec);
    printf("%d s, %d us: START\n",
           (int) tm_spec.tv_sec, (int) tm_spec.tv_nsec / 1000);

    start = 1;
    pthread_cond_broadcast(&start_cond);      //Wake up all threads waiting for condition variables COND.
    pthread_mutex_unlock(&ready_steady);      //GO!

    for (int i = 0; i < readers_num; i++)
    {
        if (pthread_join(readers_ids[i], NULL) != 0)
        {
            printf("Error while joining readers' threads.\n");
            perror(NULL);
            exit(1);
        }
    }

    for (int i = 0; i < writers_num; i++)
    {
        if (pthread_join(writers_ids[i], NULL) != 0)
        {
            printf("Error while joining writers' threads.\n");
            perror(NULL);
            exit(1);
        }
    }


    cleaner();
    return 0;
}

void cleaner()
{
    free(arr);
    free(readers_ids);
    free(writers_ids);

    pthread_cond_destroy(&cond);
}


void *reader_thread(void *arg)
{
    pthread_mutex_lock(&ready_steady);

    while (!start) pthread_cond_wait(&start_cond, &ready_steady);
    //Wait for condition variable COND to be signaled or broadcast.
    //MUTEX is assumed to be locked before.

    pthread_mutex_unlock(&ready_steady);

    int divider = *(int *) arg;

    struct timespec tm_spec;
    clock_gettime(CLOCK_MONOTONIC, &tm_spec);
    printf("%d s, %d us: reader with ID: %lu STARTS\n",
           (int) tm_spec.tv_sec, (int) tm_spec.tv_nsec / 1000, pthread_self());


    for (int i = 0; i < reading_quantity; i++)
    {
        pthread_mutex_lock(&mutex);

        while (checker) pthread_cond_wait(&cond, &mutex);   // waiting until there is no writers writing
        //                                                     writers increment variable checker,


        actual_readers++;

        pthread_mutex_unlock(&mutex);


        int counter = 0;

        for (int j = 0; j < arr_size; j++)
        {
            if (arr[j] % divider == 0)
            {
                counter++;
                if (descriptive)
                {
                    clock_gettime(CLOCK_MONOTONIC, &tm_spec);

                    printf("%d s, %d us: reader with ID: %lu reports: index: %d, value: %d, divider: %d\n",
                           (int) tm_spec.tv_sec, (int) tm_spec.tv_nsec / 1000, pthread_self(), j, arr[j], divider);
                }
            }
        }
        clock_gettime(CLOCK_MONOTONIC, &tm_spec);
        printf("%d s, %d us: reader with ID: %lu reports: %d number(s) divisible by %d\n",
               (int) tm_spec.tv_sec, (int) tm_spec.tv_nsec / 1000, pthread_self(), counter, divider);

        pthread_mutex_lock(&mutex);
        actual_readers--;
        pthread_cond_broadcast(&cond);      //Wake up all threads waiting for condition variables COND.
        pthread_mutex_unlock(&mutex);
    }

    return NULL;
}

// FAVOR WRITERS
void *writer_thread(void *arg)
{
    pthread_mutex_lock(&ready_steady);

    while (!start) pthread_cond_wait(&start_cond, &ready_steady);
    //Wait for condition variable COND to be signaled or broadcast.
    //MUTEX is assumed to be locked before.

    pthread_mutex_unlock(&ready_steady);

    struct timespec tm_spec;
    clock_gettime(CLOCK_MONOTONIC, &tm_spec);
    printf("%d s, %d us: Writer with ID:  %lu STARTS\n",
           (int) tm_spec.tv_sec, (int) tm_spec.tv_nsec / 1000, pthread_self());


    for (int i = 0; i < writing_quantity; i++)
    {
        pthread_mutex_lock(&mutex);
        checker++;                     //this incremetation gives writers the advantage over readers

        //there can be only one writer writing at the time
        while (actual_readers || actual_writers) pthread_cond_wait(&cond, &mutex);

        actual_writers++;
        pthread_mutex_unlock(&mutex);

        long how_many, index;
        int value;
        how_many = (rand() % arr_size) + 1;


        clock_gettime(CLOCK_MONOTONIC, &tm_spec);
        printf("%d s, %d us: Writer with ID:  %lu starts writing.\n",
               (int) tm_spec.tv_sec, (int) tm_spec.tv_nsec / 1000, pthread_self());

        for (int j = 0; j < how_many; j++)
        {
            index = rand() % arr_size;
            value = rand() % NUMBERS_SCOPE;

            arr[index] = value;
            if (descriptive)
            {
                clock_gettime(CLOCK_MONOTONIC, &tm_spec);
                printf("%d s, %d us: Writer with ID: %lu changed number under index %ld "
                               "to %d\n", (int) tm_spec.tv_sec, (int) tm_spec.tv_nsec / 1000, pthread_self(), index, value);
            }
        }

        pthread_mutex_lock(&mutex);
        actual_writers--;
        checker--;
        pthread_cond_broadcast(&cond);
        pthread_mutex_unlock(&mutex);

    }

    return NULL;
}

void arr_init(long size)
{
    arr_size = size;
    arr = malloc(arr_size * sizeof(int));

    printf ("Printing array: \n");
    for (int i = 0; i < arr_size; i++)
    {
        arr[i] = rand() % NUMBERS_SCOPE;
        printf("%d ", arr[i]);
    }
    printf("\n");
}

int arguments_good(int argc, char **argv)
{
    if ((argc < 6) || (argc > 7)) return 0;

    for (int i = 1; i < 6; i++) if (!is_number(argv[i])) return 0;

    if (argc == 7 && (strcmp(argv[6], "-i") != 0)) return 0;

    return 1;

}

int is_number(char const *arg)
{
    int i = 0;
    while (arg[i] != '\0')
        if (arg[i] >= '0' && arg[i] <= '9') i++;
        else return 0;
    return 1;
}