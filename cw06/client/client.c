#include <stdio.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "client-server_commons.h"

int client_q_id;    //must be global because q must be closed at exit and there are no arguments passed to handler registered by atexit(3)

void send_msg(int q_id, struct msgbuf *msg);

void receive_msg(int q_id, struct msgbuf *msg, long type, int msgflg);

enum msg_type interpret_input(const char *const input, char **argument);

void arr_dyn_to_static(const char *dyn, char *stat, long len);            //probably irrelevant

void cleaner()
{ msgctl(client_q_id, IPC_RMID, NULL); }

int main()
{
    //get access to server's queue
    int key = ftok(FTOK1, FTOK2);                    
    int server_q_id = msgget(key, 0);               //TODO: check if flag is ok due to permissions
    if (server_q_id < 0) //create if doesn't exist and return error if does
    {
        printf("Error while opening server's queue\n");
        perror(NULL);
        exit(1);
    }

    //get private queue
    client_q_id = msgget(IPC_PRIVATE, 0600);
    atexit(cleaner);
    //IPC_PRIVATE isn't a flag field but a key_t type.  If this special
    //value is used for key, the system call ignores everything but the
    //least significant 9 bits of msgflg and creates a new message queue
    //what flag should I use for private queue?
    //asnwer: http://www.cs.kent.edu/~ruttan/sysprog/lectures/shmem/msqueues.html
    //"The queue created belongs to the user whose process created the queue. Thus, since the permission bits are '0600', only processes run on behalf of this user will have access to the queue. "

    //send private queue's id to server
    struct msgbuf msg;
    msg.mtype = ENROLLMENT_REQUEST;
    msg.client_q_id = client_q_id;
    msg.client_pid = getpid();              //TODO: check if pid doesn't need refreshing


    send_msg(server_q_id, &msg);

    receive_msg(client_q_id, &msg, 0, 0);
    //TODO: check what to do with information got here (it's identification of a client)
    if (msg.mtype == DENIED)
    {
        printf ("Server denied client. Terminating client\n");
        exit(1);
    }

    printf("Connection established. Provide commands (one and only one command per line)\n");
    printf("Possible commands (X stands for an argument): \n"
                   "\t echo X \n"
                   "\t ucase X \n"
                   "\t time \n"
                   "\t shut_down_server\n"
                   "\t exit \n");
    char *input = malloc(MAX_INPUT_SIZE * sizeof(char));
    while (1)
    {

        fgets(input, MAX_INPUT_SIZE, stdin);
        char *argument;
        msg.mtype = interpret_input(input, &argument);

        if (msg.mtype == EXIT) break;
        if (msg.mtype == NONE) continue;
        if (msg.mtype == UNKNOWN)
        {
            printf("Unknown command\n");
            continue;
        }


        if (msg.mtype == ECHO)
        {
            strcpy(msg.mtext, argument);
            free(argument);
            argument = NULL;
            send_msg(server_q_id, &msg);
            receive_msg(client_q_id, &msg, 0, 0);
            printf("Received echo: %s\n", msg.mtext);
        }
        if (msg.mtype == UCASE)
        {
            strcpy(msg.mtext, argument);
            free(argument);
            argument = NULL;
            send_msg(server_q_id, &msg);
            receive_msg(client_q_id, &msg, 0, 0);
            printf("Received ucase: %s", msg.mtext);
        }
        if (msg.mtype == TIME)
        {
            send_msg(server_q_id, &msg);
            receive_msg(client_q_id, &msg, 0, 0);
            printf("Received time: %s", msg.mtext);
        }
        if (msg.mtype == SHUT_DOWN_SERVER)
        {
            send_msg(server_q_id, &msg);
            break;
        }
    }
    return 0;
}

void send_msg(int q_id, struct msgbuf *msg)
{
    if (msgsnd(q_id, msg, MSG_SIZE, 0) < 0)
    {
        printf("Error while sending message to the server. Terminating\n");
        perror(NULL);
        exit(1);
    }
}

void receive_msg(int q_id, struct msgbuf *msg, long type, int msgflg)
{
    if (msgrcv(q_id, msg, MSG_SIZE, type, msgflg) < 0)
    {
        printf("Error while receiving message from the server\n");
        perror(NULL);
        exit(1);
    }
}

enum msg_type interpret_input(const char *const input,
                              char **argument)        //return type od comand and argument of comand if necessary
{
    if (input == NULL || input[0] == '\n') return NONE;

    enum msg_type res = UNKNOWN;

    char *tmp = calloc(MAX_CMD_TYPE_SIZE, sizeof(char));
    int i = 0;
    while (i < MAX_INPUT_SIZE)
    {
        if (input[i] != '\n' && input[i] != '\0' && input[i] != ' ') tmp[i] = input[i];
        else break;
        i++;
    }

    if (strcmp(tmp, "echo") == 0)
    {
        res = ECHO;
        *argument = strdup(&input[i+1]);    //i+1 to omit space after "echo"
    }
    if (strcmp(tmp, "ucase") == 0)
    {
        res = UCASE;
        *argument = strdup(&input[i]);
    }
    if (strcmp(tmp, "time") == 0)
    {
        res = TIME;
    }
    if (strcmp(tmp, "shut_down_server") == 0)
    {
        res = SHUT_DOWN_SERVER;
    }
    if (strcmp(tmp, "exit") == 0)
    {
        res = EXIT;
    }
    free(tmp);
    return res;
}

void arr_dyn_to_static(const char *dyn, char *stat, long len)
{
    for (long i = 0; i < len; i++) stat[i] = dyn[i];

}
