#include <stdio.h>
#include <sys/types.h>
#include <fcntl.h>           /* For O_* constants */
#include <time.h>
#include <stdlib.h>
#include <errno.h>
#include <ctype.h>
#include <signal.h>
#include <mqueue.h>
#include "client-server_commons2.h"

mqd_t server_q_des;     //must be global to change parameters in handler
int clients_num;        //must be global so that reaching it in cleaner was possible
mqd_t clients_qs[MAX_CLIENTS];  //same as above

void SIGINThandler(int signum);

void cleaner(); //deleting queues

int receive_msg(mqd_t mqdes, char *ptr);

void send_msg(mqd_t mqdes, const struct msgbuf const *msg);

void enroll_client(struct msgbuf msg, int *clients_num, mqd_t *client_qs);

/*
 * getting time as string
 time_t current_time;
struct tm * time_info;
char timeString[9];  // space for "HH:MM:SS\0"

time(&current_time);
time_info = localtime(&current_time);

strftime(timeString, sizeof(timeString), "%H:%M:%S", time_info);
puts(timeString);
 */

int main()
{

    struct mq_attr server_q_attr;
    server_q_attr.mq_maxmsg = MAXMSG;
    server_q_attr.mq_msgsize = MSG_SIZE;
    server_q_attr.mq_flags = 0;

    server_q_des = mq_open(server_q_name, O_CREAT | O_RDONLY, 0600, &server_q_attr);

    if (server_q_des < 0)
    {
        printf("Error while creating server's queue\n");
        perror(NULL);
        exit(1);
    }
    atexit(cleaner);
    signal(SIGINT, SIGINThandler);
    //handling added in case that no client is still running and limit of clients has been reached but no one sent SHUT_DOWN_SERVER
    //in that case there is no mild way to stop server, but when you kill it brutally it doesn't remove message queue

    printf("Server starts.\n");

    struct msgbuf msg;


    clients_num = 0;
    struct tm *time_info;
    time_t current_time;


    while (1)
    {
        int err = receive_msg(server_q_des, (char *) &msg);

        if (err < 0 && (errno == EAGAIN || errno == EINTR)) break;

        switch (msg.mtype)
        {
            case ENROLLMENT_REQUEST:
                if (clients_num < MAX_CLIENTS)
                {
                    enroll_client(msg, &clients_num, clients_qs);
                    //includes sending message about acceptation and incrementing clients_num
                }
                else
                {
                    msg.mtype = DENIED;
                    mqd_t q_of_denied = mq_open(msg.mtext, O_WRONLY);
                    send_msg(q_of_denied, &msg);
                    mq_close(q_of_denied);
                }
                break;

            case ECHO:
                send_msg(clients_qs[msg.client_id], &msg);
                break;

            case UCASE:
                for (int i = 0; i < MAX_INPUT_SIZE; i++) msg.mtext[i] = (char) toupper(msg.mtext[i]);
                send_msg(clients_qs[msg.client_id], &msg);
                break;

            case TIME:
                time(&current_time);
                time_info = localtime(&current_time);
                strftime(msg.mtext, 10 * sizeof(char), "%H:%M:%S\n",
                         time_info);    //10 chars because of null terminating char
                send_msg(clients_qs[msg.client_id], &msg);
                break;

            case SHUT_DOWN_SERVER:
                SIGINThandler(0);   //changes parameters of the q for NONBLOCK
                break;
            case CLIENT_DISMISS:
                err = mq_close(clients_qs[msg.client_id]);
                if (err < 0)
                {
                    printf("Error while closing client's queue\n");
                    perror(NULL);
                }
                break;

            default:
                printf("Received message of unknown type. NOT terminating\n");
        }
    }
    printf("Server got signal to stop or client ordered to shut down. Stopping\n");
    return 0;
}


int receive_msg(mqd_t mqdes, char *ptr)
{
    int res = (int) mq_receive(mqdes, ptr, MSG_SIZE, NULL);
    if (res < 0)
    {
        printf("Error while receiving message from a client. NOT exiting (if not supposed to)\n");
        perror(NULL);
    }
    return res;
}

void send_msg(mqd_t mqdes, const struct msgbuf const *msg)
{
    if (mq_send(mqdes, (char *) msg, MSG_SIZE, 0) < 0)
    {
        printf("Error while sending message to client\n");
        perror(NULL);
        exit(1);
    }
}

void enroll_client(struct msgbuf msg, int *clients_num, int *client_qs)
{
    msg.mtype = ACCEPTED;
    msg.client_id = *clients_num;
    client_qs[*clients_num] = mq_open(msg.mtext, O_WRONLY);
    send_msg(client_qs[*clients_num], &msg);
    (*clients_num)++;
}

void SIGINThandler(int signum)
{
    struct mq_attr new_q_attr;
    new_q_attr.mq_flags = O_NONBLOCK;
    int err = mq_setattr(server_q_des, &new_q_attr, NULL);
    //mq_setattr() sets message queue attributes using information supplied in the mq_attr structure pointed to by newattr.
    // The only attribute that can be modified is the setting of the O_NONBLOCK flag in mq_flags.

    if (err < 0)
    {
        printf("Error while changing attributes of server's queue. NOT terminating\n");
        perror(NULL);
    }
}

void cleaner()
{
    mq_unlink(server_q_name);       //TODO: do I have to close it before?

    for (int i = 0; i < clients_num; i++) mq_close(clients_qs[i]);
}

