#include <stdio.h>
#include <sys/types.h>
#include <mqueue.h>
#include <fcntl.h>           /* For O_* constants */
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "client-server_commons2.h"

mqd_t client_q_des;    //must be global because q must be closed at exit and there are no arguments passed to handler registered by atexit(3)
mqd_t server_q_des;
char *client_q_name;
int accepted;           //useful to check if there is a need to ask server to close client's queue

void send_msg(mqd_t mqdes, struct msgbuf *msg);

void receive_msg(mqd_t mqdes, char *ptr);

enum msg_type interpret_input(const char *const input, char **argument);

void cleaner()
{
    struct msgbuf msg;
    msg.mtype = CLIENT_DISMISS;
    strcpy(msg.mtext, client_q_name);
    if (accepted > 0) send_msg(server_q_des, &msg);        //asking server to close client's queue

    mq_close(server_q_des);
    mq_unlink(client_q_name);     //TODO: DO I need to close first?
}

int main()
{
    //get access to client's queue
    server_q_des = mq_open(server_q_name, O_WRONLY);               //TODO: check if flag is ok due to permissions
    if (server_q_des < 0) //create if doesn't exist and return error if does
    {
        printf("Error while opening server's queue. Terminating\n");
        perror(NULL);
        exit(1);
    }

    //get private queue
    struct mq_attr q_attr;
    q_attr.mq_maxmsg = MAXMSG;
    q_attr.mq_msgsize = MSG_SIZE;
    q_attr.mq_flags = 0;

    client_q_name = malloc(25 * sizeof(char));   //25 chars should be enough
    sprintf(client_q_name, "/client_q_%i", getpid());
    client_q_des = mq_open(client_q_name, O_CREAT | O_RDONLY, 0600, &q_attr);
    //permission bits are '0600', only processes run on behalf of this user will have access to the queue. "
    if (client_q_des < 0)
    {
        printf("Error while creating client's queue. Terminating\n");
        perror(NULL);
        exit(1);
    }

    accepted = 0;   //assuming not
    atexit(cleaner);

    //send private queue's name to server
    struct msgbuf msg;
    msg.mtype = ENROLLMENT_REQUEST;
    msg.client_q_des = client_q_des;
    msg.client_pid = getpid();
    strcpy(msg.mtext, client_q_name);


    send_msg(server_q_des, &msg);

    receive_msg(client_q_des, (char *) &msg);

    if (msg.mtype == DENIED)
    {
        printf("Server denied client. Terminating client\n");
        exit(1);    //variable "accepted" is already 0
    }
    accepted = 1;

    printf("Connection established. Provide commands (one and only one command per line)\n");
    printf("Possible commands (X stands for an argument): \n"
                   "\t echo X \n"
                   "\t ucase X \n"
                   "\t time \n"
                   "\t shut_down_server\n"
                   "\t exit \n");
    char *input = malloc(MAX_INPUT_SIZE * sizeof(char));
    while (1)
    {

        fgets(input, MAX_INPUT_SIZE, stdin);
        char *argument;
        msg.mtype = interpret_input(input, &argument);

        if (msg.mtype == EXIT) break;
        if (msg.mtype == NONE) continue;
        if (msg.mtype == UNKNOWN)
        {
            printf("Unknown command\n");
            continue;
        }


        if (msg.mtype == ECHO)
        {
            strcpy(msg.mtext, argument);
            free(argument);
            argument = NULL;
            send_msg(server_q_des, &msg);
            receive_msg(client_q_des, (char *) &msg);
            printf("Received echo: %s\n", msg.mtext);
        }
        if (msg.mtype == UCASE)
        {
            strcpy(msg.mtext, argument);
            free(argument);
            argument = NULL;
            send_msg(server_q_des, &msg);
            receive_msg(client_q_des, (char *) &msg);
            printf("Received ucase: %s", msg.mtext);
        }
        if (msg.mtype == TIME)
        {
            send_msg(server_q_des, &msg);
            receive_msg(client_q_des, (char *) &msg);
            printf("Received time: %s", msg.mtext);
        }
        if (msg.mtype == SHUT_DOWN_SERVER)
        {
            send_msg(server_q_des, &msg);
            break;
        }
    }
    return 0;
}

void send_msg(mqd_t mqdes, struct msgbuf *msg)
{
    if (mq_send(mqdes, (char *) msg, MSG_SIZE, 0) < 0)
    {
        printf("Error while sending message to server\n");
        perror(NULL);
        exit(1);
    }
}

void receive_msg(mqd_t mqdes, char *ptr)
{
    if (mq_receive(mqdes, ptr, MSG_SIZE, NULL) < 0)
    {
        printf("Error while receiving message from the server. NOT exiting.\n");
        perror(NULL);
    }

}

enum msg_type interpret_input(const char *const input,
                              char **argument)        //return type od comand and argument of comand if necessary
{
    if (input == NULL || input[0] == '\n') return NONE;

    enum msg_type res = UNKNOWN;

    char *tmp = calloc(MAX_CMD_TYPE_SIZE, sizeof(char));
    int i = 0;
    while (i < MAX_INPUT_SIZE)
    {
        if (input[i] != '\n' && input[i] != '\0' && input[i] != ' ') tmp[i] = input[i];
        else break;
        i++;
    }

    if (strcmp(tmp, "echo") == 0)
    {
        res = ECHO;
        *argument = strdup(&input[i + 1]);    //i+1 to omit space after "echo"
    }
    if (strcmp(tmp, "ucase") == 0)
    {
        res = UCASE;
        *argument = strdup(&input[i]);
    }
    if (strcmp(tmp, "time") == 0)
    {
        res = TIME;
    }
    if (strcmp(tmp, "shut_down_server") == 0)
    {
        res = SHUT_DOWN_SERVER;
    }
    if (strcmp(tmp, "exit") == 0)
    {
        res = EXIT;
    }
    free(tmp);
    return res;
}
