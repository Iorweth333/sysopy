#include <stdio.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <stdlib.h>
#include <errno.h>
#include <ctype.h>
#include <time.h>
#include <signal.h>
#include "client-server_commons.h"

int flag;   //global cause changed in handler

void SIGINThandler(int signum);

int server_q_id; //must be global because q must be closed at exit and there are no arguments passed to handler registered by atexit(3)
void cleaner()
{ msgctl(server_q_id, IPC_RMID, NULL); }    //deleting q

int receive_msg(int q_id, struct msgbuf *msg, long type, int msgflg);
void send_msg(int q_id, const struct msgbuf const *msg);

void enroll_client (struct msgbuf msg, int *clients_num, int *client_qs);

/*
 * getting time as string
 time_t current_time;
struct tm * time_info;
char timeString[9];  // space for "HH:MM:SS\0"

time(&current_time);
time_info = localtime(&current_time);

strftime(timeString, sizeof(timeString), "%H:%M:%S", time_info);
puts(timeString);
 */

int main()
{

    int key = ftok(FTOK1, FTOK2);                           //probably must change parameters
    server_q_id = msgget(key, IPC_CREAT | IPC_EXCL | 0666);    //create if doesn't exist and return error if does. 0666 - read and write for all
    if (server_q_id < 0)
    {
        printf("Error while creating server's queue\n");
        perror(NULL);
        exit(1);
    }
    atexit(cleaner);
    signal (SIGINT, SIGINThandler);
    //handling added in case that no client is still running and limit of clients has been reached but no one sent SHUT_DOWN_SERVER
    //in that case there is no mild way to stop server, but when you kill it brutally it doesn't remove message queue

    printf("Server starts. Server q id: %i\n", server_q_id);

    struct msgbuf msg;

    flag = 0;

    int clients_num = 0;
    int clients_qs [MAX_CLIENTS];
    struct tm * time_info;
    time_t current_time;


    while (1)
    {
        int err = receive_msg(server_q_id, &msg, 0, flag);

        if (err < 0 && (errno == ENOMSG || errno == EINTR)) break;

        switch (msg.mtype)
        {
            case ENROLLMENT_REQUEST:
                if (clients_num < MAX_CLIENTS)
                {
                    enroll_client(msg, &clients_num, clients_qs);         //includes sending message about acceptation and incrementing clients_num

                }
                else
                {
                    msg.mtype = DENIED;
                    send_msg (msg.client_q_id, &msg);
                }
                break;

            case ECHO:
                send_msg(msg.client_q_id, &msg);
                break;

            case UCASE:
                for (int i=0; i<MAX_INPUT_SIZE; i++) msg.mtext[i] = (char) toupper(msg.mtext[i]);
                send_msg(msg.client_q_id, &msg);
                break;

            case TIME:
                time(&current_time);
                time_info = localtime(&current_time);
                strftime(msg.mtext, 10 * sizeof(char), "%H:%M:%S\n", time_info);    //10 chars because of null terminating char
                send_msg(msg.client_q_id, &msg);
                break;

            case SHUT_DOWN_SERVER:
                flag = IPC_NOWAIT;
                break;

            default:
                printf ("Received message of unknown type. NOT terminating\n");
        }
    }
    printf ("Server stopping\n");
    return 0;
}


int receive_msg(int q_id, struct msgbuf *msg, long type, int msgflg)
{
    int res = (int) msgrcv(q_id, msg, MSG_SIZE, type, msgflg);
    if (res < 0)
    {
        printf("Error while receiving message from a client. NOT exiting (if not supposed to)\n");
        perror(NULL);
    }
    return res;
}

void send_msg(int q_id, const struct msgbuf const *msg)
{
    if (msgsnd(q_id, msg, MSG_SIZE, 0) < 0)
    {
        printf("Error while sending message to client\n");
        perror(NULL);
        exit(1);
    }
}

void enroll_client (struct msgbuf msg, int *clients_num, int *client_qs)
{
    msg.mtype = ACCEPTED;
    msg.client_id = *clients_num;
    client_qs[*clients_num] = msg.client_q_id;		//what is interesting, there is no necessity to open client's queue
    (*clients_num)++;
    send_msg(msg.client_q_id, &msg);
}

void SIGINThandler (int signum)
{
    flag = IPC_NOWAIT;
}
