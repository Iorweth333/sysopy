//
// Constants common for server and it's clients
// Created by karolb on 07.09.17.
//

#ifndef CLIENT_CLIENT_SERVER_COMMONS_H
#define CLIENT_CLIENT_SERVER_COMMONS_H

#include <stdlib.h>
#include <sys/msg.h>
#define MAX_CLIENTS 4
#define MSG_SIZE (sizeof(struct msgbuf)-sizeof(long))


#define FTOK1 getenv("HOME")    //get environmental variable $HOME
#define FTOK2 'p'               //it actually can be whatever until it's char

#define MAX_INPUT_SIZE 101    //maximum size of user-input commands which is 100 + null
#define MAX_CMD_TYPE_SIZE 16    //length of longest possible command in client not including argument, which is "SHUT_DOWN_SERVER"


enum msg_type {
    ENROLLMENT_REQUEST = 1,     //1 because 0 means getting first msg in the queue
    ACCEPTED,
    DENIED,
    ECHO,
    UCASE,
    TIME,
    SHUT_DOWN_SERVER,
    EXIT,
    NONE,
    UNKNOWN
    //ATTENTION after a modification here check if MAX_CMD_TYPE_SIZE is correct
};

struct msgbuf {
    long mtype;
    pid_t client_pid;
    int client_id;
    int client_q_id;
    char mtext[MAX_INPUT_SIZE];
};

#endif //CLIENT_CLIENT_SERVER_COMMONS_H
