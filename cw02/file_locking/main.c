#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>


void interpret_input(char **input, char **cmd, long int *byteid, int *noblock)
{
    *cmd = strtok(*input, " \n");
    char *byteid_s = strtok(NULL, " ");
    if (byteid_s != NULL) *byteid = strtol(byteid_s, NULL, 10);
    *noblock = 0;
    char *noblock_s = strtok(NULL, " ");
    if (noblock_s != NULL && (strcmp(noblock_s, "noblock") == 0 || strcmp(noblock_s, "noblock\n") == 0))
        *noblock = 1;
}


struct flock check_lock (int file, long index)
{
    struct flock tmp;
    tmp.l_start = index;
    tmp.l_len = 1;
    tmp.l_whence = SEEK_SET;
    tmp.l_type = F_WRLCK;       //exclusive lock, so that any other locks were incompatible
    fcntl(file, F_GETLK, &tmp); //On input to this call, lock describes a lock we would like to
    //place on the file.  If the lock could be placed, fcntl() does
    //not actually place it, but returns F_UNLCK in the l_type field
    //of lock and leaves the other fields of the structure unchanged.
    return tmp;
}


void print_locks(int file)
{
    struct flock tmp;
    __off_t file_end = lseek(file, 0, SEEK_END);
    printf("offset || type || PID\n");
    for (long i = 0; i <= file_end; i++)
    {

        tmp = check_lock(file, i);
        if (tmp.l_type != F_UNLCK)
        {
            //If one or more incompatible locks would prevent this lock
            //being placed, then fcntl() returns details about one of those
            //locks in the l_type, l_whence, l_start, and l_len fields of
            //lock.  If the conflicting lock is a traditional (process-
            //associated) record lock, then the l_pid field is set to the
            //PID of the process holding that lock.
            printf("%li ", tmp.l_start);
            (tmp.l_type == F_RDLCK) ? printf("F_RDLCK ") : printf("F_WRLCK ");
            printf("%i \n", tmp.l_pid);
        }
    }
}



int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        printf("Niedostateczna ilosc argumentow. \n"
                       "Podaj sciezke do pliku \n");
        return 1;
    }
    printf("\nCo chcesz zrobic? Podaj komende i numer bajtu docelowgo (opcjonalnie dopisz noblock)\n");
    printf(
            "    ustawienie rygla do odczytu na wybrany znak pliku: lockr\n"
                    "    ustawienie rygla do zapisu na wybrany znak pliku: lockw\n"
                    "    wyświetlenie listy zaryglowanych znaków pliku: listl\n"
                    "    zwolnienie wybranego rygla: unlock\n"
                    "    odczyt wybranego znaku pliku: read\n"
                    "    zmiana wybranego znaku pliku: write (program osobno zapyta o nowy znak)\n"
                    "    zakonczenie pracy: exit\n");

    char *cmd = 0;
    long int byteid = -1;
    int noblock = 0;
    char *input = malloc(100 * sizeof(char)); //fgets needs allocation
    fgets(input, 100 * sizeof(char), stdin);
    int file = open(argv[1], O_RDWR);
    if (file == -1)
    {
        printf ("Wystapil blad przy otwieraniu pliku.\n");
        perror(NULL);
        exit(1);
    }

    struct flock param;
    param.l_start = byteid;
    param.l_whence = SEEK_SET;
    param.l_len = 1;
    param.l_pid = getpid();
    char buf;

    interpret_input(&input, &cmd, &byteid, &noblock);
    //after that procedure, *input and *cmd point the same address

    if (strcmp(cmd, "exit") == 0) printf("\nDo zobaczenia!\n");

    while (strcmp(cmd, "exit") != 0)       //TODO: CHECK IF THERE'S NO MEMORY LEAKS
    {
        int recognised = 0;
        long err = 0;


        if (strcmp(cmd, "lockw") == 0)
        {
            recognised = 1;
            param.l_type = F_WRLCK;
            param.l_start = byteid;
            if (noblock) err = fcntl(file, F_SETLK, &param);
            else err = fcntl(file, F_SETLKW, &param);
        }
        if (strcmp(cmd, "lockr") == 0)
        {
            recognised = 1;
            param.l_type = F_RDLCK;
            param.l_start = byteid;
            if (noblock) err = fcntl(file, F_SETLK, &param);
            else err = fcntl(file, F_SETLKW, &param);
        }
        if (strcmp(cmd, "listl") == 0)
        {
            recognised = 1;
            print_locks(file);
        }
        if (strcmp(cmd, "unlock") == 0)
        {
            recognised = 1;
            param.l_type = F_UNLCK;
            param.l_start = byteid;
            err = fcntl(file, F_SETLK, &param);
        }
        struct flock lock_flag;
        if (strcmp(cmd, "read") == 0)
        {
            recognised = 1;
            lock_flag = check_lock(file, byteid);
            if (lock_flag.l_type == F_UNLCK || lock_flag.l_type == F_RDLCK)
            {
                lseek (file, byteid, SEEK_SET);
                err = read(file, &buf, 1);
                printf("%c\n", buf);
            }
            else printf ("Niestety, na ten rekord nalozono kolidujaca blokade.\n");

        }
        if (strcmp(cmd, "write") == 0)
        {
            recognised = 1;
            lock_flag = check_lock(file, byteid);
            if (lock_flag.l_type == F_UNLCK)
            {
                printf("\nProsze podac znak do wpisania\n");
                buf = (char) fgetc(stdin);
                char flush =  (char) fgetc(stdin);          //flushing necessary because after getting a char a /n sign lands in *input. warning says it's unused, but actually it is used as litter bin
                lseek (file, byteid, SEEK_SET);
                err = write(file, &buf, 1);
            }
            else printf ("Niestety, na ten rekord nalozono kolidujaca blokade.\n");

        }


        if (!recognised) printf("Nie rozpoznano polecenia.\n");
        else if (err >= 0) printf("Zakonczono. \n");
        else
        {
            printf("Wystapil problem\n");
            perror(NULL);
        };

        free(cmd);     //TODO: CHECK IF THERE'S NO MEMORY LEAKS
        cmd = NULL;
        input = NULL;
        //no need to free input- it would cause SIGABRT, because this space was freed while freeing cmd

        input = malloc(100 * sizeof(char)); //fgets needs allocation

        printf (">");
        fgets(input, 100 * sizeof(char), stdin);
        interpret_input(&input, &cmd, &byteid, &noblock);
        //after that procedure, *input and *cmd point the same address

        if (cmd != NULL && strcmp(cmd, "exit") == 0) printf("\nDo zobaczenia!\n");
    }

    close(file);

    return 0;
}