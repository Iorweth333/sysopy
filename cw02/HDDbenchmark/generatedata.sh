echo "generating datafiles:"

./perf_cmp g generate data1 $a1 $b1
./perf_cmp g generate data1c $c1 $b1
./perf_cmp g generate data2 $a2 $b2
./perf_cmp g generate data2c $c2 $b2
./perf_cmp g generate data3 $a3 $b3
./perf_cmp g generate data3c $c3 $b3
./perf_cmp g generate data4 $a4 $b4
./perf_cmp g generate data4c $c4 $b4

echo " "
echo "making copies of datafiles..."  #every test requires two identical files
cp data1 data1b
cp data1c data1bc
cp data2 data2b
cp data2c data2bc
cp data3 data3b
cp data3 data3bc
cp data4 data4b
cp data4c data4bc
echo "copies made"
echo " "
