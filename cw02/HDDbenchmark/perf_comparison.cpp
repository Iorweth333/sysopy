#include <iostream>
#include <string>
#include <sstream>
#include <time.h>
#include <stdio.h>  //biblioteka do funkcji... ekhem, bibliotecznych
#include <stdlib.h>
#include <fcntl.h>    //biblioteka do funkcji systemowej open(), zawiera m. in. O_RDWR
#include <unistd.h> //biblioteka do funkcji systemowych: open, lseek, read, write, close
#include "cmake-build-debug/test_of_sorting.h"


using namespace std;


unsigned int string_to_int(string num)
{
    unsigned int res = 0;
    unsigned int mul = 1;

    if (num[0] == '-')
    {
        cout << "Liczby ujemne nie sa akceptowane" << endl;
        exit(1);
    }

    for (long i = num.size() - 1; i >= 0; i--)
    {
        res += ((num[i] - 48) * mul);
        mul *= 10;
    }
    return res;
}

char *read_randoms(unsigned int record_len, unsigned int record_num, FILE *handle)
{
    char *res = (char *) malloc(record_num * record_len);    //In C++, you need to cast the return of malloc()

    unsigned int counter = 0;

    while (counter < record_num * record_len)
    {
        counter += fread(res + counter, 1, 1, handle);  //this is made very poorly when it comes to performance, but it's because dev/random is inapropriate to get many random numbers in short time


    }

    return res;

/*
    if (record_len * record_num == fread (res, record_len, record_num, handle)) return res;
    else
    {
	cout<<"Odczyt z pliku randomowego (dev/random) sie nie powiodl"<<endl;
	return 0;
    }
*/
}


bool generate (string filename, unsigned int record_len, unsigned int record_num)
{
    FILE *handle = fopen(filename.c_str(), "w");
    //opens or creates file; b open as binary, not text,
    // a+ append/update which allows to read (anwhere) and write (only at the end),
    // w  write. writing only at the end
    // r+ read/update: Open a file for update (both for input and output). The file must exist.
    FILE *devrandom = fopen("/dev/random",
                            "r");     //"r+"	read/update: Open a file for update (both for input and output). The file must exist.

    if (handle == NULL)
    {
        perror(filename.c_str());
        return false;
    }

    char *buffer = read_randoms(record_len, record_num, devrandom);
    if (buffer == 0) return false;

    unsigned int counter = 0;
    while (counter < record_num * record_len)
    {
        counter += fwrite(buffer + counter, 1, 1, handle);
        //size_t fwrite ( const void * ptr, size_t size, size_t count, FILE * stream );

    }

    cout << "Zapisano " << counter << " bajtow." << endl;
    free(buffer);
    return true;
    //else return false;

}

void lib_shuffle(string filename, unsigned int record_len, unsigned int record_num)
{
    FILE *handle = fopen(filename.c_str(), "r+");
    //opens or creates file; b open as binary, not text,
    // a+ append/update which allows to read (anwhere) and write (only at the end),
    // w  write. writing only at the end
    // r+ read/update: Open a file for update (both for input and output). The file must exist.

    if (handle == NULL)
    {
        cout << "Nie udalo sie otworzyc pliku" << endl;
        perror(filename.c_str());
    }
    else
    {
        char *bufferA = (char *) malloc(record_len);
        char *bufferB = (char *) malloc(record_len);
        //In C++, you need to cast the return of malloc()

        unsigned int i, j;

        for (i = 0; i < record_num; i++)
        {
            j = rand() % record_num;    //choosing record to swap

            //omitting exeption handling because it would cause even worse mess in the code


            fseek(handle, i * record_len, SEEK_SET);
            //int fseek ( FILE * stream, long int offset, int origin );
            //SEEK_SET - Offset is to be measured in absolute terms.
            fread(bufferA, record_len, 1, handle);
            fseek(handle, j * record_len, SEEK_SET);
            fread(bufferB, record_len, 1, handle);

            //swapping:

            fseek(handle, i * record_len, SEEK_SET);
            fwrite(bufferB, record_len, 1, handle);
            fseek(handle, j * record_len, SEEK_SET);
            fwrite(bufferA, record_len, 1, handle);
        }

        free(bufferA);
        free(bufferB);
        fclose(handle);
    }
}


/*
Generally, system calls are slower than normal function calls.
The reason is because when you call a system call, control is relinquished to the operating system
to perform the system call. In addition, depending on the nature of the system call, your program may be blocked
by the OS until the system call has finished, thus making the execution time of your program even longer.
*/


void sys_shuffle(string filename, unsigned int record_len, unsigned int record_num)
{
    int descriptor = open(filename.c_str(), O_RDWR);
    //int open(const char *path, int oflags); returns file descriptor

    if (descriptor < 0)
    {
        cout << "Nie powiodlo sie otwieranie pliku. Zadanie nie zostalo wykonane" << endl;
    }
    else
    {
        char *bufferA = (char *) malloc(record_len);
        char *bufferB = (char *) malloc(record_len);
        //In C++, you need to cast the return of malloc()

        unsigned int i, j;

        for (i = 0; i < record_num; i++)
        {
            j = rand() % record_num;    //choosing record to swap

            //omitting exeption handling because it would cause even worse mess in the code


            lseek(descriptor, i * record_len, SEEK_SET);
            //off_t lseek(int fildes, off_t offset, int whence);
            //SEEK_SET - Offset is to be measured in absolute terms.
            read(descriptor, bufferA, record_len);
            lseek(descriptor, j * record_len, SEEK_SET);
            read(descriptor, bufferB, record_len);

            //swapping:

            lseek(descriptor, i * record_len, SEEK_SET);
            write(descriptor, bufferB, record_len);
            lseek(descriptor, j * record_len, SEEK_SET);
            write(descriptor, bufferA, record_len);
        }

        free(bufferA);
        free(bufferB);
        close(descriptor);
    }

}

void lib_bubble_sort(string filename, unsigned int record_len, unsigned int record_num)
{
    FILE *handle = fopen(filename.c_str(), "r+");

    if (handle == NULL)
    {
        cout << "Nie powiodlo sie otwieranie pliku. Zadanie nie zostalo wykonane" << endl;
    }
    else
    {
        char *bufferA = (char *) malloc(record_len);
        char *bufferB = (char *) malloc(record_len);
        //In C++, you need to cast the return of malloc()

        for (int i = record_num - 2; i >= 0; i--)
        {
            for (int j = 0; j <= i; j++)
            {
                fseek(handle, j * record_len, SEEK_SET);
                //int fseek ( FILE * stream, long int offset, int origin );
                //SEEK_SET - Offset is to be measured in absolute terms.
                fread(bufferA, record_len, 1, handle);
                //size_t fread ( void * ptr, size_t size, size_t count, FILE * stream );
                fseek(handle, (j+1) * record_len, SEEK_SET);
                fread(bufferB, record_len, 1, handle);

                if (bufferA[0] > bufferB[0])
                {
                    //swapping:

                    fseek(handle, (j+1) * record_len, SEEK_SET);
                    fwrite(bufferA, record_len, 1, handle);
                    fseek(handle, j * record_len, SEEK_SET);
                    fwrite(bufferB, record_len, 1, handle);
                }
            }
        }

        free(bufferA);
        free(bufferB);
        fclose(handle);



    }
}


//SORT DEBUGGING:
//if anything isn't right about sorting, check if records are really being swapped

void sys_bubble_sort(string filename, unsigned int record_len, unsigned int record_num)
{
    int descriptor = open(filename.c_str(), O_RDWR);
    if (descriptor < 0)
    {
        cout << "Nie powiodlo sie otwieranie pliku. Zadanie nie zostalo wykonane" << endl;
    }
    else
    {
        char *bufferA = (char *) malloc(record_len);
        char *bufferB = (char *) malloc(record_len);

        for (int i = record_num - 2; i >= 0; i--)
        {
            for (int j = 0; j <= i; j++)
            {
                lseek(descriptor, j * record_len, SEEK_SET);
                //off_t lseek(int fildes, off_t offset, int whence);
                //SEEK_SET - Offset is to be measured in absolute terms.
                read(descriptor, bufferA, record_len);
                lseek(descriptor, (j+1) * record_len, SEEK_SET);
                read(descriptor, bufferB, record_len);

                if (bufferA[0] > bufferB[0])
                {
                    //swapping:

                    lseek(descriptor, (j+1) * record_len, SEEK_SET);
                    write(descriptor, bufferA, record_len);
                    lseek(descriptor, j * record_len, SEEK_SET);
                    write(descriptor, bufferB, record_len);
                }
            }
        }

        free(bufferA);
        free(bufferB);
        close(descriptor);

    }
}

int main(int argc, char *argv[])
{
    srand(time(NULL));

    if (argc < 6)
    {
        cout
                << "Niedostateczna ilosc argumentow. \nWpisz: sys/lib/g, shuffle/generate/sort, nazwe pliku,  liczbe rekodow i ich wielkosc. W przypadku polecenia generate, jako pierwszy argument podaj litere g (pozostale tak samo)."
                << endl;
        return 1;
    }

    string sys_or_lib = argv[1];        //whether to use system's or library's functions
    string task = argv[2];              //shuffle, generate or sort
    string filename = argv[3];
    string record_number = argv[4];
    string record_length = argv[5];

    unsigned int record_len = string_to_int(record_length);
    unsigned int record_num = string_to_int(record_number);

    if (task == "generate")
    {
/*
ASSURANCE
	cout<<"Jesli wskazany plik istnieje, zostanie nadpisany. Na pewno chcesz kontynuowac? T/N"<<endl;
	char yesno;
	cin>>yesno;
	if (yesno == 'n' or yesno == 'N') return 0;
	if (yesno != 't' and yesno != 'T')
	{
		cout<<"Nie rozpoznano polecenia. Nastapi zamkniecie programu."<<endl;
		return 0;
	}
*/
        if (generate(filename, record_len, record_num))
        {
            cout << "Powodzenie!\n";
            return 0;
        }
        else    //"do not use else after return" he says. but why? i know how to do it so that he didn't complain, but that way is imho less readable
        {
            cout << "Niepowodzenie!\n";
            return 1;
        }
    }

    if (task == "shuffle")
    {
        if (sys_or_lib == "sys")
        {
            sys_shuffle(filename, record_len, record_num);
            cout << "Powodzenie!";
            return 0;
        }
        if (sys_or_lib == "lib")
        {
            lib_shuffle(filename, record_len, record_num);
            cout << "Powodzenie!";
            return 0;
        }

        cout << "Nie rozpoznanio polecenia z pierwszego argumentu. \nWybierz sys lub lib.";
        return 1;
    }


    if (task == "sort")
    {
        if (sys_or_lib == "sys")
        {
            sys_bubble_sort(filename, record_len, record_num);
            cout << "Powodzenie!";
            return 0;
        }
        if (sys_or_lib == "lib")
        {
            lib_bubble_sort(filename, record_len, record_num);
            cout << "Powodzenie!";
            return 0;
        }
        cout << "Nie rozpoznanio polecenia z pierwszego argumentu. \nWybierz sys lub lib.";
        return 1;

    }
    cout<<"Polecenie nie zostalo rozpoznane\n";
    return 1;   //if program reached this line, arguments were not recognised
}

