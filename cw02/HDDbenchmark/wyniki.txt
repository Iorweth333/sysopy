WNIOSKI NA KOŃCU
w wysłanej paczce powinny się znaleźć następujące pliki:
generatedata.sh
makecopies.sh
Makefile
perf_comparison.cpp
tests.sh
tests2.sh
wyniki.txt


[karolb@localhost cw02]$ ./tests2.sh 
making program
make: `perf_cmp' jest aktualne.
made
 
 
restoring datafiles from copies...
copies restored
 
sort tests (sys, lib, sys, lib and so on):
test1: sort  2000 records that are  4 bytes long
Powodzenie!
real	0m1.592s
user	0m0.158s
sys	0m1.430s
Powodzenie!
real	0m3.887s		#BIBLIOTECZNE DWA RAZY DŁUŻSZE!
user	0m0.640s
sys	0m3.236s
test1 continued: sort  4000 records that are  4 bytes long
Powodzenie!
real	0m15.152s
user	0m0.974s
sys	0m14.136s
Powodzenie!
real	0m20.555s		#BIBLIOTECZNE O JEDNĄ TRZECIĄ DŁUŻSZE
user	0m3.104s
sys	0m17.394s
test1 done
 
test2: sort  2000 records that are  512 bytes long
Powodzenie!
real	0m5.343s		#TU CZAS ZACZYNA SIĘ WYRÓWNYWAĆ
user	0m0.306s
sys	0m5.022s
Powodzenie!
real	0m6.129s
user	0m1.057s
sys	0m5.055s
test2 continued: sort  4000 records that are  512 bytes long
Powodzenie!
real	0m22.667s
user	0m1.314s
sys	0m21.291s
Powodzenie!
real	0m25.898s
user	0m4.321s
sys	0m21.506s
test2 done
 
test3: sort  1000 records that are  4096 bytes long
Powodzenie!
real	0m2.310s		#TU JUŻ JEST PRAKTYCZNIE RÓWNY
user	0m0.103s
sys	0m2.201s
Powodzenie!
real	0m2.277s
user	0m0.216s
sys	0m2.055s
test3 continued: sort  2000 records that are  4096 bytes long
Powodzenie!
real	0m8.071s		#A TU Z KOLEI BIBLITECZNE ZACZYNAJĄ WYGRYWAĆ
user	0m0.350s
sys	0m7.700s
Powodzenie!
real	0m5.725s
user	0m0.611s
sys	0m5.099s
test3 done
 
test4: sort  1000 records that are  8192 bytes long
Powodzenie!
real	0m3.087s		#ZNOWU RÓWNY CZAS
user	0m0.091s
sys	0m2.988s
Powodzenie!
real	0m3.018s
user	0m0.222s
sys	0m2.788s
test4 continued: sort  2000 records that are  8192 bytes long
Powodzenie!
real	0m11.921s		#I ZNOWU BIBLIOTECZNE PRZEGRYWAJĄ...
user	0m0.352s
sys	0m11.536s
Powodzenie!
real	0m13.686s
user	0m0.903s
sys	0m12.747s
test4 done
 


shuffling tests (sys, lib, sy, lib and so on):
test5: shuffle  2000 records that are  4 bytes long
Powodzenie!
real	0m0.015s
user	0m0.002s
sys	0m0.014s
Powodzenie!
real	0m0.023s
user	0m0.004s
sys	0m0.020s
test5 continued: shuffle  4000 records that are  4 bytes long
Powodzenie!
real	0m0.028s
user	0m0.004s
sys	0m0.023s
Powodzenie!
real	0m0.049s
user	0m0.007s
sys	0m0.041s
test5 done
 
test6: shuffle  2000 records that are  512 bytes long
Powodzenie!
real	0m0.027s
user	0m0.004s
sys	0m0.020s
Powodzenie!
real	0m0.041s
user	0m0.005s
sys	0m0.035s
test6 continued: shuffle  4000 records that are  512 bytes long
Powodzenie!
real	0m0.036s
user	0m0.004s
sys	0m0.032s
Powodzenie!
real	0m0.058s
user	0m0.009s
sys	0m0.049s
test6 done
 
test7: shuffle  1000 records that are  4096 bytes long
Powodzenie!
real	0m0.015s		#WYRÓWNANIE
user	0m0.004s
sys	0m0.010s
Powodzenie!
real	0m0.016s
user	0m0.001s
sys	0m0.015s
test7 continued: shuffle  2000 records that are  4096 bytes long
Powodzenie!
real	0m0.030s
user	0m0.003s
sys	0m0.027s
Powodzenie!
real	0m0.029s
user	0m0.003s
sys	0m0.025s
test7 done
 
test8: shuffle  1000 records that are  8192 bytes long
Powodzenie!
real	0m0.027s		#BIBLIOTECZNE ZACZYNAJĄ WYGRYWAĆ
user	0m0.002s
sys	0m0.022s
Powodzenie!
real	0m0.025s
user	0m0.002s
sys	0m0.023s
test8 continued: shuffle  2000 records that are  8192 bytes long
Powodzenie!
real	0m0.047s
user	0m0.004s
sys	0m0.043s
Powodzenie!
real	0m0.042s
user	0m0.003s
sys	0m0.039s
test8 done
 
removing restored datafiles
removed



WNIOSKI
szok i niedowierzanie- funkcje biblioteczne okazują się nie być lepsze niż systemowe
to po cholerę one w ogóle istnieją?
efekt krótkiego google-fu:
"Dlaczego zatem używa się funkcji bibliotecznych? Przede wszystkim funkcje te są bardziej przyjazne - nie trzeba na przykład przejmować się obsługą buforów, o wiele trudniej jest się pomylić. Funkcje biblioteczne są też bardziej rozbudowane."
źrówdło:    https://4programmers.net/C/Artyku%C5%82y/Jak_programowa%C4%87_w_Linuksie
być może nie bez znaczenia jest to, że mam dysk SSD

-dwa razy większa liczba rekordów nie powoduje dwukrotnego zwiększenia czasu wykonywania- powoduje znacznie większy wzrost
-dało się zauważyć tendencję, że wraz ze wzrostem długości rekordu, przewaga funkcji systemowych malała
-dla czasu wykonywania większe znaczenie ma liczba rekordów niż ich długość




osobny, ręczny test, który sugeruje, że wzrost liczby rekordów zmniejsza przewagę funkcji systemowych (dla 2000 rekordów przewaga była ponad dwukrotna)

[karolb@localhost cw02]$ time ./perf_cmp sys sort datafile3 10000 4
Powodzenie!
real	1m48.434s
user	0m6.952s
sys	1m41.185s
[karolb@localhost cw02]$ time ./perf_cmp lib sort datafile4 10000 4
Powodzenie!
real	2m16.933s
user	0m20.792s
sys	1m55.757s



