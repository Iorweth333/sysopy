#what does the script do?
#basicall, it does my work ;)
#it runs test of the benchmark program

#parameters 
#aX - number of records
#cX - number of records (there has to be two test for each length of records)
#bX - length of records

a1=2000
c1=4000
b1=4

a2=2000
c2=4000
b2=512

a3=1000
c3=2000
b3=4096

a4=1000
c4=2000
b4=8192

s="sort"
shu="shuffle"

echo "making program"
make
echo "made"
echo " " #new line

echo "generating datafiles:"

./perf_cmp g generate data1 $a1 $b1
./perf_cmp g generate data1c $c1 $b1
./perf_cmp g generate data2 $a2 $b2
./perf_cmp g generate data2c $c2 $b2
./perf_cmp g generate data3 $a3 $b3
./perf_cmp g generate data3c $c3 $b3
./perf_cmp g generate data4 $a4 $b4
./perf_cmp g generate data4c $c4 $b4

echo " "
echo "making copies of datafiles..."
cp data1 data1b
cp data1c data1bc
cp data2 data2b
cp data2c data2bc
cp data3 data3b
cp data3 data3bc
cp data4 data4b
cp data4c data4bc
echo "copies made"
echo " "

echo "sort tests (sys, lib, sys, lib and so on):"
echo "test1: sort " $a1 "records that are " $b1 "bytes long"
time ./perf_cmp sys $s data1 $a1 $b1 
time ./perf_cmp lib $s data1b $a1 $b1 
echo "test1 continued: sort " $c1 "records that are " $b1 "bytes long"
time ./perf_cmp sys $s data1c $c1 $b1 
time ./perf_cmp lib $s data1bc $c1 $b1 
echo "test1 done"
echo " "

echo "test2: sort " $a2 "records that are " $b2 "bytes long"
time ./perf_cmp sys $s data2 $a2 $b2 
time ./perf_cmp lib $s data2b $a2 $b2 
echo "test2 continued: sort " $c2 "records that are " $b2 "bytes long"
time ./perf_cmp sys $s data2c $c2 $b2 
time ./perf_cmp lib $s data2bc $c2 $b2 
echo "test2 done"
echo " "

echo "test3: sort " $a3 "records that are " $b3 "bytes long"
time ./perf_cmp sys $s data3 $a3 $b3 
time ./perf_cmp lib $s data3b $a3 $b3 
echo "test3 continued: sort " $c3 "records that are " $b3 "bytes long"
time ./perf_cmp sys $s data3c $c3 $b3 
time ./perf_cmp lib $s data3bc $c3 $b3 
echo "test3 done"
echo " "

echo "test4: sort " $a4 "records that are " $b4 "bytes long"
time ./perf_cmp sys $s data4 $a4 $b4 
time ./perf_cmp lib $s data4b $a4 $b4 
echo "test4 continued: sort " $c4 "records that are " $b4 "bytes long"
time ./perf_cmp sys $s data4c $c4 $b4 
time ./perf_cmp lib $s data4bc $c4 $b4 
echo "test4 done"
echo " "

echo "shuffling tests (sys, lib, sy, lib and so on):"
echo "test5: shuffle " $a1 "records that are " $b1 "bytes long"
time ./perf_cmp sys $shu data1 $a1 $b1 
time ./perf_cmp lib $shu data1b $a1 $b1 
echo "test5 continued: shuffle " $c1 "records that are " $b1 "bytes long"
time ./perf_cmp sys $shu data1c $c1 $b1 
time ./perf_cmp lib $shu data1bc $c1 $b1 
echo "test5 done"
echo " "

echo "test6: shuffle " $a2 "records that are " $b2 "bytes long"
time ./perf_cmp sys $shu data2 $a2 $b2 
time ./perf_cmp lib $shu data2b $a2 $b2 
echo "test6 continued: shuffle " $c2 "records that are " $b2 "bytes long"
time ./perf_cmp sys $shu data2c $c2 $b2 
time ./perf_cmp lib $shu data2bc $c2 $b2 
echo "test6 done"
echo " "

echo "test7: shuffle " $a3 "records that are " $b3 "bytes long"
time ./perf_cmp sys $shu data3 $a3 $b3 
time ./perf_cmp lib $shu data3b $a3 $b3 
echo "test7 continued: shuffle " $c3 "records that are " $b3 "bytes long"
time ./perf_cmp sys $shu data3c $c3 $b3 
time ./perf_cmp lib $shu data3bc $c3 $b3 
echo "test7 done"
echo " "

echo "test8: shuffle " $a4 "records that are " $b4 "bytes long"
time ./perf_cmp sys $shu data4 $a4 $b4 
time ./perf_cmp lib $shu data4b $a4 $b4 
echo "test8 continued: shuffle " $c4 "records that are " $b4 "bytes long"
time ./perf_cmp sys $shu data4c $c4 $b4 
time ./perf_cmp lib $shu data4bc $c4 $b4 
echo "test8 done"
echo " "

echo "removing datafiles"
rm data1
rm data2
rm data3
rm data4
rm data1b
rm data2b
rm data3b
rm data4b
rm data1c
rm data2c
rm data3c
rm data4c
rm data1bc
rm data2bc
rm data3bc
rm data4bc
echo "removed"

echo " "
echo " "	#endl
