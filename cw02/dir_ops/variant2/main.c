#define _XOPEN_SOURCE 500//it tells compiler to include definitions for some extra functions that are defined in the X/Open and POSIX standards.

#include <stdio.h>
#include <ftw.h>
#include <stdlib.h>
#include "time.h"

/*
int nftw(const char *dirpath,
         int (*fn) (const char *fpath, const struct stat *sb,
                    int typeflag, struct FTW *ftwbuf),
         int nopenfd, int flags);


    ścieżka bezwzględna pliku
    rozmiar w bajtach
    prawa dostępu do pliku (w formacie używanym przez ls -l, np. r--r--rw- oznacza prawa odczytu dla wszystkich i zapisu dla właściciela)
    datę ostatniej modyfikacji

*/

int filemaxsize;

char *formatdate(char *str,
                 time_t val)     //https://stackoverflow.com/questions/26306644/how-to-display-st-atime-and-st-mtime
{
    strftime(str, 36, "%d.%m.%Y %H:%M:%S", localtime(&val));
    return str;
}


static int display_info
        (const char *fpath, const struct stat *sb, int tflag, struct FTW *ftwbuf)
{
    if (tflag == FTW_F && sb->st_size <= filemaxsize)
    {
        printf("%s\n", fpath);
        printf("File Size: %i\n", (int) sb->st_size);
        printf("File Permissions: \t");
        printf((S_ISDIR(sb->st_mode)) ? "d" : "-");
        printf((sb->st_mode & S_IRUSR) ? "r" : "-");
        printf((sb->st_mode & S_IWUSR) ? "w" : "-");
        printf((sb->st_mode & S_IXUSR) ? "x" : "-");
        printf(" ");
        printf((sb->st_mode & S_IRGRP) ? "r" : "-");
        printf((sb->st_mode & S_IWGRP) ? "w" : "-");
        printf((sb->st_mode & S_IXGRP) ? "x" : "-");
        printf(" ");
        printf((sb->st_mode & S_IROTH) ? "r" : "-");
        printf((sb->st_mode & S_IWOTH) ? "w" : "-");
        printf((sb->st_mode & S_IXOTH) ? "x" : "-");

        /*
        timespec_get(&sb->st_mtime, TIME_UTC);
        char buff[100];
        strftime(buff, sizeof buff, "%D %T", gmtime(&sb->st_mtime));
        printf("\nLast modification time: %s.%09ld UTC\n\n", buff, sb->.st_mtim.tv_nsec);
         //this version doesn't fit here, because apparently struct stat used here is different than one used in variant1
         */
        char date[36];

        printf("\nLast modification time: %s\n\n", formatdate(date,
                                                              sb->st_mtime)); //https://stackoverflow.com/questions/26306644/how-to-display-st-atime-and-st-mtime
    }


    return 0; /* To tell nftw() to continue */
}

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        printf("Za malo argumentow! Nalezy podac sciezke do katalogu oraz liczbe bajtow\n"
                       "Program znajdzie pliki o rozmiarze nie wiekszym niz podany i wypisze podstawowe informacje o nich\n");
        return 1;
    }

    char *dirpath = argv[1];
    filemaxsize = atoi(argv[2]);
    int flags = 0;
    flags |= FTW_PHYS;  //If set, do not follow symbolic links.
    flags |= FTW_DEPTH; //If set, do a post-order traversal, that is, call fn() for the directory itself after handling
                        // the contents of the directory and its subdirectories. (By default, each directory is handled before its contents.)
    if (nftw(dirpath, display_info, 20, flags) == -1)
    {
        perror("nftw");
        exit(EXIT_FAILURE);
    }

    printf("\nZakonczono\n");
    return 0;
}