//variant1 with opendir, readdir and functions from stat's family

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include "string.h"
#include <time.h>

void searchdir (DIR *dir, char *dirpath, int maxsize)  
{
    struct dirent *ent = readdir(dir);  //On success, readdir() returns a pointer to a dirent structure.  (This
                                        //structure may be statically allocated; do not attempt to free(3) it.)
    while (ent != NULL)
    {

        if (ent->d_type == DT_DIR && (strcmp(".", ent->d_name) != 0 && strcmp("..", ent->d_name) != 0))
        {
            char ndir_path [PATH_MAX+1];
            strcpy(ndir_path, dirpath);
            strcat(ndir_path, "/");
            strcat(ndir_path, ent->d_name);
            DIR *ndir = opendir (ndir_path);
            if (!ndir)
            {
                printf ("Nie udalo sie otworzyc katalogu: %s  \nByc moze zapomniales o slashu na poczatku?\n", ndir_path );
                perror(NULL);
                exit(1);
            }
            searchdir(ndir, ndir_path, maxsize);       //recursive call
        }
        if (ent->d_type == DT_REG)
        {
            struct stat stats;
            char reg_path [PATH_MAX+1];
            //realpath (ent->d_name, reg_path);     //doesn't work properly. no big suprise, it looked like a magic from the very beginning
            strcpy(reg_path, dirpath);
            strcat(reg_path, "/");
            strcat(reg_path, ent->d_name);

            if (stat(reg_path, &stats) == 0)
            {
                if (stats.st_size <= maxsize)
                {
                    printf("%s \n", reg_path);
                    printf ("File Size: %i \n", stats.st_size);   //off_t is supposed to be signed integer
                    printf("File Permissions: \t");
                    printf( (S_ISDIR(stats.st_mode)) ? "d" : "-");
                    printf( (stats.st_mode & S_IRUSR) ? "r" : "-");
                    printf( (stats.st_mode & S_IWUSR) ? "w" : "-");
                    printf( (stats.st_mode & S_IXUSR) ? "x" : "-");
                    printf(" ");
                    printf( (stats.st_mode & S_IRGRP) ? "r" : "-");
                    printf( (stats.st_mode & S_IWGRP) ? "w" : "-");
                    printf( (stats.st_mode & S_IXGRP) ? "x" : "-");
                    printf(" ");
                    printf( (stats.st_mode & S_IROTH) ? "r" : "-");
                    printf( (stats.st_mode & S_IWOTH) ? "w" : "-");
                    printf( (stats.st_mode & S_IXOTH) ? "x" : "-");
                    timespec_get(&stats.st_mtim, TIME_UTC);
                    char buff[100];
                    strftime(buff, sizeof buff, "%D %T", gmtime(&stats.st_mtim.tv_sec));
                    printf("\nLast modification time: %s.%09ld UTC\n\n", buff, stats.st_mtim.tv_nsec);
                }

            }
            else
            {
                perror(NULL);
                printf ("%s \n",ent->d_name);
                printf ("%s \n",reg_path);
            }

        }

        //DEBUG
        //printf ("%s \n",ent->d_name);
        ent = readdir(dir);
    }
}


int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        printf("Za malo argumentow! Nalezy podac sciezke do katalogu oraz liczbe bajtow\n"
                       "Program znajdzie pliki o rozmiarze nie wiekszym niz podany i wypisze podstawowe informacje o nich\n");
        return 1;
    }

    char *dirpath = argv[1];
    int maxsize = atoi(argv[2]);
    DIR *dir = opendir(dirpath);

    if (!dir)
    {
        printf ("Nie udalo sie otworzyc katalogu: %s  \nByc moze zapomniales o slashu na poczatku?\n", dirpath );
        perror(NULL);
        return 1;
    }
    searchdir(dir, dirpath, maxsize);

    printf ("\nZakonczono\n");
    return 0;
}
