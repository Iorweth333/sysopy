#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include "lib/complex.h"

#define re_max 1.0
#define re_min (-2.0)
#define im_max 1.0
#define im_min (-1.0)

typedef struct
{
    long x;
    long y;
    long val;
} point;

point complex_to_point(complex c, long val, long r)
{
    double rangeX = re_max - re_min;        //width of interval of possible values
    double rangeY = im_max - im_min;

    double x = c.re - re_min;
    double y = c.im - im_min;

    point res;
    res.x = (long) (x * r / rangeX);        //x * scale
    res.y = (long) (y * r / rangeY);
    res.val = val;
    return res;
}

void arr2D_to_file(long **arr, long lines, long columns, FILE *file)
{
    for (long i = 0; i < lines; i++)
        for (long j = 0; j < columns; j++)
        {
            fprintf (file, "%ld %ld %ld\n", i, j, arr[i][j]);
        }
}

int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        printf("Mandelbrot_master.\n"
                       "Too few arguments.\nPlease provide path to fifo and integer R.\n");
        exit(1);
    }

    char *fifo_path = argv[1];
    long r = strtol(argv[2], NULL, 10);

    int err = mkfifo(fifo_path,
                     S_IRUSR | S_IWUSR);     //S_IRUSR read permission, owner, S_IWUSR write permission, owner
    //I think it's weird there is no S_IWRUSR mode
    if (err < 0)
    {
        printf("Error while creating fifo. Terminating.");
        perror(NULL);
        exit(1);
    }

    //DEBUG
    //printf ("Fifo created. Waitng for permission to continue. Press any key\n");
    //getc(stdin);

    FILE *fifo = fopen(fifo_path, "r");

    if (fifo == NULL)
    {
        printf("Error while opening fifo. Terminating.");
        perror(NULL);
        exit(1);
    }


    long **arr = calloc((size_t) r, sizeof(long *));

    for (int i = 0; i < r; i++) arr[i] = calloc((size_t) r, sizeof(long));

    complex c;
    long iters;

    //real stuff starts here

    //DEBUG
    printf ("reading from fifo and filling array\n");

    while (fscanf(fifo, "%lf %lf %ld", &c.re, &c.im, &iters) != EOF)    //this line used to cause problems- when there was fputs instead of fprintf in _slave, saved data could get corrupt
    {
        point tmp = complex_to_point(c, iters, r);
        //DEBUG
        /*
        printf ("Printing tmp: %ld %ld %ld\n", tmp.x, tmp.y, tmp.val);
        printf ("printing c: %lf %lf ", c.re, c.im);
        fflush(stdout);
        if (tmp.x < 0 || tmp.y < 0 || tmp.x > r || tmp.y > r)
        {
            printf ("printing c: %lf %lf ", c.re, c.im);
        }
*/

        arr[tmp.x][tmp.y] = tmp.val;
    }

    fclose(fifo);

    //DEBUG
    printf ("saving data into file\n");

    FILE *datafile = fopen("data", "w");
    arr2D_to_file(arr, r, r, datafile);
    fclose (datafile);

    //DEBUG
    printf ("using Gnuplot\n");

    //from documentation of popen():
    // Since the standard input of a command opened for reading shares its seek offset with the process that called popen(),
    // if the original process has done a buffered read, the command's input position may not be as expected.
    // Similarly, the output from a command opened for writing may become intermingled with that of the original process.
    // The latter can be avoided by calling fflush(3) before popen().
    //fflush(stdout);
    FILE *gnuplot = popen("gnuplot", "w");
    if (gnuplot == NULL)
    {
        printf("Failed opening gnuplot. Terminating.\n");
        perror(NULL);
        exit(1);
    }

    fprintf(gnuplot, "set view map\n");
    fprintf(gnuplot, "set xrange [0:%ld]\n", r);
    fprintf(gnuplot, "set yrange [0:%ld]\n", r);
    fprintf(gnuplot, "plot 'data' with image\n");

    fflush(gnuplot);
    printf ("press any key to close\n");
    getc(stdin);
    pclose(gnuplot);

    return 0;
}