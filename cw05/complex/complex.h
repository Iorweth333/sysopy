#ifndef COMPLEX_LIBRARY_H
#define COMPLEX_LIBRARY_H

//#include <math.h>


typedef struct {
    double re;      //realis
    double im;      //imaginalis
}complex;

complex add(complex c1, complex c2);
complex sub(complex c1, complex c2);
complex mul(complex c1, complex c2);
complex square (complex c);
double mod (complex c);
void print_complex (complex c);

#endif