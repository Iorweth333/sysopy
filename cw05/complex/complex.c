#include "complex.h"

#include <stdio.h>
#include <math.h>


complex add(complex c1, complex c2)
{
    complex res;
    res.re = c1.re + c2.re;
    res.im = c1.im + c2.im;
    return res;
}

complex sub(complex c1, complex c2)
{
    complex res;
    res.re = c1.re - c2.re;
    res.im = c1.im - c2.im;
    return res;
}

complex mul(complex c1, complex c2)
{
    complex res;
    res.re = c1.re * c2.re - c1.im * c2.im;
    res.im = c1.re * c2.im + c1.im * c2.re;
    return res;
}

complex square (complex c)
{
    return mul(c,c);
}

double mod (complex c)
{
    double re_sq = c.re * c.re;
    double im_sq = c.im * c.im;
    return sqrt (re_sq + im_sq);
}

void print_complex (complex c)
{
    printf ("re: %f im: %f\n", c.re, c.im);
}

