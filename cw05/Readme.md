How to use Mandelbrot:
first install gnuplot
then open at least two terminals- one for master and others for slaves
run master and then run slaves

in my case it looks like this:
terminal 1
./Mandelbrot_master/Mandelbrot_master /home/karolb/sysopy/cw05/fifo 600

other terminals:
./Mandelbrot_slave/Mandelbrot_slave /home/karolb/sysopy/cw05/fifo 1000000 100


i thought about writing a script but I don't know how to run programs parallel
