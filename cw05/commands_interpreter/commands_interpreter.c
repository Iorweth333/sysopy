#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <wait.h>

void clear_str(char *str, int len)
{
    for (int i = 0; i < len; i++) str[i] = 0;
}

void print_arr_3d(char const ***arr, const int dim1, const int *dim2)
{
    for (int i = 0; i < dim1; i++)
    {
        for (int j = 0; j < dim2[i]; j++) printf("%i %i: %s \n", i, j, arr[i][j]);
    }
}

void free_arr_3d(char ***arr, const int dim1, const int *dim2)
{
    for (int i = 0; i < dim1; i++)
    {
        for (int j = 0; j < dim2[i]; j++) free(arr[i][j]);
        free(arr[i]);
    }
    free(arr);
}

char ***input_interpreter(char *input, int entry_len, int *calls_num, int **calls_sizes)
{

    *calls_num = 1;

    for (unsigned int i = 0; i < strlen(input); i++) if (input[i] == '|') (*calls_num)++;
    //pipes split calls, need number of them to malloc


    char ***res = (char ***) malloc(*calls_num * sizeof(char **));
    *calls_sizes = malloc(*calls_num * sizeof(int));

    char *cmd = (char *) 1;

    int i = 0;

    char *cmd_saveptr = 0;
    cmd = strtok_r(input, "|", &cmd_saveptr);

    while (cmd != NULL)
    {
        char *path_arg = (char *) 1;    //path or argument
        char *path_args[50];       //array for path and arguments, must be fxed size because I can't come up with any other idea
        int words_num = 0;          // on how to cut the command into words and put them into result array withot knowing how much memory to malloc
        //DAMN IT! there is strlen()... why did i notice it just when i'm done...
        //TODO: make this function more general, wipe out fixed-size array and next time do proper research before coding!
        char *path_arg_saveptr = 0;
        path_arg = strtok_r(cmd, " \n", &path_arg_saveptr);

        if (path_arg != NULL)
        {
            path_args[words_num] = path_arg;
            words_num++;
        }

        while (path_arg != NULL)
        {
            path_arg = strtok_r(NULL, " \n", &path_arg_saveptr);
            if (path_arg != NULL)
            {
                path_args[words_num] = path_arg;
                words_num++;
            }
        }
        (*calls_sizes)[i] = words_num;
        res[i] = malloc(words_num * sizeof(char *));
        for (int j = 0; j < words_num; j++)
        {
            res[i][j] = strdup(path_args[j]);       //strdup has it's own malloc
        }

        cmd = strtok_r(NULL, "|", &cmd_saveptr);
        i++;
    }
    return res;
}

int main()
{

    int pipe_desc[100][2];


    int max_input_len = 1000;                           //1000 chars should do

    char *input = malloc(max_input_len * sizeof(char)); //fgets needs allocation,
    clear_str(input, max_input_len);
    char ***commands = 0;

    printf(">");
    fgets(input, max_input_len, stdin);


    while (strcmp(input, "exit") != 0 && strcmp(input, "exit\n") != 0)
    {
        int calls_num = 0;
        int *calls_sizes = 0;
        if (input != NULL) commands = input_interpreter(input, max_input_len, &calls_num, &calls_sizes);

        //DEBUG
        //print_arr_3d(commands, calls_num, calls_sizes);
        clear_str(input, max_input_len);

        if (calls_num == 1)
        {
            int pid = fork();
            if (pid == 0)
            {
                //child with no need for pipes
                if (execvp(commands[0][0], commands[0]) == -1)
                {
                    printf("Unable to execute the command\n");
                    perror(NULL);
                    exit(-1);
                }
            }
            int status;
            wait(&status);
        }
        else
        {
            for (int i = 0; i < calls_num - 1; i++)
            {                                       //creating pipes. i < calls_num - 1 cause I don't need the last one- last output will just print
                if (pipe(pipe_desc[i]) != 0)                            //pipe[1]-for writing, pipe[0]- for reading
                {
                    printf("Error while creating pipe. Terminating\n");
                    perror(NULL);
                    exit(1);
                }
            }


            int pid = fork();
            if (pid == 0)
            {
                //first child
                close(pipe_desc[0][0]);   //closing reading end
                dup2(pipe_desc[0][1], STDOUT_FILENO);
                if (execvp(commands[0][0], commands[0]) == -1)
                {
                    printf("Unable to execute first command\n");
                    perror(NULL);
                    exit(-1);
                }
            }
            else
            {
                //parent
                close(pipe_desc[0][1]);       //must close writing end so that child received EOF

                int status;
                //wait(&status);


                for (int i = 1; i < calls_num - 1; i++)
                {

                    pid = fork();
                    if (pid == 0)
                    {
                        //child

                        //below is wrong: child would have both ends opened and it wouldn't ever get EOF while reading
                        //dup2(pipe_desc[1], STDOUT_FILENO);
                        //dup2(pipe_desc[0], STDIN_FILENO);
                        //wrong ^^

                        dup2(pipe_desc[i - 1][0], STDIN_FILENO);    //getting data from previous child
                        dup2(pipe_desc[i][1], STDOUT_FILENO);       //and putting current's output to it's pipe
                        if (execvp(commands[i][0], commands[i]) == -1)
                        {
                            printf("Unable to execute %ith command\n", i);
                            perror(NULL);
                            exit(-1);
                        }
                    }
                    else
                    {
                        //parent
                        close(pipe_desc[i][1]);
                        //wait(&status);
                    }
                }

                pid = fork();
                if (pid == 0)
                {
                    //last child
                    dup2(pipe_desc[calls_num - 2][0], STDIN_FILENO);
                    //not changing stdout- important, so that last child's output lands on screen, not in pipe
                    if (execvp(commands[calls_num - 1][0], commands[calls_num - 1]) == -1)
                    {
                        printf("Unable to execute last command\n");
                        perror(NULL);
                        exit(-1);
                    }
                }
                else
                {
                    //close(pipe_desc[calls_num - 1][1]);       //<-- this close() made me search for a bug for at least two hour
                    //it caused fgets to fail and return 'Bad file descriptor' as errno
                    //the conclusion is that it closed stdin, but how did stdin found itself in pipe_desc[calls_num - 1][1] - kill me, but idk
                    //especially when there is no such pipe since I create (calls_num-1) pipes and index starts from 0

                    for (int i= 0; i< calls_num; i++) wait(&status);
                }
            }
        }



        /*
        //closing pipes----> no need for that: dup2() makes newfd be the copy of oldfd, closing newfd first if necessary
        for (int i = 0; i < calls_num - 1; i++)
        {
            close(pipe_desc[i][0]);
            close(pipe_desc[i][1]);     //let's not bother that some of them may be already closed
        }
        */
        free_arr_3d(commands, calls_num, calls_sizes);
        printf(">");
        fgets(input, max_input_len, stdin);
    }


    printf("Closing.\n");
    return 0;
}