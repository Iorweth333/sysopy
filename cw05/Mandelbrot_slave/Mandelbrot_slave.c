#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "lib/complex.h"
#include "math.h"
#include <limits.h>

#define re_max 1.0
#define re_min (-2.0)
#define im_max 1.0
#define im_min (-1.0)


complex random_complex(complex max, complex min)
{
    complex res;

    double re_range = (max.re - min.re);
    double im_range = (max.im - min.im);
    double re_div = RAND_MAX / re_range;
    double im_div = RAND_MAX / im_range;

    res.re = min.re + (rand() / re_div);
    res.im = min.im + (rand() / im_div);
    return res;
    //found here: https://stackoverflow.com/questions/33058848/generate-a-random-double-between-1-and-1
    //second answer from the top
}

long iters (complex c, long k)
{
    long i=0;
    complex tmp;     //z_0 = 0
    tmp.re = 0;
    tmp.im=0;
                    //z_n+1 = f (z_n)
                    //f (z_n) = z^2 + c
    while (i<=k && mod(tmp) <= 2)
    {
        i++;
        tmp = add(square(tmp), c);
    }
    return i;
}

char *iters_to_str(complex c, long iters)
{
    char * buffer = malloc (100 * sizeof(char));
    int cx;

    cx = snprintf ( buffer, 100, "%lf %lf %ld", c.re, c.im, iters );      //terminating char is automatically appended

    if (cx>=0 && cx<100)      // check returned value

        return buffer;

    return NULL;
}

int main(int argc, char *argv[])
{
    if (argc < 4)
    {
        printf("Mandelbrot_slave.\n"
                       "Too few arguments.\nPlease provide path to fifo and number N and K.\n");
        exit(1);
    }

    char *fifo_path = argv[1];
    long n = strtol(argv[2], NULL, 10);
    long k = strtol(argv[3], NULL, 10);

    srand((unsigned int) time(NULL));

    complex complex_max;
    complex_max.re = re_max;
    complex_max.im = im_max;

    complex complex_min;
    complex_min.re = re_min;
    complex_min.im = im_min;

    //open fifo
    FILE * fifo = fopen (fifo_path, "w");   //TODO: check this line. mode a = append, Open file for output at the end of a file.
    if (fifo == NULL)
    {
        printf ("Problem while opening fifo.\n");
        perror(NULL);
        exit(1);
    }


    for (int i = 0; i < n; i++)
    {
        complex tmp = random_complex(complex_max, complex_min);
        long tmp_iters = iters (tmp,k);
        char * buffer = iters_to_str (tmp, tmp_iters);
        fprintf (fifo, buffer);           //this line used to cause problems- when there was fputs instead of fprintf, saved data could get corrupt
        free (buffer);          //iters_to_str has it's own malloc
    }

    fclose (fifo);
    return 0;
}