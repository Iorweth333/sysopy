#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<fcntl.h>
#include<signal.h>
#include<sys/sem.h>
#include<sys/shm.h>
#include<sys/ipc.h>
#include<sys/types.h>
#include<time.h>
#include <wait.h>
#include"fifo_q.h"

int chair_sems;
int shared_mem;
struct node *queue;
int *clients;
int q_len_shm;
int *q_len_ptr;
int q_len;

void cleaner(int signo)
{
    if (signo != -1) printf("Client with pid %d leaves\n", getpid());
    //process that forkes clients also uses this cleaner, but isn't a client
    shmdt(queue);
    wait(NULL);     //wait for all children to terminate
    exit(signo);
}

int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        printf("Not enough arguments. Please provide with number of clients to fork and how many times each of them must be shaved");
        exit(1);
    }
    unsigned int clients_num = (unsigned int) strtol(argv[1], NULL, 10);
    unsigned int shave_num = (unsigned int) strtol(argv[2], NULL, 10);
    key_t key = ftok(getenv("HOME"), 1);
    q_len_shm = shmget(key, sizeof(int), 0);
    q_len_ptr = (int *) shmat(q_len_shm, NULL, 0);
    q_len = q_len_ptr[0];
    shmdt(q_len_ptr);   /* Detach shared memory segment.  */
    clients = malloc(clients_num * sizeof(int));

    //getting access to semaphores and shared memory segment
    key = ftok(getenv("HOME"), MAX_CHAIRS);
    chair_sems = semget(key, q_len + 1, 0);
    //q_len + 1 cause there is barber's chair which is not included in q but is in shared memory segment
    shared_mem = shmget(key, q_len * sizeof(struct node), 0);
    queue = (struct node *) shmat(shared_mem, NULL, 0);
    signal(SIGINT, cleaner);    //it's always useful to have simple way to terminate program
    struct timespec tm_spec;
    struct sembuf sem_change;
    pid_t pid = 1;
    printf("Time format: \nseconds, microseconds: message\n");
    for (unsigned int j = 0; j < clients_num && pid != 0; j++)     //children won't continue this loop
    {
        pid = fork();

        if (pid == 0)
        {
            //child
            struct node client;
            client.pid = getpid();
            sem_change.sem_flg = 0; //no flags needed
            unsigned int shaving_counter = 0;
            int position;              //hair index?
            while (shaving_counter < shave_num)
            {
                clock_gettime(CLOCK_MONOTONIC, &tm_spec);
                printf("%d, %d:Client with %d enters the waiting room\n",
                       (int) tm_spec.tv_sec, (int) tm_spec.tv_nsec / 1000, getpid());
                position = -1;
                for (unsigned short i = 1; i <= MAX_CHAIRS; i++)
                {
                    if (semctl(chair_sems, i, GETVAL) == 1)     //checking if chair is free
                    {
                        sem_change.sem_num = i;
                        sem_change.sem_op = -1;
                        semop(chair_sems, &sem_change, 1);      //trying to sit
                        client.sem_num = i;
                        position = i;
                        add_to_q(client, queue, q_len);
                        clock_gettime(CLOCK_MONOTONIC, &tm_spec);
                        printf("%d, %d: Client with PID %d takes seat number %d and wakes barber\n",
                               (int) tm_spec.tv_sec, (int) tm_spec.tv_nsec / 1000, getpid(), i);
                        sem_change.sem_num = 0;                 //barber sits/sleeps on chair nr 0
                        sem_change.sem_op = 1;                  //incrementing, because barber with sem 0 is sleeping
                        semop(chair_sems, &sem_change, 1);      //waking barber

                        //meanwhile chair's semaphore is 0
                        sem_change.sem_num = i;
                        sem_change.sem_op = -1;
                        semop(chair_sems, &sem_change, 1);  //will hang here while shaving
                        //taking seat again because barber will free it AFTER he finishes shaving.

                        clock_gettime(CLOCK_MONOTONIC, &tm_spec);
                        printf("%d, %d: Client with PID %d got shaved for %d/%d times\n",
                               (int) tm_spec.tv_sec, (int) tm_spec.tv_nsec / 1000,
                               getpid(), ++shaving_counter, shave_num);

                        //int s = 0;
                        //for (int k=0; k<100000; k++) s++;    //active waiting

                        sem_change.sem_num = i;
                        sem_change.sem_op = 1;
                        semop(chair_sems, &sem_change, 1);      //freeing the chair

                        break;

                    }
                }
                if (position == -1)
                {
                    clock_gettime(CLOCK_MONOTONIC, &tm_spec);
                    printf("%d, %d: No free chairs, client with PID %d leaves waiting room\n",
                           (int) tm_spec.tv_sec, (int) tm_spec.tv_nsec / 1000, getpid());
                    exit(0);
                }
            }
            clock_gettime(CLOCK_MONOTONIC, &tm_spec);
            printf("%d, %d: Client with PID %d has been shaved %d/%d times and leaves pleased.\n",
                   (int) tm_spec.tv_sec, (int) tm_spec.tv_nsec / 1000, getpid(), shaving_counter, shave_num);

        }
    }

    //else wait(NULL);
    cleaner(-1);
    //no need to wait on children
    return 0;
}