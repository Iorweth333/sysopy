#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<fcntl.h>
#include<signal.h>
#include<sys/sem.h>    //semaphores
#include<sys/shm.h>    //shared memory
#include<sys/ipc.h>
#include<sys/types.h>
#include<time.h>
#include"fifo_q.h"

int chairs_sems;
int shared_mem_des;
struct node *queue;
unsigned int chairs_num;
int q_len_shm_des;
int *q_len;

union semun
{
    int val;    /* Value for SETVAL */
    struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
    unsigned short *array;  /* Array for GETALL, SETALL */
    struct seminfo *__buf;  /* Buffer for IPC_INFO
                                           (Linux-specific) */
};  //from documentation of semctl http://man7.org/linux/man-pages/man2/semctl.2.html

void cleaner()
{
    printf("Barber closes\n");
    semctl(chairs_sems, 0, IPC_RMID, 0);
    shmdt(queue);
    shmctl(shared_mem_des, IPC_RMID, NULL);
    shmdt(q_len);
    shmctl(q_len_shm_des, IPC_RMID, NULL);
}


void SIGhandler(int signo)
{
    cleaner();
    exit(signo);
}

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        printf("Not enough arguments. Please provide how many chairs there are.\n");
        exit(1);
    }
    chairs_num = (unsigned int) strtol(argv[1], NULL, 10);


    //chairs_num for clients + 1 for barber

    //creating semaphores
    key_t key = ftok(getenv("HOME"), MAX_CHAIRS);
    chairs_sems = semget(key, chairs_num + 1, IPC_CREAT | 0666);        //rw for all
    //creating shared memory segments:
    //for chairs
    shared_mem_des = shmget(key, chairs_num * sizeof(struct node), IPC_CREAT | 0666);
    //and for queue
    key = ftok(getenv("HOME"), 1);    //different key will be necessary
    q_len_shm_des = shmget(key, sizeof(int), IPC_CREAT | 0666); //shared mem for q length


    //addding shared memory segments to address space of the process
    queue = (struct node *) shmat(shared_mem_des, NULL, 0);
    q_len = (int *) shmat(q_len_shm_des, NULL, 0);
    //shmat returns addres where shared memory segment is put,
    //It's advisable to put NULL as second parameter, which means system chooses the address
    //so from now on q_len points the beginning of the queue
    q_len[0] = chairs_num;        //first field is it's length
    q_init(queue, chairs_num);      //sets pid and sem_num of every chair to -1

    signal(SIGINT, SIGhandler);     //barber works until receiving SIGINT

    //setting values of semaphores to 1 since every chair can be used by one person at the time
    union semun attr;            //union, cause sectl takes union
    attr.val = 1;
    for (unsigned int i = 1; i < chairs_num + 1; i++) semctl(chairs_sems, i, SETVAL, attr);
    //Set the value of semval to arg.val for the sem_num-th semaphore of the set

    //setting semaphore for barber (who starts working from... sleeping ;)
    attr.val = 0;
    semctl(chairs_sems, 0, SETVAL, attr);


    struct timespec tm_spec;
    struct sembuf sem_change;          //Structure used for argument to `semop' to describe operations.  */
    signal(SIGINT, SIGhandler);
    struct node client;
    printf("Time format: \nseconds, microseconds: message\n");
    while (1)
    {
        clock_gettime(CLOCK_MONOTONIC, &tm_spec);
        printf("%d, %d: Barber falls asleep\n", (int) tm_spec.tv_sec, (int) tm_spec.tv_nsec / 1000);
        //decrementing barber's semaphore which on the beginning is 0, so it's gonna hang up
        sem_change.sem_num = 0;
        sem_change.sem_op = -1;
        semop(chairs_sems, &sem_change, 1);
        clock_gettime(CLOCK_MONOTONIC, &tm_spec);
        printf("%d, %d: Barber is waken up\n", (int) tm_spec.tv_sec, (int) tm_spec.tv_nsec / 1000);
        //sleep(1); //sleep() is forbidden in this task!
        client = get_first(queue, chairs_num);
        clock_gettime(CLOCK_MONOTONIC, &tm_spec);
        printf("%d, %d: Barber shaves client with pid %d\n", (int) tm_spec.tv_sec, (int) tm_spec.tv_nsec / 1000, client.pid);
        //incrementing chair's semaphore to mark it as free
        sem_change.sem_num = (unsigned short) client.sem_num;
        sem_change.sem_op = 1;
        semop(chairs_sems, &sem_change, 1);
        clock_gettime(CLOCK_MONOTONIC, &tm_spec);
        printf("%d, %d: Barber finishes shaving client with pid %d\n", (int) tm_spec.tv_sec, (int)tm_spec.tv_nsec / 1000, client.pid);
    }

    //no return, the only way to stop it nonbrutally is SIGINT
}