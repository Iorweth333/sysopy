#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<fcntl.h>
#include<signal.h>
#include<semaphore.h>    //semaphores
#include <sys/mman.h>   //shared memory
#include<sys/stat.h>
#include<sys/types.h>
#include<time.h>
#include <errno.h>
#include"fifo_q.h"

//int chairs_sems;
int q_len_shm_des;
struct node *queue;
unsigned int chairs_num;
int q_len_shm;
int *q_len_ptr;
int q_shm_des;

sem_t *barber_semaphore;
sem_t *client_semaphore;

union semun
{
    int val;    /* Value for SETVAL */
    struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
    unsigned short *array;  /* Array for GETALL, SETALL */
    struct seminfo *__buf;  /* Buffer for IPC_INFO
                                           (Linux-specific) */
};  //from documentation of semctl http://man7.org/linux/man-pages/man2/semctl.2.html

void cleaner()
{
    printf("Barber closes\n");
    sem_close(client_semaphore);
    sem_close(barber_semaphore);
    sem_unlink("/semaphore");

    munmap(queue, chairs_num * sizeof(struct node));    //q_len_shm or q_len_ptr?
    munmap(q_len_ptr, 2 * sizeof(int));     //TODO: check the size

    close(q_shm_des);
    close(q_len_shm_des);

    shm_unlink("/shared_mem");
    shm_unlink("/qshm");
}


void SIGhandler(int signo)
{
    cleaner();
    exit(signo);
}

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        printf("Not enough arguments. Please provide how many chairs there are.\n");
        exit(1);
    }
    chairs_num = (unsigned int) strtol(argv[1], NULL, 10);

    q_len_shm_des = shm_open("/shared_mem", O_RDWR | O_CREAT, 0666);
    //segment created. specifying it's size
    ftruncate(q_len_shm_des, sizeof(unsigned int));        //TODO: check if size is ok, rather chairs_num!

    q_len_ptr = (int *) mmap(NULL, sizeof(unsigned int), PROT_READ | PROT_WRITE, MAP_SHARED, q_len_shm_des,
                             0);  //PROT_EXEC or not?

    q_len_ptr[0] = chairs_num;



    //chairs_num for clients + 1 for barber

    //creating semaphores
    barber_semaphore = sem_open("/semaphore", O_CREAT, 0666, 0);

    if (barber_semaphore == SEM_FAILED)
    {
        printf("Error while creating barber's semaphore. Terminating\n");
        perror(NULL);
        cleaner();
        exit(errno);
    }

    q_shm_des = shm_open("/q_shm", O_RDWR | O_CREAT, 0666);
    ftruncate(q_shm_des, chairs_num * sizeof(struct node));
    queue = (struct node *) mmap(NULL, chairs_num * sizeof(struct node), PROT_READ | PROT_WRITE, MAP_SHARED, q_shm_des,
                                 0);

    //i dont open or create clients' semaphores here, cause the will be opening in the infinite loop

    q_init(queue, chairs_num);

    signal(SIGINT, SIGhandler);


    struct timespec tm_spec;
    signal(SIGINT, SIGhandler);
    struct node client;
    printf("Time format: \nseconds, microseconds: message\n");
    while (1)
    {
        clock_gettime(CLOCK_MONOTONIC, &tm_spec);
        printf("%d, %d: Barber falls asleep\n", (int) tm_spec.tv_sec, (int) tm_spec.tv_nsec / 1000);
        //decrementing barber's semaphore which on the beginning is 0, so it's gonna hang up
        sem_wait(barber_semaphore);
        clock_gettime(CLOCK_MONOTONIC, &tm_spec);
        printf("%d, %d: Barber is waken up\n", (int) tm_spec.tv_sec, (int) tm_spec.tv_nsec / 1000);
        client = get_first(queue, chairs_num);
        clock_gettime(CLOCK_MONOTONIC, &tm_spec);
        printf("%d, %d: Barber shaves client with pid %d\n", (int) tm_spec.tv_sec, (int) tm_spec.tv_nsec / 1000,
               client.pid);
        //incrementing chair's semaphore to mark it as free
        client_semaphore = sem_open(client.sem_num, 0);
        sem_post(client_semaphore);
        sem_close(client_semaphore);
        clock_gettime(CLOCK_MONOTONIC, &tm_spec);
        printf("%d, %d: Barber finishes shaving client with pid %d\n", (int) tm_spec.tv_sec,
               (int) tm_spec.tv_nsec / 1000, client.pid);
    }

    //no return, the only way to stop it nonbrutally is SIGINT
}