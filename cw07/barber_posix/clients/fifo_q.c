// Created by karolb on 17.09.17.
#include "fifo_q.h"


void q_init(struct node *node, int size)
{
    for (int i = 0; i < size; i++) node[i].pid = -1;
}

int is_full(struct node *node, int size)
{
    if (node[size - 1].pid != -1) return 1;
    return 0;
}


void add_to_q(struct node new_node, struct node *q, int size)
{
    if (size == 1)
    {
        q[0].pid = new_node.pid;
        strcpy(q[0].sem_num, new_node.sem_num);
    }
    else
        for (int i = 0; i < size; i++)
        {
            if (q[i].pid == -1)
            {
                if (i == size) break;
                q[i].pid = new_node.pid;
                strcpy(q[i].sem_num, new_node.sem_num);
                break;
            }
        }
}

void delete_first(struct node *q, int size)
{
    if (size == 1) q[0].pid = -1;
    else
        for (int i = 0; i < size - 1; i++)
        {
            if (q[i].pid == -1) break;
            q[i].pid = q[i + 1].pid;
            strcpy(q[i].sem_num, q[i + 1].sem_num);
        }
}

struct node get_first(struct node *q, int size)
{
    if (q[0].pid == -1) return q[0];
    struct node res;
    res.pid = q[0].pid;
    strcpy(res.sem_num, q[0].sem_num);
    delete_first(q, size);
    return res;
}
