//
// Created by karolb on 17.09.17.
//
#ifndef BARBER_FIFO_Q_H
#define BARBER_FIFO_Q_H


#include <string.h>

static const int MAX_CHAIRS = 10;       //also sued as parameter for ftok()


struct node
{
    int pid;
    char sem_num[10];
};

int is_full(struct node* node, int size);

struct node get_first(struct node *q, int size);

void q_init(struct node *node, int size);

void add_to_q(struct node new_node, struct node *q, int size);

int is_full(struct node *node, int size);

int queue_size(struct node *node, int size);


#endif //BARBER_FIFO_Q_H
