#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<fcntl.h>
#include<signal.h>
#include<semaphore.h>    //semaphores
#include <sys/mman.h>   //shared memory
#include<sys/stat.h>
#include<sys/types.h>
#include<time.h>
#include <errno.h>
#include <wait.h>
#include"fifo_q.h"

int q_shm_des;
struct node *queue;
int q_len_shm_des;
int *q_len_ptr;
int q_len;

sem_t *barber_semaphore;
sem_t *client_semaphore;

void cleaner(int signo)
{
    if (signo != -1) printf("Client with pid %d leaves\n", getpid());
    //process that forkes clients also uses this cleaner, but isn't a client
    sem_close(barber_semaphore);
    sem_close(client_semaphore);
    munmap(queue, q_len * sizeof(struct node));
    close(q_shm_des);
    close(q_len_shm_des);
    wait(NULL);     //wait for all children to terminate
    exit(signo);
}

void generate_sem_name(char *name);

int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        printf("Not enough arguments. Please provide with number of clients to fork and how many times each of them must be shaved");
        exit(1);
    }
    unsigned int clients_num = (unsigned int) strtol(argv[1], NULL, 10);
    unsigned int shave_num = (unsigned int) strtol(argv[2], NULL, 10);

    q_len_shm_des = shm_open("/shared_mem", O_RDWR, 0666);
    q_len_ptr = (int *) mmap(NULL, sizeof(unsigned int), PROT_READ | PROT_WRITE, MAP_SHARED, q_len_shm_des,
                             0);  //PROT_EXEC or not?
    q_len = q_len_ptr[0];
    munmap(q_len_ptr, sizeof(unsigned int));   /* Detach shared memory segment.  */
    close(q_len_shm_des);

    q_shm_des = shm_open("/q_shm", O_RDWR, 0666);
    queue = (struct node *) mmap(NULL, q_len * sizeof(struct node), PROT_READ | PROT_WRITE, MAP_SHARED, q_shm_des, 0);


    signal(SIGINT, cleaner);    //it's always useful to have simple way to terminate program
    struct timespec tm_spec;
    pid_t pid = 1;
    printf("Time format: \nseconds, microseconds: message\n");
    for (unsigned int j = 0; j < clients_num && pid != 0; j++)     //children won't continue this loop
    {
        pid = fork();

        if (pid == 0)
        {
            //child
            char sem_name[10];
            generate_sem_name(sem_name);
            struct node client;
            client.pid = getpid();
            strcpy(client.sem_num, sem_name);

            client_semaphore = sem_open(sem_name, O_CREAT | O_EXCL, 0666, 0);     //creating semaphore with value 0
            if (client_semaphore == SEM_FAILED)
            {
                printf("Error while creating client's semaphore. Terminating\n");
                perror(NULL);
                cleaner(errno);
            }
            unsigned int shaving_counter = 0;
            while (shaving_counter < shave_num)
            {
                clock_gettime(CLOCK_MONOTONIC, &tm_spec);
                printf("%d, %d:Client with %d enters the waiting room\n",
                       (int) tm_spec.tv_sec, (int) tm_spec.tv_nsec / 1000, getpid());
                if (!is_full(queue, q_len))     //checking if any chair is free
                {
                    add_to_q(client, queue, q_len);
                    clock_gettime(CLOCK_MONOTONIC, &tm_spec);
                    printf("%d, %d: Client with PID %d takes seat and wakes barber\n",
                           (int) tm_spec.tv_sec, (int) tm_spec.tv_nsec / 1000, getpid());
                    barber_semaphore = sem_open("/semaphore", 0);
                    sem_post(barber_semaphore); //waking barber
                    sem_close(barber_semaphore);
                    sem_wait(client_semaphore); //waiting for barber to finish shaving

                    clock_gettime(CLOCK_MONOTONIC, &tm_spec);
                    printf("%d, %d: Client with PID %d got shaved for %d/%d times\n",
                           (int) tm_spec.tv_sec, (int) tm_spec.tv_nsec / 1000,
                           getpid(), ++shaving_counter, shave_num);
                }
                else
                {
                    clock_gettime(CLOCK_MONOTONIC, &tm_spec);
                    printf("%d, %d: No free chairs, client with PID %d leaves waiting room\n",
                           (int) tm_spec.tv_sec, (int) tm_spec.tv_nsec / 1000, getpid());
                    cleaner(0);
                }
            }
            clock_gettime(CLOCK_MONOTONIC, &tm_spec);
            printf("%d, %d: Client with PID %d has been shaved %d/%d times and leaves pleased.\n",
                   (int) tm_spec.tv_sec, (int) tm_spec.tv_nsec / 1000, getpid(), shaving_counter, shave_num);

        }
    }

    //else wait(NULL);
    cleaner(-1);
    //no need to wait on children
    return 0;
}


void generate_sem_name(char *name)
{
    strcpy(name, "/sem");

    char pid[6];
    sprintf(pid, "%d", getpid());

    strcpy(&name[4], pid);
}