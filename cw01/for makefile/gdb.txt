english translation should be below

W celu sprowokowania błedu zmieniłem następującą linię:
add_contact_T(&Tbook.root, ncontact_T, 2);
na taką:
add_contact_T(Tbook.root, ncontact_T, 2);   
Funkcja przyjmuje wskaźnik na wskaźnik a zostaje jej podany po prostu wskaźnik. I program się sypie.
Żeby zrobić core dump, należy wpierw skompilowac program z flagą -g. Ta część jest zalatwiona w ./for makefile/Makefile, wystarczy więc wpisać make test_broken2
make test_broken różni się tylko tym że sam od razu odpala program. Niestety wówczas u mnie zrzut się nie generuje mimo komunikatu jakoby miał się wygenerować. Internet podpowiada, że problemem może być konfiguracja pliku /proc/sys/kernel/core_pattern który powoduje przekierowanie zrzutu w inne miejsce. można to jednak obejść:
make test_broken2
gdb ./test_broken
(gdb) r
generate-core-file
q
teraz zrzut powinien się wygenerować w katalogu bieżącym.
Aby przeanalizować zrzut należy uruchomić program pod kontrolą gdb z tym, że trzeba podać plik zrzutu jako argument:
gdb ./test_broken core.27254
(nazwa pliku może się różnić!)
pomocne okazuje się polecenie bt, które obwieszcza co następuje:
(gdb) bt
#0  0x00007ffff79d6f91 in add_contact_T () from ./Tlib.so
#1  0x0000000000400e17 in main () at broken_main.c:104
dowiadujemy się że problam pojawił się w funkcji add_contact wywolanej w funkcji main w 104-tej linijce
możemy wpisać list broken_main.c:104 aby obejrzeć "okolicę" 104-tej linii.
To by było na tyle, jeśli chodzi o współpracę z gdb. W dalszej części walki z błędem na wagę złota mogą okazać się komunikaty z gcc (flagi -Wall i -Wextra są zawarte w makefile), które głoszą: "warning: passing argument 1 of ‘add_contact_T’ from incompatible pointer type [enabled by default]" i dalej: "note: expected ‘struct contact_T **’ but argument is of type ‘struct contact_T *’"
to powinno wystarczyć do zlokalizowania tego błędu



ENG:
In order to provoke error i changed such a line:
add_contact_T(&Tbook.root, ncontact_T, 2);
on such:
add_contact_T(Tbook.root, ncontact_T, 2);  

Function takes pointer to pointer, but get just a pointer. And all hell breaks loose.
In order to create core dump file, first you have to compile program with -g flag. This part is already done in ./for makefile/Makefile, so only thing you have to do is type make test_broken2
the only difference between make test_broken and make test_broken2 is that the first runs program right after compilation, but unfortunately it doesn't work for me properly- the core dump does not appear. Internet hints that the problem may be caused by configuration of /proc/sys/kernel/core_pattern file, which redirects core dumps to different place. Although, it's possible to override it:
to jednak obejść:
make test_broken2
gdb ./test_broken
(gdb) r
generate-core-file
q
now core dump should appear in current directory.
In order to analyze it run the program under gdb but with core file as argument:
gdb ./test_broken core.27254
(filename may differ!)
bt command may come in handy. It says:
(gdb) bt
#0  0x00007ffff79d6f91 in add_contact_T () from ./Tlib.so
#1  0x0000000000400e17 in main () at broken_main.c:104
so what we now know is that failure was caused in add_contact function which was called in 104th line of broken_main.c file. we can now type list broken_main.c:104 so that we could see code around 104th line
That's all in gdb. For further fight against error very useful are prompts from gcc (flags -Wall and -Wextra are included in Makefile). They say:
"warning: passing argument 1 of ‘add_contact_T’ from incompatible pointer type [enabled by default]"
and
"note: expected ‘struct contact_T **’ but argument is of type ‘struct contact_T *’"
this should be enought to find the bug



