//
// Created by Karol on 01.05.2017.
//
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h> //dla clock i CLOCK_PER_SEC
#include <sys/times.h>
#include "lib/Llib.h"
#include "lib/Tlib.h"


/*
    \a is alert/bell
    \b is backspace/rubout
    \n is newline
    \r is carriage return (return to left margin)
    \t is tab

*/

void display_ticks(struct tms start, struct tms end, clock_t realtime_start, clock_t realtime_end)
{
    /* tms:
    clock_t  tms_utime  user CPU time
    clock_t  tms_stime  system CPU time
     */
    clock_t real = realtime_end - realtime_start;
    clock_t user = end.tms_utime - start.tms_utime;
    clock_t system = end.tms_stime - start.tms_stime;

    if (real == 0 && user == 0 && system == 0)
        printf("Times equal 0 --> times are too short to be properly measured! \n \n");
    else
    {
        printf("Real, \tuser, \tsystem: \n");
        printf("%ld, \t%ld, \t%ld,", real, user, system);
        printf("\n \n");
    }
}


contact_T* find_pessimistic_contact_T(const contact_T *root)
{
    if (root == 0) return 0;

    contact_T *tmp;
    if (root->right != 0) tmp = root->right;
    else
    {
        if (root->left != 0) tmp = root->left;
        else return 0;
    }

    while (tmp->right != 0 || tmp->left != 0)
    {
        if (tmp->right != 0) tmp = tmp->right;
        else if (tmp->left != 0) tmp = tmp->left;
    }
    return tmp;
}

int main ()
{
    struct tms start, end;

    clock_t realtime_start, realtime_end;

//-----------------------------------------------------------------------

    printf("CREATION OF ADDRESS BOOK:\n");

//clock_t clock ( void );               returns the number of clock ticks elapsed since an epoch related to the particular program execution.
//clock_t times(struct tms *buffer);    returns the elapsed real time, in clock ticks

    realtime_start = times(&start);

    address_book_T Tbook = new_book_T();

    realtime_end = times(&end);

    printf("Tree's version time: \n");
    display_ticks(start, end, realtime_start, realtime_end);


    realtime_start = times(&start);

    address_book_L Lbook = new_book_L();

    realtime_end = times(&end);

    printf("List's version time: \n");
    display_ticks(start, end, realtime_start, realtime_end);

//-----------------------------------------------------------------------
    printf("-------------------------------------------------------------\n");

    printf("ADDING A SINGLE ELEMENT: \n");

    realtime_start = times(&start);

    contact_T *ncontact_T = new_contact_T("Karol", "Bielen", 9, 1, 1996, "karolbielen@gmail.com", "530 424 539",
                                          "Lea 236/13");
    add_contact_T(&Tbook.root, ncontact_T, 2);

    realtime_end = times(&end);

    printf("Tree's version time: \n");
    display_ticks(start, end, realtime_start, realtime_end);


    realtime_start = times(&start);
    contact_L *ncontact_L = new_contact_L("Karol", "Bielen", 9, 1, 1996, "karolbielen@gmail.com", "530 424 539",
                                          "Lea 236/13");
    add_contact_L(&Lbook, ncontact_L);

    realtime_end = times(&end);

    printf("List's version time: \n");
    display_ticks(start, end, realtime_start, realtime_end);


//-----------------------------------------------------------------------
    printf("-------------------------------------------------------------\n");

    printf("ADDING MANY ELEMENTS: \n");

    char *names[] = {"Jan", "Antoni", "Franciszek", "Jakub", "Szymon", "Wojciech", "Aleksander", "Piotr", "Adam",
                     "Kacper", "Mikolaj", "Michal", "Mateusz", "Leon", "Filip", "Marcel", "Stanislaw", "Wiktor",
                     "Milosz", "Tomasz", "Karol", "Maksymilian", "Tymon", "Bartek", "Maciej", "Ignacy", "Krzysztof",
                     "Gabriel", "Igor", "Julian", "Lukasz", "Patryk", "Tadeusz", "Dawid", "Kajetan", "Mieszko",
                     "Ryszard", "Borys", "Dominik", "Gustaw", "Jerzy", "Ksawery", "Oliwier", "Oskar", "Tymoteusz",
                     "Alan", "Artur", "Fabian", "Henryk", "Krystian", "Leonard", "Nikodem", "Olaf", "Pawel",
                     "Sebastian", "Stefan", "Teodor", "Bruno", "Cezary", "Daniel", "Grzegorz", "Hubert", "Jeremi",
                     "Kamil", "Kazimierz", "Maks", "Marek", "Robert", "Witold", "Adrian", "Arkadiusz", "Boleslaw",
                     "Feliks", "Hugo", "Jacek", "Juliusz", "Konstanty", "Leopold", "Ludwik", "Marcin", "Milan",
                     "Radoslaw", "Seweryn", "Alex", "Anatol", "Andrzej", "Beniamin", "Bernard", "Blazej", "Bogdan",
                     "Czeslaw", "Dorian", "Emil", "Ernest", "Gaspar", "Gniewomir", "Ireneusz", "Janusz", "Jeremiasz",
                     "Jedrzej", "Jozef", "Konrad", "Kornel", "Korneliusz", "Ksawier", "Lucjan", "Marceli", "Mariusz",
                     "Natan", "Olgierd", "Radzimir", "Rafal", "Remigiusz", "Tobiasz", "Wincenty", "Wladyslaw",
                     "Zofia", "Maja", "Alicja", "Julia", "Zuzanna", "Amelia",
                     "Hanna", "Oliwia", "Aleksandra", "Maria", "Antonina", "Liliana", "Lena", "Natalia", "Pola",
                     "Gabriela", "Laura", "Anna", "Helena", "Joanna", "Marcelina", "Michalina", "Lucja", "Nina",
                     "Wiktoria", "Aniela", "Iga", "Karolina", "Magdalena", "Kornelia", "Marta", "Blanka", "Emilia",
                     "Martyna", "Weronika", "Agata", "Aurelia", "Nikola", "Paulina", "Sara", "Bianka", "Gaja", "Kalina",
                     "Klara", "Nadia", "Roza", "Ewa", "Kaja", "Karina", "Katarzyna", "Lidia", "Marianna", "Matylda",
                     "Nela", "Rozalia", "Adela", "Barbara", "Danuta", "Dominika", "Dorota", "Eliza", "Jagoda", "Janina",
                     "Kamila", "Kinga", "Klaudia", "Liwia", "Malgorzata", "Melania", "Milena", "Monika", "Patrycja",
                     "Wanda", "Adrianna", "Agnieszka", "Antonia", "Apolonia", "Aria", "Celina", "Dagmara", "Daria",
                     "Dobrawa", "Elzbieta", "Emma", "Ewelina", "Gloria", "Izabela", "Jadwiga", "Jagna", "Jasmina",
                     "Judyta", "Konstancja", "Krystyna", "Lilia", "Mia", "Nel", "Nelly", "Rebeka", "Rita", "Sandra",
                     "Tola", "Urszula"
    };
    //215names
    const int names_number = 215;

    char *surnames[] = {"Nowak", "Kowalski", "Wisniewski", "Dabrowski", "Lewandowski", "Wojcik", "Kaminski",
                        "Kowalczyk", "Zielinski", "Szymanski", "Wozniak", "Kozlowski", "Jankowski", "Wojciechowski",
                        "Kwiatkowski", "Kaczmarek", "Mazur", "Krawczyk", "Piotrowski", "Grabowski", "Nowakowski",
                        "Pawlowski", "Michalski", "Nowicki", "Adamczyk", "Dudek", "Zajac", "Wieczorek", "Jablonski",
                        "Krol", "Majewski", "Olszewski", "Jaworski", "Wrobel", "Malinowski", "Pawlak", "Witkowski",
                        "Walczak", "Stępien", "Gorski", "Rutkowski", "Michalak", "Sikora", "Ostrowski", "Baran", "Duda",
                        "Szewczyk", "Tomaszewski", "Pietrzak", "Marciniak", "Wroblewski", "Zalewski", "Jakubowski",
                        "Jasinski", "Zawadzki", "Sadowski", "Bak", "Chmielewski", "Wlodarczyk", "Borkowski",
                        "Czarnecki", "Sawicki", "Sokolowski", "Urbanski", "Kubiak", "Maciejewski", "Szczepanski",
                        "Kucharski", "Wilk", "Kalinowski", "Lis", "Mazurek", "Wysocki", "Adamski", "Kazmierczak",
                        "Wasilewski", "Sobczak", "Czerwinski", "Andrzejewski", "Cieslak", "Glowacki", "Zakrzewski",
                        "Kolodziej", "Sikorski", "Krajewski", "Gajewski", "Szymczak", "Szulc", "Baranowski",
                        "Laskowski", "Brzezinski", "Makowski", "Ziolkowski", "Przybylski", "Domanski", "Nowacki",
                        "Borowski", "Blaszczyk", "Chojnacki", "Ciesielski", "Mroz", "Szczepaniak", "Wesolowski",
                        "Gorecki", "Krupa", "Kaczmarczyk", "Leszczynski", "Lipinski", "Kowalewski", "Urbaniak", "Kozak",
                        "Kania", "Mikolajczyk", "Czajkowski", "Mucha", "Tomczak", "Koziol", "Markowski", "Kowalik",
                        "Nawrocki", "Brzozowski", "Janik", "Musial", "Wawrzyniak", "Markiewicz", "Orlowski", "Tomczyk",
                        "Jarosz", "Kolodziejczyk", "Kurek", "Kopec", "Zak", "Wolski", "Luczak", "Dziedzic", "Kot",
                        "Stasiak", "Stankiewicz", "Piatek", "Jozwiak", "Urban", "Dobrowolski", "Pawlik", "Kruk",
                        "Domagala", "Piasecki", "Wierzbicki", "Karpinski", "Jastrzebski", "Polak", "Zieba", "Janicki",
                        "Wojtowicz", "Stefanski", "Sosnowski", "Bednarek", "Majchrzak", "Bielecki", "Malecki", "Maj",
                        "Sowa", "Milewski", "Gajda", "Klimek", "Olejniczak", "Ratajczak", "Romanowski", "Matuszewski",
                        "Sliwinski", "Madej", "Kasprzak", "Wilczynski", "Grzelak", "Socha", "Czajka", "Marek", "Kowal",
                        "Bednarczyk", "Skiba", "Wrona", "Owczarek", "Marcinkowski", "Matusiak", "Orzechowski",
                        "Sobolewski", "Kedzierski", "Kurowski", "Rogowski", "Olejnik", "Debski", "Baranski",
                        "Skowronski", "Mazurkiewicz", "Pajak", "Czech", "Janiszewski", "Bednarski", "Lukasik",
                        "Chrzanowski", "Bukowski", "Lesniak", "Cieslik", "Kosinski", "Jedrzejewski", "Muszynski",
                        "Swiatek", "Koziel", "Osinski", "Czaja", "Lisowski", "Kuczynski", "Zukowski", "Grzybowski",
                        "Kwiecien", "Pluta", "Morawski"
    };
    //216 surnames
    const int surnames_number = 216;


    int i;
    int quantity = 55000;
    int counterN = 0, counterS = 0;         //counters for tabs: names and surnames
    int bday = 1, bmonth = 1, byear = 1955; //birth day, birth month, birth year

    realtime_start = times(&start);

    for (i = 0; i < quantity; i = i + 1)
    {
        ncontact_T = new_contact_T(names[counterN], surnames[counterS], bday, bmonth, byear,
                                              "examplaryemail.gmail.com", "123456789", "Examplary address");
        counterN++;
        if (counterN == names_number) counterN = 0;
        counterS++;
        if (counterS == surnames_number) counterS = 0;

        if (!add_contact_T(&Tbook.root, ncontact_T, 2))
        {
            printf("adding failed! \n");
            break;
        }
    }

    realtime_end = times(&end);

    printf("Tree's version time: \n");
    display_ticks(start, end, realtime_start, realtime_end);


    realtime_start = times(&start);

    for (i = 0; i < quantity; i = i + 1)
    {
        ncontact_L = new_contact_L(names[counterN], surnames[counterS], bday, bmonth, byear,
                                            "examplaryemail.gmail.com", "123456789", "Examplary address");
        counterN++;
        if (counterN == names_number) counterN = 0;
        counterS++;
        if (counterS == surnames_number) counterS = 0;

        if (!add_contact_L(&Lbook, ncontact_L))
        {
            printf("adding failed \n");
            break;
        }

    }

    realtime_end = times(&end);

    printf("List's version time: \n");
    display_ticks(start, end, realtime_start, realtime_end);

    printf("added %i elements\n\n", quantity);

    //-----------------------------------------------------------------------
    printf("-------------------------------------------------------------\n");

    printf("DELETING ONE ELEMENT (optimistic): \n");

    //for average case:
    //Mateusz- 13th name
    //Kozlowski- 12th surname
    //so Mateusz Kozlowski was added as 228th person

    realtime_start = times(&start);

    if (!del_contact_T(&Tbook.root, "Karol", "Bielen", 2)) printf ("Deletion from tree's version failed!\n");

    realtime_end = times(&end);

    printf("Tree's version time: \n");
    display_ticks(start, end, realtime_start, realtime_end);


    realtime_start = times(&start);

    if (!del_contact_L(&Lbook, "Karol", "Bielen")) printf ("Deletion from list's version failed!\n");

    realtime_end = times(&end);

    printf("List's version time: \n");
    display_ticks(start, end, realtime_start, realtime_end);


    //-----------------------------------------------------------------------
    printf("-------------------------------------------------------------\n");

    printf("DELETING ONE ELEMENT (pessimistic): \n");

    contact_T *pessimistic_contact = find_pessimistic_contact_T(Tbook.root);

    realtime_start = times(&start);

    if (!del_contact_T(&Tbook.root, pessimistic_contact->fname, pessimistic_contact->lname, 2)) printf ("Deletion from tree's version failed!\n");

    realtime_end = times(&end);

    printf("Tree's version time: \n");
    display_ticks(start, end, realtime_start, realtime_end);


    realtime_start = times(&start);

    if (!del_contact_L(&Lbook, Lbook.last->fname, Lbook.last->lname)) printf ("Deletion from list's version failed!\n");

    realtime_end = times(&end);

    printf("List's version time: \n");
    display_ticks(start, end, realtime_start, realtime_end);


    //-----------------------------------------------------------------------
    printf("-------------------------------------------------------------\n");

    printf("SEARCHING ELEMENT (optimistic): \n");

    realtime_start = times(&start);

    contact_T* seeked_T = find_contact_T (Tbook.root, Tbook.root->fname, Tbook.root->lname, 2);
    if (seeked_T == 0) printf ("Searching in tree's version failed!\n");

    realtime_end = times(&end);

    printf("Tree's version time: \n");
    display_ticks(start, end, realtime_start, realtime_end);


    realtime_start = times(&start);

    contact_L* seeked_L = find_contact_L(Lbook, Lbook.first->fname, Lbook.first->lname);
    if (seeked_L == 0) printf ("Searching in list's version failed!\n");

    realtime_end = times(&end);

    printf("List's version time: \n");
    display_ticks(start, end, realtime_start, realtime_end);


    //-----------------------------------------------------------------------
    printf("-------------------------------------------------------------\n");

    printf("SEARCHING ELEMENT (pessimistic): \n");

    pessimistic_contact = find_pessimistic_contact_T(Tbook.root);

    realtime_start = times(&start);

    seeked_T = find_contact_T (Tbook.root, pessimistic_contact->fname, pessimistic_contact->lname, 2);
    if (seeked_T == 0) printf ("Searching in tree's version failed!\n");

    realtime_end = times(&end);

    printf("Tree's version time: \n");
    display_ticks(start, end, realtime_start, realtime_end);


    realtime_start = times(&start);

    seeked_L = find_contact_L(Lbook, Lbook.last->fname, Lbook.last->lname);
    if (seeked_L == 0) printf ("Searching in list's version failed!\n");

    realtime_end = times(&end);

    printf("List's version time: \n");
    display_ticks(start, end, realtime_start, realtime_end);


    //-----------------------------------------------------------------------
    printf("-------------------------------------------------------------\n");

    printf("REBUILDING BOOKS: \n");

    realtime_start = times(&start);

    sort_book_by_first_name_T(&Tbook);

    realtime_end = times(&end);

    printf("Tree's version time: \n");
    display_ticks(start, end, realtime_start, realtime_end);


    realtime_start = times(&start);

    sort_book_by_first_name_L(&Lbook);

    realtime_end = times(&end);

    printf("List's version time: \n");
    display_ticks(start, end, realtime_start, realtime_end);

    printf("Finished\n");

    //char dummy;
    //scanf("%c", &dummy);

    return 0;
}



