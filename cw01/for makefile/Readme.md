This folder is created to separate .c and .h files from CLion in order to use 'make' more easily.

Type 'make X' to create and run certain version of the program.
X can be:
test_stat
test_shared
test_shared_dyn
test_broken (to train core dump creation and analysis)
and ca. dozen of others (see makefile)


