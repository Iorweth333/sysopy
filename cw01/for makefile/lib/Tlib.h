#include <stdbool.h>

typedef struct
{
    int day, month, year;
}date_T;

typedef struct contact_T contact_T;

typedef struct address_book_T address_book_T;

struct contact_T
{
    char *fname;    //first name
    char *lname;    //last name
    date_T bdate;     //birth date_T
    char *email;
    char *tel;      //telephone
    char *address;

    struct contact_T *left, *right, *parent;

};

struct address_book_T
{
    contact_T * root;
};

typedef int ( * comparing_fun_T) (contact_T *, contact_T*);   //pointer to a comparing function

address_book_T  new_book_T(void);

void cut_tree_T(contact_T **node);      // deletes tree, which can be a whole tree

void del_book_T(address_book_T *book);

contact_T * new_contact_T(char *firstname, char *lastname, int birth_day, int birth_month, int birth_year, char *email,
                          char *telephone_number, char *address);

bool add_contact_T(contact_T **root, contact_T *ncontact, int parameter);

bool del_contact_T(contact_T **root, char *first_name, char *last_name, int parameter);             //THIS FUNCTION IS UNTRUSTED! It may spoil the tree (desort it, to be preciseful)

contact_T * detatch_contact_T(contact_T **root, char *first_name, char *last_name, int parameter);

contact_T * find_contact_T(contact_T *root, char *first_name, char *last_name, int parameter);    //will return first matching occurence but as a pointer

int cmp_by_fname_T(contact_T *node1, contact_T *node2);

int cmp_by_lname_T(contact_T *node1, contact_T *node2);

int cmp_by_birthdate_T(contact_T *node1, contact_T *node2);      //0 if equal, -1 if node1 is younger, +1 if older

void rebuild_book_T(contact_T **new_root, contact_T *node, int parameter);

void sort_book_by_first_name_T(address_book_T *book);

void sort_book_by_last_name_T(address_book_T *book);

void sort_book_by_birth_date_T(address_book_T *book);

void display_book_T(const contact_T *root);

void display_book_from_end_T(const contact_T *root);

int count_contacts (const contact_T *root, char *first_name, char *last_name);
