#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "Llib.h"

int main()
{
    address_book_L book = new_book_L();

    contact_L * nkontakt = new_contact_L("Karol", "Bielen", 9, 1, 1996, "karolbielen@gmail.com", "530 424 539",
                                         "Lea 236/13");

    add_contact_L(&book, nkontakt);

    char * names [] = {"Zuzanna", "Kornelia", "Jakub", "Oliwier", "Lena", "Magdalena", "Antoni",
                       "Julia", "Karolina", "Szymon", "Oskar", "Maja", "Michalina", "Jan", "Maciej", "Zofia",
                       "Weronika", "Filip", "Tomasz", "Hanna", "Marcelina", "Kacper", "Natan", "Amelia", "Agata",
                       "Aleksander", "Dominik", "Aleksandra", "Jagoda", "Franciszek", "Krzysztof"};
    //31 names
    char * surnames [] ={"Nowak", "Kowalski", "Wisniewski", "Dabrowski", "Lewandowski", "Wojcik",
                         "Kaminski", "Kowalczyk", "Zielinski", "Szymanski", "Wozniak", "Kozlowski",
                         "Jankowski", "Wojciechowski", "Kwiatkowski", "Kaczmarek", "Mazur", "Krawczyk",
                         "Piotrowski", "Grabowski", "Nowakowski", "Pawlowski", "Michalski", "Nowicki",
                         "Adamczyk", "Dudek", "Zajac", "Wieczorek", "Jablonski", "Krol", "Majewski",
                         "Olszewski", "Jaworski", "Wrobel", "Malinowski"};
    //35 surnames
    //char * gmail = "gmail.com";

    int counterN = 0, counterS = 0;         //counters for tabs: names and surnames
    int bday = 1, bmonth = 1, byear = 1950; //birth day, birth month, birth year

    int i;
    for (i=0; i<1000; i= i+1)
    {
        contact_L * ncontact = new_contact_L(names[counterN], surnames[counterS], bday, bmonth, byear,
                                             "examplaryemail.gmail.com", "123456789", "Examplary address");

        counterN++;
        if (counterN >= 31) counterN = 0;
        counterS++;
        if (counterS >= 35) counterS = 0;

        bday++;

        if (bday == 29)
        {
            bday = 1;
            bmonth++;
        }

        if (bmonth == 13)
        {
            bmonth = 1;
            byear++;
        }


        if ( !add_contact_L(&book, ncontact))
        {
            printf ("adding failed \n");
            break;
        }
    }

    contact_L * iter = book.first;

    i =0;

    while (iter)
    {
        printf ("%s", iter->lname);
        printf (" ");
        printf ("%s", iter->fname);
        printf ("\n");

        i++;
        iter = iter->next;
    }

    printf ("%d", i);

    printf (" \n \n Sorting attempt: \n \n");

    sort_book_by_first_name_L(&book);

    display_book_L(book);

    printf (" \n \n From behind: \n \n");

    display_book_from_end_L(book);

    printf (" \n \n Searching test: \n \n");

    contact_L * seeked = find_contact_L(book, "Karol", "Bielen");

    printf ("%s", seeked->fname);
    printf (" ");
    printf ("%s", seeked->lname);

    printf ("\nnext: \n");

    printf ("%s", seeked->next->fname);    //will cause null pointer exception if seeked was last!
    printf (" ");
    printf ("%s", seeked->next->lname);


    if (del_contact_L(&book, "Karol", "Bielen")) printf ("\ndel_contact returned true \n \n");

    printf ("\ndeleting book: \n");

    if (del_book_L(&book)) printf ("\n del_book_L returned true: \n ");

    if (book.first == 0 && book.last == 0) printf ("  first and last are nulls- book has been deleted");
    else printf ("  first or last isn't null");

    return 0;
}

