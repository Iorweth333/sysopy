#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "Tlib.h"


int main()
{
    printf ("Some basic debug test for tree version... \n");

    address_book_T book = new_book_T();

    contact_T * nkontakt = new_contact_T("Karol", "Bielen", 9, 1, 1996, "karolbielen@gmail.com", "530 424 539",
                                         "Lea 236/13");

    if ( !add_contact_T(&book.root, nkontakt, 2)) printf ("failed to add contact_T");

    if (book.root != 0) printf ("added Karol Bielen \n");

    char * names [] = {"Zuzanna", "Kornelia", "Jakub", "Oliwier", "Lena", "Magdalena", "Antoni",
                       "Julia", "Karolina", "Szymon", "Oskar", "Maja", "Michalina", "Jan", "Maciej", "Zofia",
                       "Weronika", "Filip", "Tomasz", "Hanna", "Marcelina", "Kacper", "Natan", "Amelia", "Agata",
                       "Aleksander", "Dominik", "Aleksandra", "Jagoda", "Franciszek", "Krzysztof"
    };
    //31 names
    char * surnames [] = {"Nowak", "Kowalski", "Wisniewski", "Dabrowski", "Lewandowski", "Wojcik",
                          "Kaminski", "Kowalczyk", "Zielinski", "Szymanski", "Wozniak", "Kozlowski",
                          "Jankowski", "Wojciechowski", "Kwiatkowski", "Kaczmarek", "Mazur", "Krawczyk",
                          "Piotrowski", "Grabowski", "Nowakowski", "Pawlowski", "Michalski", "Nowicki",
                          "Adamczyk", "Dudek", "Zajac", "Wieczorek", "Jablonski", "Krol", "Majewski",
                          "Olszewski", "Jaworski", "Wrobel", "Malinowski"
    };
    //35 surnames
    //char * gmail = "gmail.com";

    int counterN = 0, counterS = 0;         //counters for tabs: names and surnames
    int bday = 1, bmonth = 1, byear = 1950; //birth day, birth month, birth year



    printf ("starting to add contacts... \n");

    int i;
    for (i=0; i<1000; i= i+1)
    {
        contact_T * ncontact = new_contact_T(names[counterN], surnames[counterS], bday, bmonth, byear,
                                             "examplaryemail.gmail.com", "123456789", "Examplary address");

        counterN++;
        if (counterN >= 31) counterN = 0;
        counterS++;
        if (counterS >= 35) counterS = 0;

        bday++;

        if (bday == 29)
        {
            bday = 1;
            bmonth++;
        }

        if (bmonth == 13)
        {
            bmonth = 1;
            byear++;
        }

        printf ("Adding : %s %s \n", ncontact->lname, ncontact->fname);

        if ( !add_contact_T(&book.root, ncontact, 2))
        {
            printf ("adding failed! \n");
            break;
        }
    }

    printf ("\n\nfinished adding contacts... \n\n");

    display_book_T(book.root);


    printf ("\n\ndeleting contact_T Karol Bielen \n");
    if (del_contact_T(&book.root, "Karol", "Bielen", 2)) printf ("\ndel_contact returned true \n");





    printf (" \n \nSearching test: \n \n");

    contact_T * seeked = 0;
    seeked = find_contact_T(book.root, "Karol", "Bielen", 2);
    if (seeked != 0)
    {
        printf ("Found ");
        printf ("%s", seeked->fname);
        printf (" %s \n", seeked->lname);
    }
    else printf ("Not found\n\n");

    printf (" \n \n re-sorting attempt: \n \n");

    sort_book_by_first_name_T(&book);

    display_book_T(book.root);

    printf (" \n \n From behind: \n \n");

    display_book_from_end_T(book.root);

    printf (" \n \n Second searching test: \n \n");

    contact_T * seeked2 = 0;
    seeked2 = find_contact_T(book.root, "Karol", "Bielen", 1);
    if (seeked2 != 0)
    {
        printf ("Found:");
        printf ("%s", seeked2->fname);
        printf (" ");
        printf ("%s", seeked2->lname);
    }
    else printf ("Not found \n");

    printf (" \n \n Deleting test: \n");

    if (del_contact_T(&book.root, "Karol", "Bielen", 1)) printf ("\ndel_contact returned true \n");     //chyba bedzie trzeba przerobic na **
    else printf ("\ndel_contact returned false\n");

    seeked = find_contact_T(book.root, "Karol", "Bielen", 1);
    if (seeked == 0) printf ("contact_T hasn't been found- succes \n");
    else printf ("Some contact has been found- failure! \n");

    printf (" \n \n Another deleting test: \n");

    if (del_contact_T(&book.root, "Antoni", "Krol", 1)) printf ("\ndel_contact returned true \n");     //chyba bedzie trzeba przerobic na **
    else printf ("\ndel_contact returned false\n");

    seeked = find_contact_T(book.root, "Antoni", "Krol", 1);
    if (seeked == 0) printf ("contact_T hasn't been found- succes \n");
    else printf ("Some contact has been found- failure! \n");

    printf ("\ndeleting book: \n \n");

    del_book_T(&book);

    if (book.root == 0) printf ("\nbook has been deleted \n \n");
    else display_book_T(book.root);


    //char dummy;
    //scanf("%c",dummy);
    return 0;
}

