//
// Created by Karol on 01.05.2017.
//
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "lib/Ltest.h"
#include "lib/Ttest.h"
#include "time_comparison.h"

int main (int argc, char  * argv [])
{
    if (argc != 4)
    {
        printf ("Nieprawidlowa ilosc argumentow: trzeba czterech. \nKazdy to litera y lub inna zaleznie od tego jakie testy trzeba przeprowadzic.");
        return 1;
    }
    if (strcmp(argv[1], "y") == 0) Ltest();
    if (strcmp(argv[2], "y") == 0) Ttest();
    if (strcmp(argv[3], "y") == 0) time_comp();

    printf("Finished\n");

    //char dummy;
    //scanf("%c", &dummy);

    return 0;
}


