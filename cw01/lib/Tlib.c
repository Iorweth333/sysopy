#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include "Tlib.h"


address_book_T new_book_T(void)
{
    address_book_T nbook;

    nbook.root = 0;

    return nbook;
}

void cut_tree_T(contact_T **node)      // deletes branch or even a whole tree
{
    if ((*node)->left != 0) cut_tree_T(&(*node)->left);
    if ((*node)->right != 0) cut_tree_T(&(*node)->right);

    free((*node)->fname);
    free((*node)->lname);
    free((*node)->address);
    free((*node)->email);
    free((*node)->tel);
    free((*node));
    printf("deleted one node \n");
    (*node) = 0;
}

void del_book_T(address_book_T *book)
{
    if (book->root != 0) cut_tree_T(&book->root);
}

bool add_contact_T(contact_T **root, contact_T *ncontact,
                   int parameter)    //parameter: 1 to compare by first name, 2 - last name, 3 - birth date_T.
{                                                        // default parameter is lastname
    comparing_fun_T cmp = 0;                  //pointer to a comparing function
    if (parameter == 1) cmp = cmp_by_fname_T;
    if (parameter == 2) cmp = cmp_by_lname_T;
    if (parameter == 3) cmp = cmp_by_birthdate_T;

    if (cmp == 0)
    {
        printf("Something isn't right. Program doesn't know which field of structure to use to sort the book");
        return false;
    }

    ncontact->left = 0;         //will be useful while rebuilding the tree
    ncontact->right = 0;

    if (*root == 0)
    {
        *root = ncontact;
        ncontact->parent = 0;

        return true;
    }
    else
    {
        contact_T *parent = *root;

        if (cmp(parent, ncontact) < 0)     //adding to right
        {
            if (parent->right != 0) return add_contact_T(&(*root)->right, ncontact, parameter);
            else
            {
                parent->right = ncontact;
                ncontact->parent = parent;
                return true;
            }
        }
        else                                //adding to left
        {
            if (parent->left != 0) return add_contact_T(&(*root)->left, ncontact, parameter);
            else
            {
                parent->left = ncontact;
                ncontact->parent = parent;
                return true;
            }
        }
    }

}


bool del_contact_T(contact_T **root, char *first_name, char *last_name, int parameter)
{
    contact_T *tmp = detatch_contact_T(root, first_name, last_name, parameter);
    //previously:
    //contact_T * tmp = detatch_contact_T(&(*root), first_name, last_name, parameter);

    if (tmp == 0) return false;         //there was no such contact_T in the book

    free(tmp->fname);      //freeing space
    free(tmp->lname);
    free(tmp->address);
    free(tmp->email);
    free(tmp->tel);
    free(tmp);

    tmp->fname = 0;
    tmp->lname = 0;
    tmp->address = 0;
    tmp->email = 0;
    tmp->tel = 0;

    tmp = 0;
    return true;
}

contact_T *inorder_successor_T(const contact_T *root)
{
    if (root == 0) return 0;

    contact_T *tmp;

    if (root->right != 0)        //right tree exists
    {
        tmp = root->right;
        while (tmp->left != 0) tmp = tmp->left;
        return tmp;
    }
    else
    {
        if (root->parent != 0) return root->parent;   //returning parent
        else return root->right;                         //returning right son- or 0 if root is the only one node in tree
    }
}

contact_T *detatch_contact_T(contact_T **root, char *first_name, char *last_name, int parameter)
{
    contact_T *tmp = find_contact_T(*root, first_name, last_name, parameter);
    if (tmp == 0) return 0;

    contact_T *parent = tmp->parent;
    contact_T *z;

    if (tmp->right == 0 || tmp->left ==
                           0)                 //this is gonna be freakin' tough algorithm. found here: http://eduinf.waw.pl/inf/utils/002_roz/mp001.php
    {
        if (tmp->left != 0) z = tmp->left;
        else z = tmp->right;

        if (z != 0) z->parent = parent;

        if (parent != 0)
        {
            if (parent->left == tmp) parent->left = z;
            else parent->right = z;
            return tmp;
        }
        else
        {
            *root = z;
            return tmp;
        }
    }
    else
    {
        contact_T *succ = inorder_successor_T(tmp);     //both subtrees exist, so parent won't ever be returned

        if (tmp->right != succ)
        {
            if (succ->right != 0)     //if successor has got right son and is not son of detatched node
            {
                succ->right->parent = succ->parent;
                succ->parent->left = succ->right;
            }
            else succ->parent->left = 0;
        }


        succ->parent = tmp->parent;
        succ->left = tmp->left;
        if (tmp->right != succ) succ->right = tmp->right;

        if (parent == 0) (*root) = succ;
        return tmp;
        /*
        z = detatch_contact_T(root, tmp->right->fname, tmp->right->lname, parameter);
        z->left = tmp->left;
        z->right = tmp->right;                      //CHECK THIS HERE

        if (z->left != 0) z->left->parent = z;
        if (z->right != 0) z->right->parent = z;
         */
    }
}


contact_T *new_contact_T(char *firstname, char *lastname, int birth_day, int birth_month, int birth_year, char *email,
                         char *telephone_number, char *address)
{
    contact_T *ncontact;
    ncontact = malloc(sizeof(contact_T));

    ncontact->fname = strdup(firstname);
    ncontact->lname = strdup(lastname);
    ncontact->email = strdup(email);
    ncontact->tel = strdup(telephone_number);
    ncontact->address = strdup(address);

    ncontact->bdate.year = birth_year;
    ncontact->bdate.month = birth_month;
    ncontact->bdate.day = birth_day;

    ncontact->parent = 0;
    ncontact->left = 0;
    ncontact->right = 0;

    return ncontact;
}


contact_T *find_contact_T(contact_T *root, char *first_name, char *last_name,
                          int parameter)    //will return first matching occurence but as a pointer
{
    contact_T *tmp = root;       //kinda' iterator
    contact_T *seeked = new_contact_T(first_name, last_name, 0, 0, 0, "0", "0",
                                      "0");      //helpful temporary contact_T- thanks to it I can use previously made comparing functions which use pointers

    comparing_fun_T cmp = 0;                  //pointer to a comparing function
    if (parameter == 1) cmp = cmp_by_fname_T;
    if (parameter == 2) cmp = cmp_by_lname_T;
    if (parameter == 3)
    {
        printf("find_contact_T function doesn't work if the book is sorted by birth date_T.");
        return 0;
    }

    if (cmp == 0)
    {
        printf("Something isn't right. Program doesn't know which field of structure to use to sort the book");
        return 0;
    }

    while (tmp != 0)
    {
        if (cmp(tmp, seeked) == 0) return tmp;
        else
        {
            if (cmp(tmp, seeked) > 0) tmp = tmp->left;
            else tmp = tmp->right;
        }
    }

    free(seeked->fname);      //freeing space of temporary contact_T
    free(seeked->lname);
    free(seeked->address);
    free(seeked->email);
    free(seeked->tel);
    free(seeked);

    return 0;       // when not found

}


inline
int cmp_by_fname_T(contact_T *node1, contact_T *node2)
{
    return strcmp(node1->fname, node2->fname);
}

inline
int cmp_by_lname_T(contact_T *node1, contact_T *node2)
{
    return strcmp(node1->lname, node2->lname);
}

inline
int cmp_by_birthdate_T(contact_T *node1, contact_T *node2)      //0 if equal, -1 if node1 is younger, +1 if older
{
    if (node1->bdate.year == node2->bdate.year && node1->bdate.month == node2->bdate.month &&
        node1->bdate.day == node2->bdate.day)
        return 0;

    if (node1->bdate.year > node2->bdate.year) return -1;
    if (node1->bdate.year < node2->bdate.year) return 1;

    if (node1->bdate.month > node2->bdate.month) return -1;
    if (node1->bdate.month < node2->bdate.month) return 1;

    if (node1->bdate.day > node2->bdate.day) return -1;
    if (node1->bdate.day < node2->bdate.day) return 1;

    return -2;  //default made to silence the warning
}


void rebuild_book_T(contact_T **new_root, contact_T *node, int parameter)
{
    if (node->left != 0) rebuild_book_T(new_root, node->left, parameter);
    if (node->right != 0) rebuild_book_T(new_root, node->right, parameter);

    add_contact_T(new_root, node, parameter);
}

void sort_book_by_first_name_T(address_book_T *book)
{
    contact_T *nroot = 0;     //new root
    int parameter = 1;

    rebuild_book_T(&nroot, book->root, parameter);
    book->root = nroot;
}

void sort_book_by_last_name_T(address_book_T *book)
{
    contact_T *nroot = 0;     //new root
    int parameter = 2;

    rebuild_book_T(&nroot, book->root, parameter);
    book->root = nroot;
}

void sort_book_by_birth_date_T(address_book_T *book)
{
    contact_T *nroot = 0;     //new root
    int parameter = 3;

    rebuild_book_T(&nroot, book->root, parameter);
    book->root = nroot;
}

void display_book_T(const contact_T *root)      //call it like display_book_T(Tbook.root);
{
    if (root != NULL)
    {
        if (root->left != NULL) display_book_T(root->left);

        //debug
        /*
        bool lewo=false, prawo=false;
        if (root->left != NULL)
        {
            lewo = true;
            display_book_T(root->left);
        }
*/
        printf("%s ", root->lname);
        printf("%s \n", root->fname);

        if (root->right != NULL) display_book_T(root->right);
/*
        //debug
        if (root->right != NULL)
        {
            prawo = true;
            display_book_T(root->right);
        }
        */
    }
    else printf("The book is empty!\n");
}

void display_book_from_end_T(const contact_T *root)
{
    if (root != 0)
    {
        if (root->right != 0) display_book_from_end_T(root->right);

        printf("%s", root->lname);
        printf(" ");
        printf("%s", root->fname);
        printf("\n");

        if (root->left != 0) display_book_from_end_T(root->left);
    }

}

int count_contacts(const contact_T *root, char *first_name, char *last_name)
{
    if (root != 0)
    {
        int left = 0, right = 0;
        if (root->left != 0) left = count_contacts(root->left, first_name, last_name);
        if (root->right != 0) right = count_contacts(root->right, first_name, last_name);

        if (strcmp(root->fname, first_name) == 0 && strcmp(root->lname, last_name) == 0) return right + left + 1;
        else return right + left;
    }
    else return 0;
}
