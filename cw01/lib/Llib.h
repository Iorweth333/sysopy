#ifndef _Llib_H
#define _Llib_H

//#include "Llib.h"
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef struct
{
    int day, month, year;
}date_L;

typedef struct contact_L contact_L;

typedef struct address_book_L address_book_L;

struct contact_L
{
    char *fname;    //first name
    char *lname;    //last name
    date_L bdate;     //birth date_L
    char *email;
    char *tel;      //telephone
    char *address;

    struct contact_L *next, *prev;

};

struct address_book_L
{
    contact_L * first;
    contact_L * last;
};

typedef int ( * comparing_fun_L) (contact_L *, contact_L*);   //pointer to a comparing function

address_book_L  new_book_L(void);  //tested

bool del_book_L(address_book_L *book);     //tested

contact_L * new_contact_L(char *firstname, char *lastname, int birth_day, int birth_month, int birth_year, char *email,
                          char *telephone_number, char *address);    //tested

bool add_contact_L(address_book_L *book, contact_L *ncontact);   //tested

bool del_contact_L(address_book_L *book, const char *first_name, const char *last_name);  //call it like del_contact_L (&book...)      //tested


contact_L * find_contact_L(const address_book_L book, const char *first_name, const char *last_name);    //will return first matching occurence but as a pointer       //tested

int cmp_by_fname_L(contact_L *node1, contact_L *node2);     //tested

int cmp_by_lname_L(contact_L *node1, contact_L *node2);     //tested

int cmp_by_birthdate_L(contact_L *node1, contact_L *node2);      //0 if equal, -1 if node1 is younger, +1 if older      //tested

contact_L * mergee_L(contact_L *list1, contact_L *list2, int parameter);    //parameter: 1 to sort by first name, 2 - last name, 3 - birth date_L         //tested

contact_L * middle_L(contact_L *node);   //searches for half of the list            //tested

contact_L*  sort_book_L(contact_L *node, int parameter);            //tested

void fix_last_pointer_L(address_book_L *book);        //tested

void sort_book_by_first_name_L(address_book_L *book);      //tested

void sort_book_by_last_name_L(address_book_L *book);       //tested

void sort_book_by_birth_date_L(address_book_L *book);      //tested

void display_book_L(const address_book_L book);              //tested

void display_book_from_end_L(const address_book_L book);     //tested

#endif
