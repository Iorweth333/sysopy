#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include "Llib.h"




address_book_L  new_book_L(void)
{
    address_book_L nbook;

    nbook.first = 0;
    nbook.last  = 0;

    return nbook;
}

bool del_book_L(address_book_L *book)      //call it like del_book_L (&book_to_del)
{
    if (book->first == 0 && book->last == 0) return true;

    if (book->first->prev != 0)
    {
        printf ("Something isn't right. First->prev pointer is not null!");
        return false;
    }

    while (book->first != 0)
    {
        contact_L * tmp = book->first;
        book->first = book->first->next;
        free (tmp->fname);
        free (tmp->lname);
        free (tmp->address);
        free (tmp->email);
        free (tmp->tel);
        free (tmp);
    }

    book->first = book->last = 0;
    return true;
}




contact_L * new_contact_L(char *firstname, char *lastname, int birth_day, int birth_month, int birth_year, char *email,
                          char *telephone_number, char *address)
{
    contact_L * ncontact;
    ncontact  = malloc (sizeof(contact_L));

    ncontact->fname = strdup (firstname);
    ncontact->lname = strdup (lastname);
    ncontact->email = strdup (email);
    ncontact->tel = strdup (telephone_number);
    ncontact->address = strdup (address);

    ncontact->bdate.year = birth_year;
    ncontact->bdate.month = birth_month;
    ncontact->bdate.day = birth_day;

    ncontact->next = 0;
    ncontact->prev = 0;

    return ncontact;
}


bool add_contact_L(address_book_L *book, contact_L *ncontact)
{
    if (book->first == 0)
    {
        if (book->last != 0)
        {
            printf ("Something isn't right. First node in the book is 0, but last is not...");
            return false;
        }

        book->first = book->last = ncontact;

        ncontact->next = ncontact->prev = 0;
        return true;
    }
    else
    {
        book->last->next = ncontact;
        ncontact->prev = book->last;
        book->last = ncontact;
        ncontact->next = 0;

        return true;
    }

}


bool del_contact_L(address_book_L *book, const char *first_name, const char *last_name)  //call it like del_contact_L (&book...)
{
    if (book->first == 0) return false; //nothing to delete
    int deleted = 0;                    //function is supposed to delete all occurances. let's count them by the way
                                        //also if there is no occurance, function will return false

    contact_L * tmp = book->first;

    while (tmp != 0)
    {
        if (strcmp (tmp->fname, first_name) == 0 && strcmp (tmp->lname, last_name) == 0) //comparing strings, 0 if equal
        {
            free (tmp->fname);      //freeing space
            free (tmp->lname);
            free (tmp->address);
            free (tmp->email);
            free (tmp->tel);

            tmp->fname = 0;
            tmp->lname = 0;
            tmp->address = 0;
            tmp->email = 0;
            tmp->tel = 0;

            if (tmp->next != 0 && tmp->prev != 0)   //checking if this is not the only contact_L in the book
            {
                tmp->next->prev = tmp->prev;    //detachment when deleting contact_L is neither first nor last
                tmp->prev->next = tmp->next;
            }
            else
            {
                if (tmp->next == 0)      //detachment when deleting contact_L is last in the book
                {
                    tmp->prev->next = 0;
                    book->last = tmp->prev;
                }
                else                    //detachment when deleting contact_L is first in the book
                {
                    tmp->next->prev = 0;
                    book->first = tmp->next;
                }
            }

            free (tmp);
            tmp = 0;
            deleted = deleted + 1;
        }
        else tmp = tmp->next;
    }

    if (deleted > 0)return true;
    else return false;

}



contact_L * find_contact_L(const address_book_L book, const char *first_name, const char *last_name)    //will return first matching occurence but as a pointer
{
    contact_L * tmp = book.first;

    contact_L * result = 0;

    bool found_it = false;
    while (tmp != 0 && found_it == false)
    {
        if (strcmp (tmp->fname, first_name) == 0 && strcmp (tmp->lname, last_name) == 0)        //why didnt i just return tmp?
        {
            found_it = true;
            result = tmp;
        }
        tmp = tmp->next;
    }

    return result;
}


int cmp_by_fname_L(contact_L *node1, contact_L *node2)
{
    return strcmp (node1->fname, node2->fname);
}

int cmp_by_lname_L(contact_L *node1, contact_L *node2)
{
    return strcmp (node1->lname, node2->lname);
}

int cmp_by_birthdate_L(contact_L *node1, contact_L *node2)      //0 if equal, -1 if node1 is younger, +1 if older
{
    if (node1->bdate.year == node2->bdate.year && node1->bdate.month == node2->bdate.month && node1->bdate.day == node2->bdate.day) return 0;

    if (node1->bdate.year > node2->bdate.year) return -1;
    if (node1->bdate.year < node2->bdate.year) return 1;

    if (node1->bdate.month > node2->bdate.month) return -1;
    if (node1->bdate.month < node2->bdate.month) return 1;

    if (node1->bdate.day > node2->bdate.day) return -1;
    if (node1->bdate.day < node2->bdate.day) return 1;

    return -2;  //just to supress warning, program will never reach this line
}


contact_L * mergee_L(contact_L *list1, contact_L *list2, int parameter)    //parameter: 1 to compare by first name, 2 - last name, 3 - birth date_L
{
    if (list1 != 0 && list2 == 0) return list1;
    if (list1 == 0 && list2 != 0) return list2;
    if (list1 == 0 && list2 == 0) return 0;

    contact_L *list3 = 0;

    comparing_fun_L cmp = 0;                  //pointer to a comparing function
    if (parameter == 1) cmp = cmp_by_fname_L;
    if (parameter == 2) cmp = cmp_by_lname_L;
    if (parameter == 3) cmp = cmp_by_birthdate_L;
    if (cmp == 0)
    {
        printf ("Something isn't right. Program doesn't know which field of structure to use to sort the book");
        return 0;
    }

    if (cmp (list1, list2) < 0 )
    {
        list3 = list1;
        list1 = list1 -> next;
        list3 -> next = 0;
        list3 -> prev = 0;
    }
    else
    {
        list3 = list2;
        list2 = list2 -> next;
        list3 -> next = 0;
        list3 -> prev = 0;
    }

    contact_L *tmp = list3;               //temporary pointer wchich will point the end of list which is about to be returned
    while (list1 != 0 && list2 != 0)
    {
        if (cmp (list1,list2) < 0)
        {
            tmp -> next = list1;
            tmp -> next -> prev = tmp;
            list1 = list1 -> next;
            tmp = tmp -> next;
            tmp -> next = 0;
        }
        else
        {
            tmp -> next = list2;
            tmp -> next -> prev = tmp;
            list2 = list2 -> next;
            tmp = tmp-> next;
            tmp -> next = 0;
        }
    }

    if (list1 != 0)
    {
        tmp -> next = list1;
        list1 -> prev = tmp;
    }
    else
    {
        tmp -> next = list2;
        list2 -> prev = tmp;
    }


    return list3;
}



contact_L * middle_L(contact_L *node)   //searches for half of the list
{
    if (node == 0) return 0;
    contact_L * half = node;
    bool step = false;
    while (node->next != 0)         //one pointer does only every second step
    {
        if (step) half = half->next;
        node = node->next;
        step = !step;
    }
    return half;
}


contact_L*  sort_book_L(contact_L *node, int parameter)
{
    if (node == 0) return 0;
    if (node->next == 0) return node;

    contact_L * mid = middle_L(node);                  //searching for middle_L of the list
    contact_L * l2 = mid->next;                       //creatin a new list...
    l2->prev = 0;
    mid->next = 0;                                  //...by detaching the second part of the given
    contact_L * l1 = sort_book_L(node, parameter);               //sorting both parts recursively
    contact_L * l3 = sort_book_L(l2, parameter);
    return mergee_L(l1, l3, parameter);
}

void fix_last_pointer_L(address_book_L *book)
{
    contact_L * tmp = book->first;
    while (tmp->next != 0) tmp = tmp->next;
    book->last = tmp;

}


void sort_book_by_first_name_L(address_book_L *book)       //equivalent of "mergesort" function in my private algorithm ''library''
{
    book->first = sort_book_L(book->first, 1);
    fix_last_pointer_L(book);
}

void sort_book_by_last_name_L(address_book_L *book)
{
    book->first = sort_book_L(book->first, 2);
    fix_last_pointer_L(book);
}

void sort_book_by_birth_date_L(address_book_L *book)
{
    book->first = sort_book_L(book->first, 3);
    fix_last_pointer_L(book);
}

void display_book_L(const address_book_L book)
{
    contact_L * iter = book.first;

    while (iter)
    {
        printf ("%s", iter->fname);
        printf (" ");
        printf ("%s", iter->lname);
        printf ("\n");

        iter = iter->next;
    }
}

void display_book_from_end_L(const address_book_L book)
{
    contact_L * iter = book.last;

    while (iter)
    {
        printf ("%s", iter->fname);
        printf (" ");
        printf ("%s", iter->lname);
        printf ("\n");

        iter = iter->prev;
    }
}
