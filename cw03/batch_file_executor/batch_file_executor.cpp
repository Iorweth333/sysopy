#include <iostream>
#include <string>
#include <unistd.h>
#include <stdio.h>
#include <fstream>
#include <sys/wait.h>
#include <cstring>

using namespace std;

char **extract_arguments(string line)
{
    int count = 0;

    for (unsigned int i = 0; i < line.size(); i++) if (line[i] == ' ') count++;

    count++;   //because counter counted spaces between arguments and I want number of arguments

    char **res = (char **) malloc(count * sizeof(char *));

    string argument;
    int argument_index = 0;

    for (unsigned int i = 0; i < line.size(); i++)
    {
        if (line[i] != ' ') argument += line[i];
        else
        {
            res[argument_index] = (char *) malloc(argument.length() * sizeof(char));
            strcpy(res[argument_index], argument.c_str());
            argument_index++;
            argument.clear();
        }
    }
    res[argument_index] = (char *) malloc(argument.length() * sizeof(char));
    strcpy(res[argument_index], argument.c_str());
    return res;

}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        cout << "Not enough arguments! Please provide batch file name.\n";
        return 1;
    }

    ifstream batch_file;
    batch_file.open (argv[1]);

    string line;
    unsigned long space_pos;
    int lines_counter = 0;

    while (batch_file.good())
    {
        lines_counter++;

        getline(batch_file, line);

        if (line[0] == '#')
        {
            //environmental variable. luckily these variables are inherited by forked process

            space_pos = line.find(' ');
            if (space_pos == string::npos)    //space not found, variable is about to be deleted
            {
                string variable_name = line.substr(1, line.length() - 1);     //copying line without # sign
                cout<<"\nUnseting the variable "<<variable_name<<endl;
                if (unsetenv(variable_name.c_str()) != 0)
                {
                    cout<<"Error occurred while unsetting variable: "<<variable_name<<endl;
                    perror(nullptr);
                }
            }
            else
            {
                //setenv or putenv

                string variable_name = line.substr(1, space_pos - 1);     //#bla bla
                string value = line.substr(space_pos + 1, line.length() - space_pos - 1);
                cout<<"\nSetting the variable "<<variable_name<<" with value "<<value<<endl;

                if (setenv(variable_name.c_str(), value.c_str(), 1) != 0)    //1 to overwrite if existed before. returns 0 on success
                {
                    cout<<"Error occurred while setting variable: "<<variable_name<<endl;
                    perror(nullptr);
                }
            }
        }
        else
        {
            if (!line.empty())
            {
                //action to execute

                pid_t pid = fork();

                if (pid == 0)       //in child, must change content of program
                {
                    int err = 0;
                    char **arguments = nullptr;
                    space_pos = line.find(' ');
                    cout<<"\nRunning a child process with command "<<line<<endl<<endl;

                    if (space_pos != string::npos)    //space found, so probably there are arguments
                    {
                        arguments = extract_arguments(line);
                        err = execvp(arguments[0], arguments);
                        if (err != 0)
                        {
                            cout<<"Error occurred\n";
                            perror(nullptr);
                            exit (err);
                        }

                    }
                    else
                    {
                        arguments = (char**) malloc (sizeof (char*));
                        arguments[0] = (char *) malloc (line.size() * sizeof (char));   //that mambo-jumbo is neccessary because every wants to have call path as argument
                        strcpy (arguments[0], line.c_str());                            //and I decided to use string class. you know, for convenience
                        err = execvp(line.c_str(), arguments);
                        if (err != 0)
                        {
                            cout<<"Error occurred\n";
                            perror(nullptr);
                            exit (err);
                        }
                    }
                    //https://www.youtube.com/watch?v=mj2VjcOXXs4
                    //with l: comma separated arguments
                    //with v: vector (i.e. an array of strings)
                    //with p: include normal searchpath for executable
                }
                else
                {
                    //https://www.youtube.com/watch?v=9seb8hddeK4

                    if (pid == -1)
                    {
                        cout<<"Problem in line number "<< lines_counter<<". Failed to fork.\n";
                        perror(nullptr);
                        exit(1);
                    }
                    int child_status;

                    //if childpid wanted:
                    //pid_t childpid = wait (&child_status);

                    wait (&child_status);
                    int child_returned_value = -10;
                    if (WIFSIGNALED(child_status)) child_returned_value = WTERMSIG(child_status);  //WTERMSIG should be employed only if WIFSIGNALED returned true.
                    if (WIFEXITED(child_status)) child_returned_value = WEXITSTATUS (child_status);
                    if (child_returned_value != 0)
                    {
                        cout<<"Problem in line number "<< lines_counter<<". Child process returned "<<child_returned_value<<endl;
                        exit(1);
                    }

                }
            }
        }
    }


return 0;
}
