
#include <stdio.h>
#include <stdlib.h>

int main()
{
    char *arr= (char *) 1;
    int n = 1000000;

    while (n)
    {
        arr = malloc (n * sizeof (char)); //allocating n chars

    }
    printf ("DONE\n");
    return 0;
}

//down there is better version- with error handling and allowing to choose how much memory is to be allocated

/*
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
int main(int argc, char *argv[])
{
    printf("Here we go!\n");
    char *arr;
    long n = 1000000; //default: million
    if (argc == 2) n = strtol(argv[1], NULL, 10);
    long count = 0;
    while (n)
    {
        arr = malloc (1024*1024); //allocating 1 MiB
        count = count+1;
        if (errno == ENOMEM)
        {
            printf ("Out of memory!\n");
            break;
        }
        printf ("Allocated %ld megabytes\n", count);

        n = n-1;
    }
    printf ("Finished!\n");
    return 0;
}
 */