#include <iostream>

using namespace std;

/*
 * Na potrzeby demonstracji zadania przygotuj również program,
 * który przyjmuje w argumencie nazwę zmiennej środowiskowej,
 * a następnie odczytuje ze środowiska i wypisuje na ekranie jej aktualną wartość.
 * Program powinien również wypisywać stosowny komunikat, jeśli w środowisku nie ma zmiennej o wskazanej nazwie.
 */

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        cout << "Niewlasciwa ilosc argumentow! Podaj nazwe zmiennej.\n";
        return 1;
    }

    const char* s = getenv(argv[1]);
    if (s != nullptr) cout<<"Wartosc rzeczonej zmiennej: \n"<<s<<"\n";
    else cout<<"Taka zmienna nie istnieje!\n";
    return 0;
}