#include <iostream>
#include <string>
#include <unistd.h>
#include <sys/resource.h>
#include <fstream>
#include <sys/wait.h>
#include <cstring>
#include <sys/time.h> //Including <sys/time.h> is not required these days, but increases
                      //portability.  (Indeed, struct timeval is defined in <sys/time.h>.)
using namespace std;

char **extract_arguments(string line)
{
    int count = 0;

    for (unsigned int i = 0; i < line.size(); i++) if (line[i] == ' ') count++;

    count++;   //because counter counted spaces between arguments and I want number of arguments

    char **res = (char **) malloc(count * sizeof(char *));

    string argument;
    int argument_index = 0;

    for (unsigned int i = 0; i < line.size(); i++)
    {
        if (line[i] != ' ') argument += line[i];
        else
        {
            res[argument_index] = (char *) malloc(argument.length() * sizeof(char));
            strcpy(res[argument_index], argument.c_str());
            argument_index++;
            argument.clear();
        }
    }
    res[argument_index] = (char *) malloc(argument.length() * sizeof(char));
    strcpy(res[argument_index], argument.c_str());
    return res;

}

void setlimits (long proc, long mem)
{
    rlimit limits;
    int err;
    limits.rlim_max = (rlim_t) proc;
    limits.rlim_cur = (rlim_t) proc;
    err = setrlimit(RLIMIT_CPU, &limits);
    if (err != 0)
    {
        cout<<"Error occurred\n";
        perror(nullptr);
        exit (err);
    }
    limits.rlim_cur = (rlim_t) (mem * 1048576); //limit is given in megabytes
    limits.rlim_max = (rlim_t) (mem * 1048576); //and setrlimit takes bytes
    err = setrlimit(RLIMIT_AS, &limits);
    if (err != 0)
    {
        cout<<"Error occurred\n";
        perror(nullptr);
        exit (err);
    }
    cout<<"Limits set.\n";
}

void print_usage (rusage &overall, rusage &prev_overall)
{
    rusage curr;
    int err = getrusage(RUSAGE_CHILDREN, &overall);

    curr.ru_stime.tv_sec = overall.ru_stime.tv_sec - prev_overall.ru_stime.tv_sec;
    curr.ru_stime.tv_usec = overall.ru_stime.tv_usec - prev_overall.ru_stime.tv_usec;
    curr.ru_utime.tv_usec = overall.ru_utime.tv_usec - prev_overall.ru_utime.tv_usec;
    curr.ru_utime.tv_sec = overall.ru_utime.tv_sec - prev_overall.ru_utime.tv_sec;

    if (err == 0)
    {
        cout<<"\nThis child used: \n";
        cout<<"-user time: "<<curr.ru_utime.tv_sec<<" (sec) + "<< curr.ru_utime.tv_usec<<" (microsec)\n";
        cout<<"-system time: "<<curr.ru_stime.tv_sec<<" (sec) + "<< curr.ru_stime.tv_usec<<" (microsec)\n";
        //https://stackoverflow.com/questions/556405/what-do-real-user-and-sys-mean-in-the-output-of-time1
    }
    else
    {
        cout<<"Error occurred\n";
        perror (nullptr);
        exit(err);
    }
    //prev_overall = overall;
    prev_overall.ru_utime.tv_sec  = overall.ru_utime.tv_sec;
    prev_overall.ru_utime.tv_usec = overall.ru_utime.tv_usec;
    prev_overall.ru_stime.tv_usec = overall.ru_stime.tv_usec;
    prev_overall.ru_stime.tv_sec  = overall.ru_stime.tv_sec;
}

int main(int argc, char *argv[])
{
    if (argc != 4)
    {
        cout << "Not enough arguments! Please provide batch file name and restrains: processor's time (seconds) and memory (megabytes)\n";
        return 1;

        /*
         * Zmodyfikuj program z Zadania 1 tak, aby każde polecenie wykonywane przez interpreter
         * miało nałożone pewne twarde ograniczenie na dostępny czas procesora oraz rozmiar pamięci wirtualnej.
         * Wartości tych ograniczeń (odpowiednio w sekundach i megabajtach) powinny być przekazywane
         * jako drugi i trzeci argument wywołania interpretera (pierwszym argumentem jest nazwa pliku wsadowego).
         */
    }

    ifstream batch_file;
    batch_file.open (argv[1]);
    long processor_limit = strtol (argv[2], nullptr, 10);
    long memory_limit = strtol (argv[3], nullptr, 10);
    rusage prev_overall;
    prev_overall.ru_stime.tv_sec=0;
    prev_overall.ru_stime.tv_usec=0;
    prev_overall.ru_utime.tv_usec=0;
    prev_overall.ru_utime.tv_sec=0;
    rusage overall;

    string line;
    unsigned long space_pos;
    int lines_counter = 0;

    while (batch_file.good())
    {
        lines_counter++;

        getline(batch_file, line);

        if (line[0] == '#')
        {
            //environmental variable. luckily these variables are inherited by forked process

            space_pos = line.find(' ');
            if (space_pos == string::npos)    //space not found, variable is about to be deleted
            {
                string variable_name = line.substr(1, line.length() - 1);     //copying line without # sign
                cout<<"\nUnseting the variable "<<variable_name<<endl;
                if (unsetenv(variable_name.c_str()) != 0)
                {
                    cout<<"Error occurred while unsetting variable: "<<variable_name<<endl;
                    perror(nullptr);
                }
            }
            else
            {
                //setenv or putenv

                string variable_name = line.substr(1, space_pos - 1);     //#bla bla
                string value = line.substr(space_pos + 1, line.length() - space_pos - 1);
                cout<<"\nSetting the variable "<<variable_name<<" with value "<<value<<endl;

                if (setenv(variable_name.c_str(), value.c_str(), 1) != 0)    //1 to overwrite if existed before. returns 0 on success
                {
                    cout<<"Error occurred while setting variable: "<<variable_name<<endl;
                    perror(nullptr);
                }
            }
        }
        else
        {
            if (!line.empty())
            {
                //action to execute

                pid_t pid = fork();

                if (pid == 0)       //in child, must change content of program
                {
                    int err = 0;
                    char **arguments = nullptr;

                    space_pos = line.find(' ');
                    cout<<"\nRunning a child process with command "<<line<<endl;

                    if (space_pos != string::npos)    //space found, so probably there are arguments
                    {
                        arguments = extract_arguments(line);
                        //setting limits
                        setlimits(processor_limit, memory_limit);
                        err = execvp(arguments[0], arguments);
                        if (err != 0)
                        {
                            cout<<"Error occurred\n";
                            perror(nullptr);
                            exit (err);
                        }

                    }
                    else
                    {
                        arguments = (char**) malloc (sizeof (char*));
                        arguments[0] = (char *) malloc (line.size() * sizeof (char));   //that mambo-jumbo is neccessary because every wants to have call path as argument
                        strcpy (arguments[0], line.c_str());                            //and I decided to use string class. you know, for convenience
                        //setting limits
                        setlimits(processor_limit, memory_limit);
                        err = execvp(line.c_str(), arguments);
                        if (err != 0)
                        {
                            cout<<"Error occurred\n";
                            perror(nullptr);
                            exit (err);
                        }
                    }
                    //https://www.youtube.com/watch?v=mj2VjcOXXs4
                    //with l: comma separated arguments
                    //with v: vector (i.e. an array of strings)
                    //with p: include normal searchpath for executable
                }
                else
                {
                    //https://www.youtube.com/watch?v=9seb8hddeK4

                    if (pid == -1)
                    {
                        cout<<"Problem in line number "<< lines_counter<<". Failed to fork.\n";
                        perror(nullptr);
                        exit(1);
                    }
                    int child_status;

                    //if childpid wanted:
                    //pid_t childpid = wait (&child_status);



                    wait (&child_status);
                    print_usage (overall, prev_overall);

                    int child_returned_value = -10;
                    if (WIFSIGNALED(child_status)) child_returned_value = WTERMSIG(child_status);  //WTERMSIG should be employed only if WIFSIGNALED returned true.
                    if (WIFEXITED(child_status)) child_returned_value = WEXITSTATUS (child_status);
                    if (child_returned_value != 0)
                    {
                        cout<<"Problem in line number "<< lines_counter<<". Child process returned "<<child_returned_value<<endl;
                        exit(1);
                    }

                }
            }
        }
    }


    return 0;
}